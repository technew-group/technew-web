## 目录
* [项目简介](#项目简介)
    * 1.项目背景
    * 2.整体系统结构图
* [功能模块描述](#功能模块描述)
    * 功能模块描述图
* [数据交互](#数据交互)
    * 1.发送微信消息处理流程
    * 2.关键字交互图

# 项目简介

## 1. 项目背景
**“哎哟铁牛”** 校园服务号项目，面向黑龙江科技大学的同学，提供查课表，空教室，成绩推送，上课提醒，订阅中心等服务。  
作为团队的创始人之一，现已将项目开源，想学习微信开发的同学可以自行下载~

## 2. 整体系统结构图
<div align="center"> <img src="https://gitlab.com/technew-group/technew-web/raw/master/src/main/resources/read-images/%E7%B3%BB%E7%BB%9F%E6%9E%B6%E6%9E%84%E5%9B%BE.png" width="600"/> </div><br>

可以看出，客户端可以是微信公众号端（对话框输入的关键字），用户绑定的Jsp页面，课表的异步查询，以及公众号后台配置时验证的check接口。
主要服务是上课提醒，成绩推送，时间换算，定时任务和订阅。  
ngrok内网穿透是我们的内网域名转换的测试工具，主要用于对于测试号使用时在本机自测。


# 功能模块描述
主要有5个模块，timetable，tools，wechat，worm，web
>主要功能是**成绩推送，上课提醒，订阅中心**。用户绑定后，可以通过定时任务的调度，先判断是否订阅该功能，然后将每次更新的成绩推送给用户。上课提醒一般在上课的前一小时发送模板消息。
  
模块功能写的比较细，所以分了两张图。
<div align="center"> <img src="https://gitlab.com/technew-group/technew-web/raw/master/src/main/resources/read-images/%E5%8A%9F%E8%83%BD%E6%A8%A1%E5%9D%97%E5%9B%BE1.png" width="600"/> </div><br>

<div align="center"> <img src="https://gitlab.com/technew-group/technew-web/raw/master/src/main/resources/read-images/%E5%8A%9F%E8%83%BD%E6%A8%A1%E5%9D%97%E5%9B%BE2.png" width="600"/> </div><br>

值得注意的是，在旧版本中，是对M黑科技的数据进行爬取。利用模拟登录获取token，再拿http的响应报文获取对应的json数据，解析封装，逻辑处理后展示给用户。  

# 数据交互

## 1. 发送微信消息处理流程
微信消息包含，链接，文本，图片，表情包，这里看下对于微信消息整体的处理逻辑。
<div align="center"> <img src="https://gitlab.com/technew-group/technew-web/raw/master/src/main/resources/read-images/%E5%8F%91%E9%80%81%E6%B6%88%E6%81%AF%E5%A4%84%E7%90%86%E6%B5%81%E7%A8%8B%E5%9B%BE.png" width="600"/> </div><br>

## 2. 关键字交互图
关键字的交互就是微信消息处理的一种特殊情况，只不过这个消息是**文本**。  
下面来看看消息文本消息具体是如何交互的。
<div align="center"> <img src="https://gitlab.com/technew-group/technew-web/raw/master/src/main/resources/read-images/%E5%BE%AE%E4%BF%A1%E5%85%B3%E9%94%AE%E5%AD%97%E6%95%B0%E6%8D%AE%E6%B5%81%E5%90%91.png" width="600"/> </div><br>

### 感觉有用的小伙伴不妨**star**一下哈，另外欢迎关注知乎专栏**Java修仙道路**~