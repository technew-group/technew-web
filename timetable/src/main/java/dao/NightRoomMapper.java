package dao;

import org.apache.ibatis.annotations.Param;
import pojo.NightRoom;
import pojo.NightRoomExample;

import java.util.List;

public interface NightRoomMapper {
    int countByExample(NightRoomExample example);

    int deleteByExample(NightRoomExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(NightRoom record);

    int insertSelective(NightRoom record);

    List<NightRoom> selectByExample(NightRoomExample example);

    NightRoom selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") NightRoom record, @Param("example") NightRoomExample example);

    int updateByExample(@Param("record") NightRoom record, @Param("example") NightRoomExample example);

    int updateByPrimaryKeySelective(NightRoom record);

    int updateByPrimaryKey(NightRoom record);
}