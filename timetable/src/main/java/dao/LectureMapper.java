package dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pojo.Lecture;
import pojo.LectureExample;

public interface LectureMapper {
    int countByExample(LectureExample example);

    int deleteByExample(LectureExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Lecture record);

    int insertSelective(Lecture record);

    List<Lecture> selectByExample(LectureExample example);

    Lecture selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Lecture record, @Param("example") LectureExample example);

    int updateByExample(@Param("record") Lecture record, @Param("example") LectureExample example);

    int updateByPrimaryKeySelective(Lecture record);

    int updateByPrimaryKey(Lecture record);
}