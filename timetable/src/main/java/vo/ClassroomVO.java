package vo;

/**
 * @author  zhangll
 * @since  2018.11.5
 */
/*
   实体类
    封装查找空教室的请求参数
 */
public class ClassroomVO {
    private String place; //请求参数中地点 （主楼或科厦）
    private Integer floor;  //请求参数中的 教室的楼层
    private  Integer dayOfWeek;  //请求参数中 周几
    private  Integer  currentWeek; //请求参数中 第几周
    private Integer timeCase; //请求参数中  第几节
    //get()  set()


    @Override
    public String toString() {
        return "ClassroomVO{" +
                "place='" + place + '\'' +
                ", floor=" + floor +
                ", dayOfWeek=" + dayOfWeek +
                ", currentWeek=" + currentWeek +
                ", timeCase=" + timeCase +
                '}';
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public void setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public void setCurrentWeek(Integer currentWeek) {
        this.currentWeek = currentWeek;
    }

    public void setTimeCase(Integer timeCase) {
        this.timeCase = timeCase;
    }

    public String getPlace() {
        return place;
    }

    public Integer getFloor() {
        return floor;
    }

    public Integer getDayOfWeek() {
        return dayOfWeek;
    }

    public Integer getCurrentWeek() {
        return currentWeek;
    }

    public Integer getTimeCase() {
        return timeCase;
    }
}
