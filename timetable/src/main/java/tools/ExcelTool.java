package tools;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;

/**
 * excel导入工具类
 *
 * @author 吕圣业
 * @since 31 十月 2018
 */
public class ExcelTool {

    private static final int NUMERIC = 0;
    private static final int STRING = 1;
    private static final int FORMULA = 2;
    private static final int BLANK = 3;
    private static final int BOOLEAN = 4;
    private static final int ERROR = 5;
    private String tableName = "lecture";


    /**
     *   * 解析方法
     *   *
     *   * @param file
     *   * @return sql
     *   * @throws Exception
     *  
     */
    public String getExcel(String filePath) throws Exception {
        // 创建对Excel工作簿文件的引用
        boolean isExcel2003 = filePath.toLowerCase().endsWith("xls") ? true : false;
        Workbook workbook = null;
        if (isExcel2003) {
            workbook = new HSSFWorkbook(new FileInputStream(new File(filePath)));
        } else {
            workbook = new XSSFWorkbook(new FileInputStream(new File(filePath)));
        }
        // 在Excel文档中，第一张工作表的缺省索引是0
        // 其语句为：
        // HSSFSheet sheet = wookbook.getSheetAt(0);
        Sheet sheet = workbook.getSheet("Sheet1");
        // 获取到Excel文件中的行数
        int rows = sheet.getPhysicalNumberOfRows();
        // 遍历行
//        List<String[]> list_excel = new ArrayList<String[]>();
        StringBuffer value = new StringBuffer("insert into " + tableName + " values");

        for (int i = 0; i <= rows; i++) {
            value.append("(default,\"");
            // 读取左上端单元格
            Row row = sheet.getRow(i);
            // 行不为空
            if (row != null) {
                // 获取到Excel文件中的列数
                int cells = row.getPhysicalNumberOfCells();
                // 遍历列
                for (int j = 0; j <= cells; j++) {
                    // 获取列对象
                    Cell cell = row.getCell(j);
                    if (cell != null) {
                        switch (cell.getCellType()) {
                            case NUMERIC:
                                value.append(cell.getNumericCellValue());
                                break;
                            case FORMULA:
                                try {
                                    value.append(String.valueOf(cell.getStringCellValue()));
                                } catch (IllegalStateException e) {
                                    System.out.println("ExcelTool参数异常,请定位到getExcel()方法");
//                                    String valueOf = String.valueOf(cell.getNumericCellValue());
//                                    BigDecimal bd = new BigDecimal(Double.valueOf(valueOf));
//                                    bd = bd.setScale(2, RoundingMode.HALF_UP);
                                }
                                break;
                            case BLANK:
//                                if(j==cells-1){
//                                    value.append("0\"");
//                                    break;
//                                }
//                                value.append("0,");
                                break;
                            case BOOLEAN:
                                value.append(cell.getBooleanCellValue());
                                break;
                            case ERROR:
                                value.append(cell.getErrorCellValue());
                                break;
                            case STRING:
                                value.append(String.valueOf(cell.getStringCellValue().replace(" ", "")));
                                value.toString().replace("\t", "");

                                if (j == cells - 1) {
                                    value.append("\"");
                                    break;
                                }
                                value.append("\",\"");
                                break;
                            default:
                                break;
                        }

                    }
                }
//                String[] val = value.toString().split(",");
//                list_excel.add(val);
            }
            if (i == rows - 1) {
                value.append(")\n");
                break;
            }
            value.append("),\n");
        }
        return value.toString();

    }

}