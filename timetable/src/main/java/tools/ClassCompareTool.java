package tools;

import org.apache.commons.lang3.StringUtils;
import pojo.Lecture;

import java.util.*;

/**
 * 班级过滤工具类
 *
 * @author 吕圣业
 * @since 6 十一月 2018
 */
public class ClassCompareTool {

    public List<Lecture> majorClassFilter(List<Lecture> lectureList, String currentClass) {

        Iterator<Lecture> iterator = lectureList.iterator();
        Map<String, Integer> map = parseClass(currentClass);
        int grade = map.get("grade");
        int classNum = map.get("classNum");
        if(StringUtils.isBlank(theStringBeforeNum(currentClass))){
            return null;
        }
        while (iterator.hasNext()) {
            Lecture lecture = iterator.next();
            String majorClass = lecture.getMajorClass();
            int markCount = markCount(majorClass, '、');
            int hyphenCount = markCount(majorClass, '-');
            boolean result = false;
            //根据年级过滤,不符合直接进入下个循环
            majorClass.substring(0, majorClass.indexOf("-"));
            if (majorClass.indexOf("-") != -1) {
                boolean isGrade = theNumberInString(majorClass.substring(0, majorClass.indexOf("-"))) == grade;
                if (!isGrade) {
                    iterator.remove();
                    continue;
                }
            } else {
                //单独处理该字段
                if ("全校18级俄语生".equals(majorClass)) {
                    result = true;
                }
            }
            //处理有顿号的情况
            if (majorClass.indexOf("、") != -1) {
                for (int i = 0; i < markCount + 1; i++) {
                    hyphenCount = markCount(majorClass, '-');
                    int beginIndex = 0;
                    int endIndex = majorClass.indexOf("、");
                    if (i == markCount) {
                        endIndex = majorClass.length();
                    }
                    String nextString = majorClass.substring(beginIndex, endIndex);
                    //处理单个"-"的情况
                    if (hyphenCount == 1) {
                        int num = theNumberInString(StringUtils.substringAfter(nextString, "-"));
                        if (num == classNum) {
                            result = true;
                        }
                    } else if (hyphenCount > 1) {
                        //处理多个"-"的情况
                        int firstHyphen = nextString.indexOf("-");
                        nextString = nextString.substring(firstHyphen + 1);
                        int hyphenIndex = nextString.indexOf("-");
                        if (i == 0) {
                            hyphenIndex = nextString.lastIndexOf("-");
                        }
                        String afterHyphen = nextString.substring(hyphenIndex + 1);
                        int firstClass = theNumberInString(StringUtils.substringBefore(nextString, "-"));
                        int lastClass = theNumberInString(afterHyphen);
                        if (classNum >= firstClass && classNum <= lastClass) {
                            result = true;
                        }
                    } else {
                        //处理没有"-"的情况
                        int num = theNumberInString(nextString);
                        if (num == classNum) {
                            result = true;
                        }
                    }
                    if (i == markCount) {
                        majorClass = majorClass.substring(endIndex);
                        continue;
                    }
                    majorClass = majorClass.substring(endIndex + 1);
                }
            } else if (hyphenCount == 1) {
                //处理单个专业且只有一个班级的情况
                majorClass = lecture.getMajorClass();
                String professionName = theStringBeforeNum(majorClass);
                int num = theNumberInString(majorClass.substring(majorClass.indexOf("-") + 1));
                if (num == classNum && professionName == theStringBeforeNum(currentClass)) {
                    result = true;
                }
            } else if (hyphenCount == 2) {
                //处理单个专业的，多班级的情况
                majorClass = lecture.getMajorClass();
                String professionName = theStringBeforeNum(majorClass);
                Map<Integer,String> majorMap=parseAll(majorClass);
                if(majorMap.size()>1 && !"".equals(professionName)){
                    for(int k=0;k<majorMap.size();k++){
                        if(majorMap.get(k+1).equals(currentClass)){
                            result = true;
                        }
                    }
                }
                for (int i = 0; i < hyphenCount + 1; i++) {
                    int hyphenIndex = majorClass.indexOf("-");
                    if (i == 0) {
                        continue;
                    }
                    int beginIndex = hyphenIndex;
                    String nextString = majorClass.substring(beginIndex + 1);
                    int maxClass = theNumberInString(StringUtils.substringAfter(nextString, "-"));
                    int minClass = theNumberInString(StringUtils.substringBefore(nextString, "-"));
                    if (!"".equals(professionName)) {
                        if (classNum >= minClass && classNum <= maxClass && professionName.equals(theStringBeforeNum(currentClass))) {
                            result = true;
                        }
                    }
                }
            } else {
                //单字段多个专业的情况
                Map<Integer, String> all = parseAll(majorClass);
                for (int i = 0; i < all.size(); i++) {
                    String profession = all.get(i + 1);
                    //目前符合条件的专业的名字
                    String professionName = theStringBeforeNum(all.get(i + 1));
                    int hCount = markCount(profession, '-');
                    //同专业单班级的情况
                    if (hCount == 1) {
                        if (profession == majorClass) {
                            result = true;
                        }
                    } else if (hCount > 1) {
                        //同专业多班级的情况
                        for (int j = 0; j < markCount(profession, '-') + 1; j++) {
                            if (j == 0) {
                                profession = profession.substring(profession.indexOf("-") + 1);
                                continue;
                            }
                            int minClass = theNumberInString(StringUtils.substringBefore(profession, "-"));
                            int maxClass = theNumberInString(StringUtils.substringAfter(profession, "-"));
                            if (professionName != null || !"".equals(professionName)) {
                                if (classNum >= minClass && classNum <= maxClass && professionName.equals(theStringBeforeNum(currentClass))) {
                                    result = true;
                                }
                            }
                        }
                    } else {
                        //无横杠情况
                        //暂时由前面特殊处理了
                    }

                }
            }
            //班级不符合，过滤数据
            if (result == false) {
                iterator.remove();
            }
        }
        return lectureList;
    }

    public int markCount(String s, char mark) {
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            String one = s.substring(i, i + 1);
            char ar = one.charAt(0);
            if (ar == mark) {
                count++;
            }
        }
        return count;
    }

    /**
     * @param s
     * @return 返回字符串中的数字
     */
    public int theNumberInString(String s) {
        String result = "";
        for (int i = 0; i < s.length(); i++) {
            String one = s.substring(i, i + 1);
            char ar = one.charAt(0);
            if (ar >= '0' && ar <= '9') {
                result += ar;
            }
        }
        if ("".equals(result)) {
            return 0;
        }
        int maxWeek = Integer.parseInt(result);
        return maxWeek;
    }

    /**
     * 解析当前班级
     *
     * @param currentClass
     * @return 年级和班级
     */
    public Map<String, Integer> parseClass(String currentClass) {
        String beforeHyphen = StringUtils.substringBefore(currentClass, "-");
        String afterHyphen = StringUtils.substringAfter(currentClass, "-");
        int grade = theNumberInString(beforeHyphen);
        int classNum = theNumberInString(afterHyphen);
        Map<String, Integer> map = new HashMap<>();
        map.put("grade", grade);
        map.put("classNum", classNum);
        return map;
    }

    /**
     * 解析无顿号，多专业的情况
     *
     * @return
     */
    public Map<Integer, String> parseAll(String str) {
        Map<Integer, String> map = new HashMap<>();
        //记录下标
        List<Integer> list = new ArrayList<>();
        int index = 0;
        int flag = 0;
        int length = str.length();
        while (index < length) {
            if (isNumOrHyphen(str.charAt(index))) {
                //是数字或"-"
                flag = 0;
                index++;
            } else {
                //中文
                if (flag == 0) {
                    //此时index是中文字符的索引
                    list.add(index);
                    flag = 1;
                }
                index++;
            }
        }
        list.add(str.length());
        for (int i = 1; i < list.size(); i++) {
            map.put(i, str.substring(list.get(i - 1), list.get(i)));
        }

        return map;
    }

    /**
     * 数字或横杠为true，中文为false
     *
     * @param c
     * @return
     */
    public static boolean isNumOrHyphen(char c) {
        if ((c >= '0' && c <= '9') || c == '-') {
            return true;
        }
        return false;
    }

    /**
     * @param s
     * @return 返回第一个数字前的字符串
     */
    public String theStringBeforeNum(String s) {
        String result = "";
        for (int i = 0; i < s.length(); i++) {
            String one = s.substring(i, i + 1);
            char ar = one.charAt(0);
            if (ar >= '0' && ar <= '9') {
                return result;
            }
            result += ar;
        }
        return result;
    }
}
