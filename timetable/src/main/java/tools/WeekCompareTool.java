package tools;

import org.apache.commons.lang3.StringUtils;
import pojo.Lecture;

import java.util.Iterator;
import java.util.List;

/**
 * 周次过滤工具类
 *
 * @author 吕圣业
 * @since 6 十一月 2018
 */
public class WeekCompareTool {
    /**
     * 周次过滤方法
     *
     * @param currentWeek
     * @param lectureList
     * @return
     */
    public List<Lecture> weekFilter(List<Lecture> lectureList, int currentWeek) {

        Iterator<Lecture> iterator = lectureList.iterator();
        while (iterator.hasNext()) {
            boolean result = false;
            boolean single_result = false;
            boolean double_result = false;
            boolean weekTogether = true;
            Lecture lecture = iterator.next();
            String successiveWeek = lecture.getSuccessiveWeek();
            //处理单双周
//            for (int i = 0; i < successiveWeek.length(); i++) {
//                String one = successiveWeek.substring(i, i + 1);
//                char ar = one.charAt(0);
//                if (ar == '单') {
//                    single_result = true;
//                    weekTogether = currentWeek % 2 == 0 ? double_result : single_result;
//                }
//                if (ar == '双') {
//                    double_result = true;
//                    weekTogether = currentWeek % 2 == 0 ? double_result : single_result;
//                }
//            }
            if (successiveWeek.indexOf('单') != -1) {
                single_result = true;
                weekTogether = currentWeek % 2 == 0 ? double_result : single_result;
            }
            if (successiveWeek.indexOf('双') != -1) {
                double_result = true;
                weekTogether = currentWeek % 2 == 0 ? double_result : single_result;
            }

            int commaCount = commaCount(successiveWeek);
            //处理有逗号的情况
            if (commaCount != 0) {
                //一个循环处理一个逗号前面的连续周次
                for (int j = 0; j < commaCount + 1; j++) {
                    int commaIndex = successiveWeek.indexOf(",");
                    int beginIndex = 0;
                    int endIndex = commaIndex;
                    if (j == commaCount) {
                        endIndex = successiveWeek.length();
                    }
                    String nextString = successiveWeek.substring(beginIndex, endIndex);
                    int hyphenIndex = nextString.indexOf("-");
                    //截取逗号之后的字段
                    successiveWeek = successiveWeek.substring(commaIndex + 1);

                    if (hyphenIndex != -1) {
                        String afterHyphen = nextString.substring(hyphenIndex + 1);
                        int minWeek = Integer.parseInt(StringUtils.substringBefore(nextString, "-"));
                        int maxWeek = theNumberInString(afterHyphen);
                        if (currentWeek >= minWeek && currentWeek <= maxWeek && weekTogether) {
                            result = true;
                        }
                    } else {//处理单独的数字
                        int theweek = theNumberInString(nextString);
                        if (theweek == currentWeek && weekTogether) {
                            result = true;
                        }
                    }
                }
            } else {
                //没有逗号分隔的情况
                int minWeek = 0;
                int maxWeek = 0;
                //有“-”分隔符
                if (successiveWeek.indexOf("-") != -1) {
                    minWeek = Integer.parseInt(StringUtils.substringBefore(successiveWeek, "-"));
                    String afterHyphen = successiveWeek.substring(successiveWeek.indexOf("-"));
                    maxWeek = theNumberInString(afterHyphen);
                    if (currentWeek >= minWeek && currentWeek <= maxWeek && weekTogether) {
                        result = true;
                    }
                } else {
                    //没有“-”分隔符
                    int theweek = theNumberInString(successiveWeek);
                    if (theweek == currentWeek && weekTogether) {
                        result = true;
                    }
                }
            }
            //周次不符合，过滤数据
            if (result == false || !weekTogether) {
                iterator.remove();
            }
        }
        return lectureList;
    }

    public int commaCount(String s) {
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            String one = s.substring(i, i + 1);
            char ar = one.charAt(0);
            if (ar == '，' || ar == ',') {
                count++;
            }
        }
        return count;
    }

    /**
     * @param s
     * @return 返回字符串中的数字
     */
    public int theNumberInString(String s) {
        String result = "";
        for (int i = 0; i < s.length(); i++) {
            String one = s.substring(i, i + 1);
            char ar = one.charAt(0);
            if (ar >= '0' && ar <= '9') {
                result += ar;
            }
        }
        int maxWeek = Integer.parseInt(result);
        return maxWeek;
    }

}
