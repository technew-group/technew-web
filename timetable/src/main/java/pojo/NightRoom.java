package pojo;

public class NightRoom {
    private Integer id;

    private String classroom;

    private Integer floor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClassroom() {
        return classroom;
    }

    @Override
    public String toString() {
        return "NightRoom{" +
                "id=" + id +
                ", classroom='" + classroom + '\'' +
                ", floor=" + floor +
                '}';
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom == null ? null : classroom.trim();
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }
}