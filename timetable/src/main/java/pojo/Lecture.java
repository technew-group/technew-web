package pojo;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.annotation.JSONType;

@JSONType(orders = {"courseName", "faculty", "majorClass", "place", "successiveWeek", "teacherName", "week", "timePoint"})
public class Lecture implements Comparable{

    @JSONField(serialize = false)
    private Integer id;

    private String courseName;

    private String teacherName;

    private String majorClass;

    private String faculty;

    private String successiveWeek;
    private Integer week;


    private Integer timePoint;

    @JSONField(serialize = false)
    private Integer successivePoint;

    private String place;

    @JSONField(serialize = false)
    private Integer floor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName == null ? null : courseName.trim();
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName == null ? null : teacherName.trim();
    }

    public String getMajorClass() {
        return majorClass;
    }

    public void setMajorClass(String majorClass) {
        this.majorClass = majorClass == null ? null : majorClass.trim();
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty == null ? null : faculty.trim();
    }

    public String getSuccessiveWeek() {
        return successiveWeek;
    }

    public void setSuccessiveWeek(String successiveWeek) {
        this.successiveWeek = successiveWeek == null ? null : successiveWeek.trim();
    }

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }

    public Integer getTimePoint() {
        return timePoint;
    }

    public void setTimePoint(Integer timePoint) {
        this.timePoint = timePoint;
    }

    public Integer getSuccessivePoint() {
        return successivePoint;
    }

    public void setSuccessivePoint(Integer successivePoint) {
        this.successivePoint = successivePoint;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place == null ? null : place.trim();
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    @Override
    public String toString() {
        return "Lecture{" +
                "id=" + id +
                ", courseName='" + courseName + '\'' +
                ", teacherName='" + teacherName + '\'' +
                ", majorClass='" + majorClass + '\'' +
                ", faculty='" + faculty + '\'' +
                ", successiveWeek='" + successiveWeek + '\'' +
                ", week=" + week +
                ", timePoint=" + timePoint +
                ", place='" + place + '\'' +
                ", floor=" + floor +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        return false;
    }

    @Override
    public int compareTo(Object o) {
        Lecture l = (Lecture) o;
        if (this.getWeek() > l.getWeek()) {
            return 1;
        } else if (this.getWeek() == l.getWeek()) {
            if (this.getTimePoint() > l.getTimePoint()) {
                return 1;
            } else if (this.getTimePoint() == l.getTimePoint()) {
                return 0;
            } else {
                return -1;
            }
        }
        return -1;
    }
}