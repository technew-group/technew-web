package serviceimpl;


import dao.ClassroomMapper;
import dao.LectureMapper;
import dao.NightRoomMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import pojo.*;
import service.ClassroomService;
import tools.WeekCompareTool;
import vo.ClassroomVO;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


@Service("classroomService")
public class ClassroomServiceImpl implements ClassroomService {
    @Resource
    ClassroomMapper classroomMapper;
    @Resource
    LectureMapper lectureMapper;
    @Resource
    NightRoomMapper nightRoomMapper;

    //查询有课的教室
    @Override
    public List<Lecture> getClassroom(ClassroomVO classroom) {

        LectureExample example = new LectureExample();
        LectureExample.Criteria criteria = example.createCriteria();
        //地点名是否为空
        if (StringUtils.isNotBlank(classroom.getPlace())) {
            criteria.andPlaceLike(classroom.getPlace());
        }
        //楼层是否为空
        if (null != classroom.getFloor()) {
            criteria.andFloorEqualTo(classroom.getFloor());
        }
        //周几是否为空
        if (null != classroom.getDayOfWeek()) {
            criteria.andWeekEqualTo(classroom.getDayOfWeek());
        }
        if (null != classroom.getTimeCase()) {
            criteria.andTimePointEqualTo(classroom.getTimeCase());

        }

        List<Lecture> list = lectureMapper.selectByExample(example);
        list = new WeekCompareTool().weekFilter(list, classroom.getCurrentWeek());

        //在集合中加入连上四节的课
        list = fourclassFilder(list, classroom);

        return list;
    }

    //查询符合要求的所有教室
    @Override
    public List<Classroom> getAllClassroom(ClassroomVO classroom) {
        ClassroomExample example = new ClassroomExample();
        ClassroomExample.Criteria criteria = example.createCriteria();
        if (StringUtils.isNotBlank(classroom.getPlace())) {
            criteria.andPlaceLike(classroom.getPlace());
        }
        if (null != classroom.getFloor()) {
            criteria.andFloorEqualTo(classroom.getFloor());
        }

        List<Classroom> list = classroomMapper.selectByExample(example);
        return list;
    }

    //查询指定地点的晚自习教室
    @Override
    public List<NightRoom> getNightRoom(ClassroomVO classroom) {
        NightRoomExample example = new NightRoomExample();
        NightRoomExample.Criteria criteria = example.createCriteria();
        criteria.andClassroomLike(classroom.getPlace()).andFloorEqualTo(classroom.getFloor());

        return nightRoomMapper.selectByExample(example);
    }

    //查询符合要求的空教室
    @Override
    public List<String> getEmptyClassroom(ClassroomVO classroom) {
        //空教室集合
        List<String> emptyList = new ArrayList<String>();
        //所有教室集合
        List<Classroom> allList = this.getAllClassroom(classroom);
        //将所有空教室添加到空教室集合中
        for (Classroom classroom1 : allList) {
            emptyList.add(classroom1.getPlace());
        }
        List<NightRoom> nigthroomList = null;
        //在 9 10节或者晚自习才会去处理晚自习教室
        if (classroom.getTimeCase() == 9 || classroom.getTimeCase() == 11) {
            nigthroomList = this.getNightRoom(classroom);
        }

        if (classroom.getTimeCase() != 11) {
            //所有有课教室集合
            List<Lecture> list = this.getClassroom(classroom);
            for (Lecture lecture : list) {
                if (emptyList.contains(lecture.getPlace())) {
                    emptyList.remove(lecture.getPlace());
                }
            }
            if (classroom.getTimeCase() == 9) {
                for (NightRoom nightRoom : nigthroomList) {
                    if (emptyList.contains(nightRoom.getClassroom())) {
                        emptyList.remove(nightRoom.getClassroom());

                    }
                }
            }
        } else {
            for (NightRoom nightRoom : nigthroomList) {
                if (emptyList.contains(nightRoom.getClassroom())) {
                    emptyList.remove(nightRoom.getClassroom());

                }
            }
        }
        return emptyList;
    }

    //返回一个包含四连课的有课集合
    @Override
    public List<Lecture> fourclassFilder(List<Lecture> list, ClassroomVO classroom) {
        LectureExample example = new LectureExample();
        LectureExample.Criteria criteria = example.createCriteria();
        criteria.andSuccessivePointEqualTo(4);

        //获得数据库中所有连上四节的课
        List<Lecture> fourclassLectures = lectureMapper.selectByExample(example);
        Iterator<Lecture> iterator = fourclassLectures.iterator();
        //如果用户查上午的课
        if (classroom.getTimeCase() <= 4) {
            while (iterator.hasNext()) {
                //将下午的四连课从集合中删除
                if (iterator.next().getTimePoint() == 5) {
                    fourclassLectures.remove(iterator);
                }
            }
            //如果用户查下午的课
        } else if (classroom.getTimeCase() >= 5) {
            while (iterator.hasNext()) {
                //将上午的四连课从集合中删除
                if (iterator.next().getTimePoint() == 1) {
                    fourclassLectures.remove(iterator);
                }
            }
        }
        list.addAll(fourclassLectures);
        return list;
    }


}
