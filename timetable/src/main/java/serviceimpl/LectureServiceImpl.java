package serviceimpl;

import dao.LectureMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import pojo.Lecture;
import pojo.LectureExample;
import service.LectureService;
import tools.ClassCompareTool;
import tools.WeekCompareTool;

import javax.annotation.Resource;
import java.util.List;

/**
 * 有关课表服务的实现
 *
 * @author 吕圣业
 * @since 6 十一月 2018
 */
@Service("lectureService")
public class LectureServiceImpl implements LectureService {

    @Resource
    private LectureMapper lectureMapper;

    @Override
    public List<Lecture> getLectures(Lecture lecture, int currentWeek) {
        //默认没有"-"的专业都是瞎写的，用户瞎写了个专业，不进行逻辑处理，直接返回
        if (StringUtils.isNotBlank(lecture.getMajorClass()) && lecture.getMajorClass().indexOf("-") == -1) {
            return null;
        }

        List<Lecture> originalLectures = getOriginalLectures(lecture);
        originalLectures = weekFilter(originalLectures, currentWeek);

        if (StringUtils.isNotBlank(lecture.getMajorClass())) {
            String currentClass = lecture.getMajorClass();
            originalLectures = majorClassFilter(originalLectures, currentClass);
        }
        return originalLectures;
    }

    @Override
    public List<Lecture> getOriginalLectures(Lecture lecture) {
        //知道currentWeek和（courseName,place,majorClass,teacherName）
        LectureExample example = new LectureExample();
        example.setOrderByClause("week asc");
        LectureExample.Criteria criteria = example.createCriteria();

        if (StringUtils.isNotBlank(lecture.getCourseName())) {
            criteria.andCourseNameLike("%" + lecture.getCourseName() + "%");
         }
        if (StringUtils.isNotBlank(lecture.getTeacherName())) {
            criteria.andTeacherNameEqualTo(lecture.getTeacherName());
        }
        if (StringUtils.isNotBlank(lecture.getPlace())) {
            String place = lecture.getPlace().toUpperCase();
            criteria.andPlaceLike("%" + place + "%");
        }
        if (StringUtils.isNotBlank(lecture.getMajorClass())) {
            //获取专业前缀，如计算机16-2，prefix=计算机16，根据prefix模糊查
            String prefix = lecture.getMajorClass().substring(0, lecture.getMajorClass().indexOf("-"));
            criteria.andMajorClassLike('%' + prefix + '%');
        }
        return lectureMapper.selectByExample(example);
    }

    @Override
    public List<Lecture> weekFilter(List<Lecture> lectureList, int currentWeek) {
        WeekCompareTool tool = new WeekCompareTool();
        lectureList = tool.weekFilter(lectureList, currentWeek);
        return lectureList;
    }

    /**
     * 将符合条件的课程和专业进行匹配，返回该专业的课程
     * @param lectureList
     * @param currentClass
     * @return
     */
    @Override
    public List<Lecture> majorClassFilter(List<Lecture> lectureList, String currentClass) {
        ClassCompareTool tool = new ClassCompareTool();
        lectureList = tool.majorClassFilter(lectureList, currentClass);
        return lectureList;
    }

    @Override
    public Lecture queryById(Integer id) {
        return lectureMapper.selectByPrimaryKey(id);
    }
}
