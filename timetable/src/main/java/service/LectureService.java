package service;

import pojo.Lecture;

import java.util.List;

/**
 * 提供有关课表的服务
 *
 * @author 吕圣业
 * @since 5 十一月 2018
 */

public interface LectureService {

    /**
     * 组装调用其他方法，返回最终结果
     *
     * @param lecture
     * @return
     */
    List<Lecture> getLectures(Lecture lecture, int currentWeek);

    /**
     * 在数据库中初步过滤，
     * 根据lecture，courseName，place，majorClass（模糊查）,teacherName过滤
     *
     * @param lecture
     * @return List<Lecture>
     */
    List<Lecture> getOriginalLectures(Lecture lecture);

    /**
     * 过滤周次，处理单双周，解析successive_week字段
     *
     * @param lectureList
     * @param currentWeek
     * @return
     */
    List<Lecture> weekFilter(List<Lecture> lectureList, int currentWeek);

    /**
     * 过滤班级
     *
     * @param lectureList
     * @return
     */
    List<Lecture> majorClassFilter(List<Lecture> lectureList, String currentClass);

    Lecture queryById(Integer id);

}
