package service;

import pojo.Classroom;
import pojo.Lecture;
import pojo.NightRoom;
import vo.ClassroomVO;

import java.util.List;

/**
  @author zhangll
  @since 2018.11.5
 */
/*
 查空教室的服务类
 */
public interface ClassroomService {

    //从lectrue表中查出符合要求的有课的教室

    /**
     * 前端封装的参数
     * @Param ClassroomVO
     *  返回所有有课教室名的集合
     * @return List
     */
    public List<Lecture> getClassroom(ClassroomVO classroom);

    //从classroom表中返回指定地点的所有教室
    /**
     * 前端封装的参数
     * @Param ClassroomVO
     *  返回所有教室名的集合
     * @return List
     */
    public List<Classroom> getAllClassroom(ClassroomVO classroom);


    //返回所有空教室的集合
    public  List<String> getEmptyClassroom(ClassroomVO classroom);

    //返回指定地点的晚自习空教室
    public  List<NightRoom> getNightRoom(ClassroomVO classrom);

    /*
     * 作为最后整合函数
     * 主要功能是将上午1234节和下午5678节有四节连课的课加入有课教室的集合
     * @author 崔荣来
     * */
    public List<Lecture> fourclassFilder(List<Lecture> lectureList,ClassroomVO classroom);
}
