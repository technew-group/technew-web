import org.junit.Test;
import tools.ExcelTool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class InsertExcelTest {
    @Test
    public void getExcelTest() {
        ExcelTool excel=new ExcelTool();
        try {
            System.out.println("---------------");
            System.out.println(excel.getExcel("C:\\Users\\asus\\Desktop\\新建文件夹\\1.xls"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Test
    public void importTest(){
        Connection conn=null;
        String url="jdbc:mysql://127.0.0.1:3306/technew_study?useUnicode=true&characterEncoding=utf-8";
        String username="root";
        String password="root";
        String sql;
        PreparedStatement ps=null;

        try {
            //1.加载驱动
            Class.forName("com.mysql.jdbc.Driver");
            //2.获取连接
            conn= DriverManager.getConnection(url,username,password);
            //3.执行sql语句
            ExcelTool excel=new ExcelTool();
            sql=excel.getExcel("C:\\Users\\asus\\Desktop\\新建文件夹\\其他.xls");
            System.out.println(sql);
            System.out.println("--------------------------");
            ps=conn.prepareStatement(sql);
            ps.execute();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
