import org.junit.Test;
import pojo.Lecture;
import tools.ClassCompareTool;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClassCompareTest {
    @Test
    public void majorClassFilterTest() {
        List<Lecture> list = new ArrayList<>();
//        Lecture l1 = new Lecture();
//        l1.setMajorClass("中加计算机18-1、3");
//        list.add(l1);
//        Lecture l2 = new Lecture();
//        l2.setMajorClass("城管18-1-3风景18-1-3城规18-1财汇s2018");
//        list.add(l2);
        Lecture l3 = new Lecture();
        l3.setMajorClass("测绘15-1-3");
        list.add(l3);

        ClassCompareTool tool = new ClassCompareTool();
        tool.majorClassFilter(list, "测绘15-1");
        System.out.println("符合的有" + list.size() + "个");
//        System.out.println(list.get(0).getMajorClass());
        System.out.println(list);
    }

    @Test
    public  void parseAllTest() {
        String str = "城管16-1-3风景18-1-3城规18-1财汇s2018";
        ClassCompareTool tool = new ClassCompareTool();
        Map<Integer,String> map=tool.parseAll(str);
        System.out.println(map.size());
        System.out.println(map.get(1));
    }
}
