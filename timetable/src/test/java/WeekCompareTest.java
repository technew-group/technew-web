import org.junit.Test;
import pojo.Lecture;
import tools.WeekCompareTool;

import java.util.ArrayList;
import java.util.List;

public class WeekCompareTest {
    @Test
    public void weekFilterTest(){
        List<Lecture> lectureList=new ArrayList<>();
        List<Lecture> list;

        Lecture l1=new Lecture();
        l1.setSuccessiveWeek("10-18周");
        lectureList.add(l1);
        Lecture l2=new Lecture();
        l2.setSuccessiveWeek("1-11双周");
        lectureList.add(l2);
        Lecture l3=new Lecture();
        l3.setSuccessiveWeek("2-13周上");
        lectureList.add(l3);

        WeekCompareTool tool=new WeekCompareTool();
        list=tool.weekFilter(lectureList,10);

        System.out.println("符合的有"+list.size()+"个");
        System.out.println(list.get(0).getSuccessiveWeek());
        System.out.println(list.get(1).getSuccessiveWeek());

//        System.out.println(list.get(1).getSuccessiveWeek());
    }
}
