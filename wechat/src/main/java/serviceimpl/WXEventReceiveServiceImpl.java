package serviceimpl;

import me.chanjar.weixin.mp.bean.*;
import org.springframework.stereotype.Service;
import service.WXEventReceiveService;

import java.io.PrintWriter;

@Service
public class WXEventReceiveServiceImpl implements WXEventReceiveService {
    @Override
    public String receivesubscribe(WxMpXmlMessage message) {
        String fromUserName = message.getFromUserName();
        String toUserName = message.getToUserName();
        //文本消息  文本内容
        PrintWriter out = null;
        WxMpXmlOutTextMessage text;
        WxMpXmlOutNewsMessage newsMessage;
        String xml;
        if (message.getEventKey().equals("thyjx") || message.getEventKey().equals("qrscene_thyjx")) {
            WxMpXmlOutNewsMessage.Item item = new WxMpXmlOutNewsMessage.Item();
            item.setPicUrl("http://ayoniusth.com/web/img/yijian.png");
            item.setDescription("同学们有什么关于教学的意见或建议可以在下方填写，天海广纳的老师会及时处理，感谢大家对天海广纳的支持。");
            item.setTitle("天海广纳意见箱");
            item.setUrl("http://v76y0x4ra2ju8hz8.mikecrm.com/YJydWAn");
            newsMessage = WxMpXmlOutNewsMessage.NEWS().toUser(fromUserName).fromUser(toUserName).addArticle(item).build();
            xml = newsMessage.toXml();
        } else if (message.getEventKey().equals("zx") || message.getEventKey().equals("qrscene_zx")) {
            text = WxMpXmlOutTextMessage.TEXT().toUser(fromUserName).fromUser(toUserName).content("[自习时间]\n周一到周五9:00-17:00\n其余时间为上课时间\n\n" +
                    "父子局：首先离开键盘的是儿子\n\n(此课表本学期有效)\n——2019.3.25发布").build();
            xml = text.toXml();
        } else {
            //创建消息文本
            text = WxMpXmlOutTextMessage.TEXT().toUser(fromUserName).fromUser(toUserName).content("你好，欢迎关注AYONIUSTH！").build();
            xml = text.toXml();
        }

        return xml;
    }

    @Override
    public String receiveClick(WxMpXmlMessage message) {
        String fromUserName = message.getFromUserName();
        String toUserName = message.getToUserName();
        //文本消息  文本内容
        String keyName = message.getEventKey();
        PrintWriter out = null;

        if ("寝室报修".equals(keyName)) {
            //创建消息文本
            WxMpXmlOutTextMessage text = WxMpXmlOutTextMessage.TEXT().toUser(fromUserName).fromUser(toUserName).content("点击链接↓填写报修表单，偶尔表单打开时间较长（在1分钟左右），请耐心等待！\n" +
                    "http://koudaigou.net/web/formview/59e0c16ebb7c7c67aea6713d\n" +
                    "  每天20:00之后是大学生服务中心整理数据的时间，这个时间之后请第二天再填写报修单。紧急报修分类请联系门卫室阿姨或者拨打维修建议电话，想知道维修建议电话请回复关键词“维修”。").build();
            String xml = text.toXml();
            return xml;
        }
        return "";
    }


    public String receiveEvent(WxMpXmlMessage message) {
        String event = message.getEvent();
        //点击菜单事件
        String str = "";
        if ("click".equals(event)) {
            str = this.receiveClick(message);
        }
        //关注事件
        else if ("subscribe".equals(event)) {
            str = this.receivesubscribe(message);
        }
        return str;
    }
}
