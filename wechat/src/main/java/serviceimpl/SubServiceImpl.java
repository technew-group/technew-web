package serviceimpl;

import dao.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pojo.User;
import service.SubService;

import javax.annotation.Resource;

@Service
public class SubServiceImpl implements SubService {
    @Autowired
    private UserMapper userMapper;

    /**
     * 用于返回用户的订阅状态
     */
    @Override
    public User getNowSub(User user) {
        user = userMapper.selectByPrimaryKey(user.getId());
        return user;
    }

    /**
     * 用于改变订阅状态
     */
    @Override
    public void updateNewSub(User user, Integer type, Integer flag) {
        if (type == 1) {
            user.setStatusGrade(flag);
        } else {
            user.setStatusSchedule(flag);
        }
        userMapper.updateByPrimaryKey(user);
    }
}

