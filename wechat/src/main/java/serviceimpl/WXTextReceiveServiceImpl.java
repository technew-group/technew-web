package serviceimpl;


import me.chanjar.weixin.mp.bean.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.WxMpXmlOutTextMessage;
import org.springframework.stereotype.Service;
import pojo.Custom;
import pojo.Grade;
import pojo.Send_Grade;
import pojo.User;
import service.GradeService;
import service.MyWxService;
import service.UserService;
import service.WXTextReceiveService;
import wxutils.ConstantUtils;
import wxutils.WxUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class WXTextReceiveServiceImpl implements WXTextReceiveService {

    @Resource
    private UserService userService;
    @Resource
    private GradeService gradeService;
    /**
     * 接收微信用户发送的普通文本消息  并且回复
     */
    @Override
    public String receiveText(WxMpXmlMessage message) {

        String str="";
        String fromUserName = message.getFromUserName();
        String toUserName = message.getToUserName();
        //文本消息  文本内容
        String content = message.getContent();
        User user = userService.isExist(fromUserName);
        if ("绑定".equals(content)) {
            if(null!=user){
                WxMpXmlOutTextMessage text = WxMpXmlOutTextMessage.TEXT().toUser(fromUserName).fromUser(toUserName).content("已经绑定，如需解除绑定,请输入关键词 \"解绑\"").build();
                str = text.toXml();
            }
            else {
                String token = ConstantUtils.MYTOKEN;
                String timestamp = String.valueOf(System.currentTimeMillis());
                String signature = WxUtils.getSignature(token, timestamp);
                //创建消息文本
                WxMpXmlOutTextMessage text = WxMpXmlOutTextMessage.TEXT().toUser(fromUserName).fromUser(toUserName).content("<a href=\"" + ConstantUtils.HTTPURL + "/wx/check.do?signature=" + signature + "&timestamp=" + timestamp + "&openid=" + fromUserName + "\">点我绑定</a>").build();
                str = text.toXml();
            }
        }
        else if("解绑".equals(content)||"解除绑定".equals(content)){
            userService.deleteUserByOpenid(fromUserName);
             WxMpXmlOutTextMessage text = WxMpXmlOutTextMessage.TEXT().toUser(fromUserName).fromUser(toUserName).content("解绑成功,如需再次绑定,请输入关键词 \"绑定\"").build();
             str= text.toXml();
        }
        else if("成绩".equals(content)){

            List<Send_Grade> grades;
            StringBuffer buffer=new StringBuffer();
            if(null == user){
                String token = ConstantUtils.MYTOKEN;
                String timestamp = String.valueOf(System.currentTimeMillis());
                String signature = WxUtils.getSignature(token, timestamp);
                //创建消息文本
                WxMpXmlOutTextMessage text = WxMpXmlOutTextMessage.TEXT().toUser(fromUserName).fromUser(toUserName).content("<a href=\"" + ConstantUtils.HTTPURL + "/wx/check.do?signature=" + signature + "&timestamp=" + timestamp + "&openid=" + fromUserName + "\">点我绑定</a>").build();
                str = text.toXml();
            }
           else {
                grades=gradeService.getGrade(user);
                if(grades == null || grades.isEmpty()) {
                    WxMpXmlOutTextMessage text = WxMpXmlOutTextMessage.TEXT().toUser(fromUserName).fromUser(toUserName).content("本学期成绩尚未更新  请等待！").build();
                    str = text.toXml();
                } else  {
                    WxMpXmlOutTextMessage text = WxMpXmlOutTextMessage.TEXT().toUser(fromUserName).fromUser(toUserName).content("<a href=\" "+ ConstantUtils.HTTPURL + "/grade/wxTextGetGrade.do?openid="+fromUserName+"\" >点我查看成绩</a>").build();
                    str = text.toXml();

                }
            }

        }

        else {
            MyWxService service=new MyWxServiceImpl();
            StringBuffer buffer=new StringBuffer();
            buffer.append(service.getNaneByOpenid(fromUserName)+":");
            buffer.append(content+"\n\n");
            buffer.append(fromUserName);
            Custom custom=new Custom();
            custom.setToUser(ConstantUtils.MANAGET_OPENID);
            custom.setContent(buffer.toString());
            service.sendCustom(custom);
            //创建消息文本
//            WxMpXmlOutTextMessage text = WxMpXmlOutTextMessage.TEXT().toUser(fromUserName).fromUser(toUserName).content("嗯哪，老牛我听见了，我会尽快给你回复滴！").build();
//            str = text.toXml();
        }

        return str;
    }

}
