package serviceimpl;


import com.tools.TimeTool;
import me.chanjar.weixin.common.bean.WxMenu;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.WxMpCustomMessage;
import me.chanjar.weixin.mp.bean.WxMpTemplateMessage;
import me.chanjar.weixin.mp.bean.result.WxMpQrCodeTicket;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import me.chanjar.weixin.mp.bean.result.WxMpUserList;
import pojo.Custom;
import pojo.template.ActivityTemplate;
import pojo.template.CourseTemplate;
import pojo.template.GradeTemplate;
import service.MyWxService;
import wxutils.ConstantUtils;
import wxutils.WxGetObjectUtils;

import java.util.List;

/**
 * @author zhangll
 * 微信业务封装类
 */
public class MyWxServiceImpl implements MyWxService {
    WxMpInMemoryConfigStorage wxMpInMemoryConfigStorage = new WxMpInMemoryConfigStorage();
    WxMpService wxMpService = new WxMpServiceImpl();

    {
        wxMpInMemoryConfigStorage.setAppId(ConstantUtils.APPID);
        wxMpInMemoryConfigStorage.setSecret(ConstantUtils.SECRET);

        wxMpService.setWxMpConfigStorage(wxMpInMemoryConfigStorage);
    }

    /**
     * 获取配置好的 weixin-java-tools中的WXMpService
     */
    @Override
    public WxMpService getService() {
        return wxMpService;
    }

    /**
     * 获取关注全部用户的信息(openid)
     */
    @Override
    public List<String> getAllUserInfo() {

        WxMpService wxMpService = this.getService();
        try {
            WxMpUserList wxMpUserList = wxMpService.userList("");
            return wxMpUserList.getOpenIds();
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 通过用户的openid获取用户名
     */
    @Override
    public String getNaneByOpenid(String openid) {
        String name = "";
        try {
            WxMpUser user = getService().userInfo(openid, "zh_CN");
            if (null != user) {
                name = user.getNickname();
            }
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        return name;
    }


    /**
     * 向微信管理员发送活动通知的模板消息
     */
    @Override
    public boolean sendTemplateForActivityAndForme(ActivityTemplate activityTemplate) {
        boolean flag = true;
        WxMpService wxMpService = this.getService();
        WxMpUser user = null;
        try {
            user = wxMpService.userInfo(ConstantUtils.MANAGET_OPENID, "zh_CN");
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        String name = user.getNickname();
        activityTemplate.setName(name);
        activityTemplate.setToUser(ConstantUtils.MANAGET_OPENID);
        WxMpTemplateMessage wxActivityTemplate = WxGetObjectUtils.getWXActivityTemplate(activityTemplate);
        try {
            wxMpService.templateSend(wxActivityTemplate);
        } catch (WxErrorException e) {
            flag = false;
            e.printStackTrace();
        }
        // }
        return flag;
    }

    /**
     * 向微信所有用户发送活动通知的模板消息
     */
    @Override
    public boolean sendTemplateForActivity(ActivityTemplate activityTemplate) {
        boolean flag = true;
        WxMpService wxMpService = this.getService();
        List<String> toUsers = this.getAllUserInfo();
        for (String str : toUsers) {
            WxMpUser user = null;
            try {
                user = wxMpService.userInfo(str, "zh_CN");
            } catch (WxErrorException e) {
                e.printStackTrace();
            }
            String name = user.getNickname();
            activityTemplate.setName(name);
            activityTemplate.setToUser(str);
            WxMpTemplateMessage wxActivityTemplate = WxGetObjectUtils.getWXActivityTemplate(activityTemplate);
            try {
                wxMpService.templateSend(wxActivityTemplate);
            } catch (WxErrorException e) {
                flag = false;
                e.printStackTrace();
            }
        }
        return flag;
    }


    /**
     * 上课提醒
     *
     * @param courseTemplate
     * @return
     */
    @Override
    public boolean sendTemplateForCourse(CourseTemplate courseTemplate) {
        boolean flag = true;
        WxMpService wxMpService = this.getService();
        try {
            int timepoint= TimeTool.getCurrentTimePoint();
            if(timepoint==5){
                courseTemplate.setBeginTime("13:50");
                courseTemplate.setEndTime("15:15");
            }
            if(timepoint==7){
                courseTemplate.setBeginTime("15:30");
                courseTemplate.setEndTime("16:55");
            }
            WxMpTemplateMessage wxCourseTemplate = WxGetObjectUtils.getWXCourseTemplate(courseTemplate);
            wxMpService.templateSend(wxCourseTemplate);
        } catch (WxErrorException e) {
            e.printStackTrace();
            flag = false;
        }
        return flag;
    }

    /**
     * 向微信用户发送成绩的模板消息
     */
    @Override
    public boolean sendTemplateForGrade(GradeTemplate gradeTemplate) {
        boolean flag = true;
        WxMpService wxMpService = this.getService();
        WxMpTemplateMessage wxGradeTemplate = WxGetObjectUtils.getWXGradeTemplate(gradeTemplate);
        try {
            wxMpService.templateSend(wxGradeTemplate);
        } catch (WxErrorException e) {
            flag = false;
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 向微信用户发送客服消息
     */
    @Override
    public boolean sendCustom(Custom custom) {
        boolean flag = true;
        WxMpService wxMpService = this.getService();
        WxMpCustomMessage wxCustom = WxGetObjectUtils.getWxCustom(custom);
        try {
            wxMpService.customMessageSend(wxCustom);
        } catch (WxErrorException e) {
            flag = false;
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 微信公众号创建菜单
     */
    @Override
    public boolean createMenu() {
        boolean flag = true;
        WxMpService wxMpService = this.getService();
        WxMenu wxMenu = WxGetObjectUtils.getWxMenu();
        try {
            wxMpService.menuCreate(wxMenu);
        } catch (WxErrorException e) {
            flag = false;
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 获得生成的临时带参二维码的地址
     * id:带参二维码的参数
     * second：带参二维码的有效时间
     *
     * @return 返回二维码链接地址
     */
    @Override
    public String getQRcode(String id, String second) {
        String url = "";
        try {
            WxMpQrCodeTicket ticket = getService().qrCodeCreateTmpTicket(Integer.parseInt(id), Integer.parseInt(second) * 86400);
            url = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=" + ticket.getTicket();
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        return url;
    }

    /**
     * 获得生成的永久带参二维码的地址
     * id:带参二维码的参数
     *
     * @return 返回二维码链接地址
     */
    @Override
    public String getQRcode(String id) {
        String url = "";
        try {
            WxMpQrCodeTicket ticket = getService().qrCodeCreateLastTicket(id);
            url = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=" + ticket.getTicket();
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        return url;
    }
}
