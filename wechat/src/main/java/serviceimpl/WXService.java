//package serviceimpl;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONObject;
//import me.chanjar.weixin.common.exception.WxErrorException;
//import org.apache.http.HttpEntity;
//import org.apache.http.client.ClientProtocolException;
//import org.apache.http.client.methods.CloseableHttpResponse;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClients;
//import org.apache.http.util.EntityUtils;
//import pojo.TemplateData;
//import wxutils.WxUtils;
//
//import java.io.IOException;
//import java.io.UnsupportedEncodingException;
//
///**
// * @author  zhangll
// * 这个服务类没什么用   自己实现调用微信接口的方法  weixxin-java-tools jar包中已经封装好了 只需要去调用
// *
// *
//   微信发送客服消息的工具类
//
// */
//public class WXService {
//
//    /*
//    //获取access_token
//       grant_type		获取access_token填写client_credential
//       appid	 	    第三方用户唯一凭证
//       secret	        第三方用户唯一凭证密钥，即appsecret
//     */
//
//    public String getAccessToken() throws UnsupportedEncodingException {
//        String gramt_type = "client_credential";
//        String appid = "wxf01d2bac35e08db8";
//        String secret = "7c9c9dcd8084ede12e079440d169a805";
//        String accessToken = "";
//        //1.使用默认的配置的httpclient
//        CloseableHttpClient client = HttpClients.createDefault();
//        //2.使用get方法
//        HttpGet httpGet = new HttpGet("https://api.weixin.qq.com/cgi-bin/token?" +
//                "grant_type=" + gramt_type + "&appid=" + appid + "&secret=" + secret);
//        CloseableHttpResponse response = null;
//
//        try {
//            //3.执行请求，获取响应
//            response = client.execute(httpGet);
//            //看请求是否成功，这儿打印的是http状态码
//            if (response.getStatusLine().getStatusCode() == 200) {
//                //4.获取响应的实体内容，就是我们所要抓取得网页内容
//                HttpEntity entity = response.getEntity();
//
////            5.将其打印到控制台上面
////            方法一：使用EntityUtils
//                if (entity != null) {
//                    String data = EntityUtils.toString(entity, "utf-8");
//                    JSONObject jsonObject = JSON.parseObject(data);
//                    accessToken = jsonObject.getString("access_token");
//                }
//                EntityUtils.consume(entity);
//            }
//
//        } catch (ClientProtocolException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return accessToken;
//    }
//
//
//    /**
//     *  向微信用户发送模板消息
//     */
//    public void  sendTemplateMessage(String accessToken){
//        //创建模板消息
//        String  template=new TemplateData().
//        setTemplate_id("v-DTX1l0lZNbqJHDFS_tIEBetzfPcJXApmKG-8Kd0oQ").
//        setTouser("oYD-31L8JWIDoyjQPYd1Ap5wCu6o").
//        setUrl("www.baidu.com").
//        add("first","张龙龙","#173177").
//        add("course","高等数学","#173177").
//        add("result","99","#173177").
//        add("end","本次更新了成绩  点击查看详情","#173177").build();
//        System.out.println(template);
//        String url="https://api.weixin.qq.com/cgi-bin/message/template/send?access_token="+accessToken;
//        excutionHttp(url, template);
//    }
//
//
//
//    /**
//     * 向微信用户发送普通会话消息
//     */
//    public void  sendSimpleMessage( String accessToken){
//        String data = "呃呃您的成绩更新了,你的数据库成绩为100";
//        String json = "{\"touser\": \"" + "oYD-31L8JWIDoyjQPYd1Ap5wCu6o"
//                + "\",\"msgtype\": \"text\"," +
//                " \"text\":" +
//                " {\"content\": \"" + data + "\"}}";
//        String url="https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token="+accessToken;
//        excutionHttp(url, json);
//    }
//    /**
//     * 创建自定义菜单
//     */
//    public void  creatMenu(String  accessToken){
//        //调用接口
//        String url="https://api.weixin.qq.com/cgi-bin/menu/create?access_token="+accessToken;
//        //创建自定义菜单 数据格式
//        //创建第一个菜单
//         Button button1=new Button();  //校园生活
//        button1.setName("校园生活");
//         //第一个菜单的子菜单
//         Button  subButton1_1=new ViewButton(); //信息大厅
//         subButton1_1.setName("信息大厅");
//         subButton1_1.setType("view");
//         ((ViewButton) subButton1_1).setUrl("http://www.baidu.com/");
//        Button  subButton1_2=new ClickButton();  //寝室报修
//        subButton1_2.setName("寝室报修");
//        subButton1_2.setType("click");
//        ((ClickButton) subButton1_2).setKey("寝室报修");
//        Button  subButton1_3=new ViewButton();   //what new
//        subButton1_3.setName("Whta's New");
//        subButton1_3.setType("view");
//         ((ViewButton) subButton1_3).setUrl("http://www.baidu.com/");
//
//         button1.setSub_button(new Button[]{subButton1_1,subButton1_2,subButton1_3});
//        //创建第二个菜单
//        Button button2=new Button();  //查Ta课表
//        button2.setName("查Ta课表");
//        //第二个菜单的子菜单
//        Button  subButton2_1=new ViewButton(); //在线图书馆
//        subButton2_1.setName("在线图书馆");
//        subButton2_1.setType("view");
//        ((ViewButton) subButton2_1).setUrl("http://www.baidu.com/");
//
//        Button  subButton2_2=new ViewButton();  //一卡通充值
//        subButton2_2.setName("一卡通充值");
//        subButton2_2.setType("view");
//        ((ViewButton) subButton2_2).setUrl("http://www.baidu.com/");
//
//        Button  subButton2_3=new ViewButton();   //全网视频破解
//        subButton2_3.setName("全网视频破解");
//        subButton2_3.setType("view");
//        ((ViewButton) subButton2_3).setUrl("http://www.baidu.com/");
//
//        Button  subButton2_4=new ViewButton();   //全网视频破解
//        subButton2_4.setName("Ta课表|空教室");
//        subButton2_4.setType("view");
//        ((ViewButton) subButton2_4).setUrl("http://www.baidu.com/");
//        button2.setSub_button(new Button[]{subButton2_1,subButton2_2,subButton2_3,subButton2_4});
//
//        //创建第三个菜单
//        Button button3=new Button();  //联系我们
//        button3.setName("联系我们");
//        //第三个菜单的子菜单
//        Button  subButton3_1=new ViewButton(); //联系我们
//        subButton3_1.setName("联系我们");
//        subButton3_1.setType("view");
//        ((ViewButton) subButton3_1).setUrl("http://www.baidu.com/");
//
//        Button  subButton3_2=new ViewButton();  //关于我们
//        subButton3_2.setName("关于我们");
//        subButton3_2.setType("view");
//        ((ViewButton) subButton3_2).setUrl("http://www.baidu.com/");
//        button3.setSub_button(new Button[]{subButton3_1,subButton3_2});
//
//        Menu menu=new Menu();
//        menu.setButton(new Button[]{button1,button2,button3});
//       String json =JSON.toJSONString(menu);
//        System.out.println(json);
//         excutionHttp(url,json);
//
//
//    }
//    public void excutionHttp(String url, String message) {
//        try {
//            //创建HttpClient
//            CloseableHttpClient httpClient = HttpClients.createDefault();
//            //创建HttpPost
//            HttpPost httpPost = new HttpPost(url );
//            HttpEntity httpEntity = new StringEntity(message, "utf-8");
//            httpPost.setEntity(httpEntity);
//            CloseableHttpResponse response = httpClient.execute(httpPost);
//            System.setProperty("sun.net.client.defaultConnectTimeout", "30000");// 连接超时30秒
//            System.setProperty("sun.net.client.defaultReadTimeout", "30000"); // 读取超时30秒
//            System.out.println(response);
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
//    }
//    public void excutionHttpByGet(String url) {
//        try {
//            //创建HttpClient
//            CloseableHttpClient httpClient = HttpClients.createDefault();
//            //创建HttpPost
//            HttpGet httpPost = new HttpGet(url);
//            CloseableHttpResponse response = httpClient.execute(httpPost);
//            System.setProperty("sun.net.client.defaultConnectTimeout", "30000");// 连接超时30秒
//            System.setProperty("sun.net.client.defaultReadTimeout", "30000"); // 读取超时30秒
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }
//    }
//    public static void main(String[] args) throws UnsupportedEncodingException, WxErrorException {
////        WXService service=new WXService();
////        String  accessToken=service.getAccessToken();
////        System.out.println(accessToken);
////       //service.excutionHttpByGet("https://api.weixin.qq.com/cgi-bin/menu/delete?access_token="+accessToken);
////       service.sendTemplateMessage(accessToken);
////        // service.sendSimpleMessage(accessToken);
////      // service.creatMenu(accessToken);
//
//
////        WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
////        WxMpInMemoryConfigStorage wxMpInMemoryConfigStorage = new  WxMpInMemoryConfigStorage();
////        wxMpInMemoryConfigStorage.setAppId("wxf01d2bac35e08db8");
////        wxMpInMemoryConfigStorage.setSecret("7c9c9dcd8084ede12e079440d169a805");
////        WxMpService wxMpService=new WxMpServiceImpl();
////        wxMpService.setWxMpConfigStorage(wxMpInMemoryConfigStorage);
////        System.out.println(wxMpService.getAccessToken());
////         MyWxServiceImpl myService=new MyWxServiceImpl();
////
////        UserTemplate userTemplate=new UserTemplate();
////        userTemplate.setOpenid("oYD-31L8JWIDoyjQPYd1Ap5wCu6o");
////        userTemplate.setName("张龙龙");
////        userTemplate.setCourseName("高等数学");
////        userTemplate.setScore(99);
////        userTemplate.setContent("hello word");
////        WxMpTemplateMessage template = myService.getTemplate(userTemplate);
////        System.out.println(template.toJson());
////        wxMpService.templateSend(template);
//
//      //  WxMenu wxMenu = myService.getWxMenu();
//       // wxMpService.menuDelete();
//     //   System.out.println(wxMenu.toJson());
//      //  wxMpService.menuCreate(wxMenu);
////        WxMpCustomMessage wxCustom = myService.getWxCustom(userTemplate);
////        wxMpService.customMessageSend(wxCustom);
//        long time=System.currentTimeMillis();
//        String timestamp=String.valueOf(time);
//        String signature = WxUtils.getSignature("weixin", timestamp);
//        System.out.println(WxUtils.check(signature,"weixin",timestamp));
//        MyWxServiceImpl service=new MyWxServiceImpl();
//        System.out.println(service.getAllUserInfo());
//    }
//
//}