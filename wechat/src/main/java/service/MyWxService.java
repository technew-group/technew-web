package service;

import me.chanjar.weixin.mp.api.WxMpService;
import pojo.Custom;
import pojo.template.ActivityTemplate;
import pojo.template.CourseTemplate;
import pojo.template.GradeTemplate;

import java.util.List;

public interface MyWxService {

    /**
     * 获取已配置参数的WxMpService
     * @return
     */
    WxMpService getService();

    /**
     * 获取所有关注的用户的openid的集合
     * @return
     */
    List<String> getAllUserInfo();

    /**
     * 给管理员发送活动模板消息
     * @param activityTemplate
     * @return
     */
    boolean sendTemplateForActivityAndForme(ActivityTemplate activityTemplate);

    /**
     * 给所有用户发送活动模板消息
     * @param activityTemplate
     * @return
     */
    boolean sendTemplateForActivity(ActivityTemplate activityTemplate);

    /**
     * 发送成绩模板消息
     * @param gradeTemplate
     * @return
     */
    boolean sendTemplateForGrade(GradeTemplate gradeTemplate);

    /**
     * 发送客服消息
     * @param custom
     * @return
     */
    boolean sendCustom(Custom custom);

    /**上课提醒
     * @param courseTemplate
     * @return
     */
    boolean  sendTemplateForCourse(CourseTemplate courseTemplate);

    /**
     * 创建菜单
     * @return
     */
    boolean  createMenu();

    /**
     * 通过oenid获取用户网名
     * @param openid
     * @return
     */
    String  getNaneByOpenid(String openid);


    /**
     * 生成来临时的带参二维码地址
     * @param id ：二维码参数
     * @param second：二维码有效时间
     * @return 二维码地址
     */
    String getQRcode(String id,String second);

    /**
     * 生成永久的带参二维码地址
     * @param id ：二维码参数
     * @return 二维码地址
     */
    String getQRcode(String id);
}
