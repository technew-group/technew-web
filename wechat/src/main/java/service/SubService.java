package service;

import org.springframework.stereotype.Service;
import pojo.User;

/**
 *
 * 消息推送订阅接口
 */
public interface SubService {

    /**
     用于返回当前用户的订阅状态
     */
    User getNowSub(User user);


    /**
     用于更改订阅状态
     */
    void updateNewSub(User user,Integer type , Integer flag);

}
