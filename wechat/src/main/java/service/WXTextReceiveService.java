package service;

import me.chanjar.weixin.mp.bean.WxMpXmlMessage;

/**
 *
 * 接受微信用户发送的普通文本消息
 */
public interface WXTextReceiveService {
    /*
       接收用户发送的普通消息
       并且向微信用户发送回复消息  比如关键字回复  被动回复消息
     */
    public String receiveText(WxMpXmlMessage message);
}
