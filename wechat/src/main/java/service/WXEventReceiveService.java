package service;

import me.chanjar.weixin.mp.bean.WxMpXmlMessage;

/**
 * 微信用户发送的推送消息
 */
public interface WXEventReceiveService {

    /*
     关注/取消关注事件
     */
    public String receivesubscribe(WxMpXmlMessage message);

    /*
    自定义菜单事件
    推送事件和跳转链接
     */
    public String receiveClick(WxMpXmlMessage message);

}
