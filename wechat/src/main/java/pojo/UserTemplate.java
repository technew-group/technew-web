package pojo;

public class UserTemplate {
    private  String  openid;  //微信用户唯一的openid
    private  String  name;    //学生姓名
    private  String  courseName;  //课程名
    private  int  score;   //对应的课程成绩
    private  String  content;  //客服消息发送的模板
    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
