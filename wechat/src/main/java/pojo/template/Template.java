package pojo.template;

/**
 * @authoor zhangll
 * 微信模板消息所需实体类
 */
public class Template {
    //微信用户openid
    protected String toUser;
    //定义在微信公众平台的模板id
    protected String template_id;
    //模板消息需要跳转的 url
    protected String url;
    //标题颜色
    protected String topcolor;

    public String getTopcolor() {
        return topcolor;
    }

    public void setTopcolor(String topcolor) {
        this.topcolor = topcolor;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
