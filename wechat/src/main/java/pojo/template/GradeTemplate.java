package pojo.template;

/**
 * 发送成绩的消息模板
 */
public class GradeTemplate extends Template {
    private  String  name; //用户姓名
    private  String courseName; //考试的科目
    private  String  grade;  //考试成绩
    private  String  details; //结束提示语

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    private Builder createBuilder() {
        Builder builder = new Builder();
        return builder;
    }
    public static class Builder{
        private GradeTemplate target;
        public Builder() {
            target = new GradeTemplate();
        }
        public Builder setName(String name){
            target.name = name;
            return this;
        }
        public Builder setCourseName(String courseName){
            target.courseName = courseName;
            return this;
        }

        public Builder setGrade(String grade){
            target.grade = grade;
            return this;
        }
        public Builder setDetails(String details){
            target.details = details;
            return this;
        }
        public Builder setToUser(String user){
            target.toUser = user;
            return this;
        }
        public Builder setUrl(String url){
            target.url = url;
            return this;
        }
        public GradeTemplate build() {
            return target;
        }
    }

}
