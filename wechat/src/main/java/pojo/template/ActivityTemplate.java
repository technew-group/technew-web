package pojo.template;

/**
 * 发送活动通知用的消息模板实体
 */
public class ActivityTemplate extends Template {
    //用户昵称
      private  String name;
    //活动开始
      private  String first;
      //第一个关键字
      private  String keyword1;
      //第一个关键字
     private  String keyword2;
     //第一个关键字
     private  String keyword3;
     //结束
     private  String  remark;

    @Override
    public String toString() {
        return "ActivityTemplate{" +
                "name='" + name + '\'' +
                ", first='" + first + '\'' +
                ", keyword1='" + keyword1 + '\'' +
                ", keyword2='" + keyword2 + '\'' +
                ", keyword3='" + keyword3 + '\'' +
                ", remark='" + remark + '\'' +
                ", toUser='" + toUser + '\'' +
                ", template_id='" + template_id + '\'' +
                ", url='" + url + '\'' +
                ", topcolor='" + topcolor + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getKeyword1() {
        return keyword1;
    }

    public void setKeyword1(String keyword1) {
        this.keyword1 = keyword1;
    }

    public String getKeyword2() {
        return keyword2;
    }

    public void setKeyword2(String keyword2) {
        this.keyword2 = keyword2;
    }

    public String getKeyword3() {
        return keyword3;
    }

    public void setKeyword3(String keyword3) {
        this.keyword3 = keyword3;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
