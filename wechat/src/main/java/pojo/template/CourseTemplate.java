package pojo.template;

/**
 * @author 张龙龙
 * @date 2019/3/3 10:40
 */
public class CourseTemplate extends Template {
    //上课用户姓名
    private String name;
    //所需要上课的科目名称
    private String courseName;
    //上课的开始时间
    private String  beginTime;
    //上课的节次
    private String  timePoint;
    //结束时间
    private String endTime;
    //上课地点
    private String  place;
    //结束语
    private String  remark;

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getTimePoint() {
        return timePoint;
    }

    public void setTimePoint(String timePoint) {
        this.timePoint = timePoint;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    //静态内部类  构造者模式
   public static class Builder{
        CourseTemplate target;
        public Builder(){
            target=new CourseTemplate();
        }
        public Builder setName(String name){
            target.name=name;
            return this;
        }
        public Builder setCourseName(String courseName){
            target.courseName = courseName;
            return this;
        }
        public  Builder setPlace(String  place){
            target.place=place;
            return this;
        }
        public Builder setBeginTime(String beginTime){
            target.beginTime=beginTime;
            return this;
        }
        public Builder setEndTime(String endTime){
            target.endTime=endTime;
            return this;
        }
        public Builder  setTimePoint(String timePoint){
            target.timePoint=timePoint;
            return this;
        }
        public Builder setRemark(String remark){
            target.remark = remark;
            return this;
        }
        public Builder setToUser(String user){
            target.toUser = user;
            return this;
        }
        public Builder setUrl(String url){
            target.url = url;
            return this;
        }
        public CourseTemplate build() {
            return target;
        }
    }
}
