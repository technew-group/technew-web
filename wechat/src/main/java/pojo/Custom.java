package pojo;

/**
 * @authoor  zhangll
 * 微信客服消息所需实体类
 */
public class Custom {
    private String toUser; //微信用户唯一标识
    private String msgtype; //发送客服消息类型  文本为text
    private String content; //发送的文本消息类容

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public String getMsgtype() {
        return msgtype;
    }

    public void setMsgtype(String msgtype) {
        this.msgtype = msgtype;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
