package pojo;

import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.result.WxMpUserList;
import org.junit.jupiter.api.Test;
import pojo.template.GradeTemplate;
import service.MyWxService;
import service.UserService;
import serviceimpl.MyWxServiceImpl;
import serviceimpl.UserServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class sendTest {
    @Test
    public void send() {
        MyWxService myWxService = new MyWxServiceImpl();
        WxMpService wxMpService = new WxMpServiceImpl();
        UserService userService = new UserServiceImpl();
        try {
            WxMpUserList list = wxMpService.userList(null);
            List<String> opplist = list.getOpenIds();
            List<User> userList = userService.getAllUsers();
            List<String> sendlist = new ArrayList<String>();
            boolean flag = true;
            for (int i = 0; i < opplist.size(); i++) {
                flag = true;
                for (int j = 0; j < userList.size(); i++) {
                    if (userList.get(j).getOpenId().equals(opplist.get(i))) {
                        flag = false;
                    }
                }
                if (flag == true) {
                    sendlist.add(opplist.get(i));
                }
            }
            for (int i = 0; i < sendlist.size(); i++) {
                if (opplist.get(i) != null) {
                    GradeTemplate template = new GradeTemplate.Builder()
                            .setToUser(opplist.get(i))
                            .setName("检测到您未绑定教务网")
                            .setCourseName("如果想要开通此功能")
                            .setGrade("请回复关键字[绑定]")
                            .build();

                    myWxService.sendTemplateForGrade(template);
                }
            }
        } catch (WxErrorException e) {
            e.printStackTrace();
        }

    }

}
