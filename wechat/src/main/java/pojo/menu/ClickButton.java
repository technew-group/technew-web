package pojo.menu;

/**
 * 点击菜单  继承于菜单
 */
public class ClickButton  extends Button {
    private  String  key; //点击菜单  key值会发送给服务器 可以根据key值 与用户进行交互

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
