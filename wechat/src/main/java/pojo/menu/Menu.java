package pojo.menu;

/**
 * 菜单  多个子菜单的集合
 */
public class Menu {
    private Button[] button;

    public void setButton(Button[] button) {
        this.button = button;
    }

    public Button[] getButton() {
        return button;
    }
}
