package pojo.menu;
/**
 * 用来自定义菜单使用（菜单父类）
 */
public class Button {
    private String name; //菜单名
    private String type;  //菜单的类型  常用有两种（view ，click）
    private Button[] sub_button; //菜单下的子菜单  子菜单个数不超过5个

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Button[] getSub_button() {
        return sub_button;
    }

    public void setSub_button(Button[] sub_button) {
        this.sub_button = sub_button;
    }
}
