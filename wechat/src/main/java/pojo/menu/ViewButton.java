package pojo.menu;

/**
 * 跳转菜单  继承于Button
 */
public class ViewButton extends Button {
    private  String  url; //点击菜单跳转的页面

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
