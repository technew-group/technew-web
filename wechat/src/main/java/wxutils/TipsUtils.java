package wxutils;


/**
 * @author 张龙龙
 * @date 2019/4/5 14:34
 *
 *  模板消息提示语
 */
public class TipsUtils {
    static String[] tips=new String[]{
            "点击查看今日课程安排",
            "「寝室报修」：在菜单左边，直连总务处维修处理快！",
            "「一卡通充值」：在菜单中间，记得及时充钱哟~",
            "「在线图书馆」：在菜单中间，大多数阅览室晚上也能借书，详情参考「开放时间」",
            "「在线图书馆」：在菜单中间，「馆藏查询」受限于学校服务器有点慢，请耐心等待",
            "「VIP视频破解」：在菜单中间，不开VIP也能随便看爱腾优VIP视频",
            "「查Ta课表」：在菜单中间，学霸用来蹭课，情侣拿来查岗，单身狗与男神女神制造偶遇",
            "「查空教室」：在菜单中间，一键查找空教室，不用楼上楼下来回跑",
            "「铁牛校历」：在菜单右边，看看啥时候放假方便买回家的票",
            "「牛工智能」：在菜单右边，这个聊天机器人是你没玩过的“船新”版本",
            "「科大影讯」:在菜单右边，看看最近上映了啥电影，还可以顺便买票"
              };
     static int i=0;
    public static String getTip(){
        String tip=tips[i];
        i= ++i<tips.length?i:0;
        return tip;
}
//    public static void main(String[] args) {
//
//        for(int j=0;j<10000;j++){
//            System.out.println(getTip());
//
//        }
//    }
}
