package wxutils;

import java.security.MessageDigest;
import java.util.Arrays;

/**
 * @author  zhangll
 * 微信工具类
 */
public class WxUtils {

     //通过Sha1方法 进行加密
    public static String getSha1(String str){
        if(str==null||str.length()==0){
            return null;
        }
        char hexDigits[] = {'0','1','2','3','4','5','6','7','8','9',
                'a','b','c','d','e','f'};
        try {
            MessageDigest mdTemp = MessageDigest.getInstance("SHA1");

            mdTemp.update(str.getBytes("UTF-8"));
            byte[] md = mdTemp.digest();

            int j = md.length;

            char buf[] = new char[j*2];

            int k = 0;

            for (int i = 0; i < j; i++) {

                byte byte0 = md[i];

                buf[k++] = hexDigits[byte0 >>> 4 & 0xf];

                buf[k++] = hexDigits[byte0 & 0xf];

            }

            return new String(buf);

        } catch (Exception e) {

            // TODO: handle exception

            return null;

        }

    }
    //根据 token timestamp生成一个签名
    public static String getSignature(String  token,String  timestamp){
        String[] array={token,timestamp};
        Arrays.sort(array);
        StringBuffer buffer=new StringBuffer();
        for(String  str:array){
            buffer.append(str);
        }
       return WxUtils.getSha1(buffer.toString());
    }
    //对签名进行验证
    public static Boolean check(String signature,String  token,String timestamp){
        String mySignature = WxUtils.getSignature(token, timestamp);
        if(signature!=null&&signature.equals(mySignature)){
            return true;
        }
        return  false;
    }
}
