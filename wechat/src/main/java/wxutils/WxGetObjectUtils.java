package wxutils;




import me.chanjar.weixin.common.bean.WxMenu;

import me.chanjar.weixin.mp.bean.WxMpCustomMessage;
import me.chanjar.weixin.mp.bean.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.WxMpTemplateMessage;
import pojo.Custom;
import pojo.template.ActivityTemplate;
import pojo.template.CourseTemplate;
import pojo.template.GradeTemplate;


import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static java.net.URLEncoder.encode;

/**
 * @author  zhangll
 * 此类是一个工具类 用来封装对象
 */
public class WxGetObjectUtils {

    /**
       封装成绩模板消息对象
     * @param gradeTemplate
     * @return
     */
    public static WxMpTemplateMessage getWXGradeTemplate(GradeTemplate gradeTemplate){
        WxMpTemplateMessage template=new WxMpTemplateMessage();
        template.setToUser(gradeTemplate.getToUser());
        template.setTemplateId(ConstantUtils.TEMPLATE_ID_GRADE);
        template.setUrl(gradeTemplate.getUrl());
        List<WxMpTemplateData> datas = template.getDatas();
        WxMpTemplateData dataName=new WxMpTemplateData("first","哈喽，"+gradeTemplate.getName()+"\n又公布了一门成绩，快来看看吧！\n","#173177");
        WxMpTemplateData dataCourse=new WxMpTemplateData("keyword1",gradeTemplate.getCourseName(),"#173177");
        WxMpTemplateData dataScore=new WxMpTemplateData("keyword2",gradeTemplate.getGrade(),"#173177");
        WxMpTemplateData dataEnd=new WxMpTemplateData("remark","\n点击查看全部成绩","#173177");
        datas.add(dataCourse);
        datas.add(dataName);
        datas.add(dataScore);
        datas.add(dataEnd);
        return template;
     }

    /**
     * 封装上课提醒所需要的WxMpTemplateMessage
     * @param courseTemplate
     * @return
     */
    public static WxMpTemplateMessage getWXCourseTemplate(CourseTemplate courseTemplate){
        WxMpTemplateMessage template=new WxMpTemplateMessage();
        template.setToUser(courseTemplate.getToUser());
        template.setTemplateId(ConstantUtils.TEMPLATE_ID_COURSE);
        template.setUrl(courseTemplate.getUrl());
        List<WxMpTemplateData> datas = template.getDatas();
        WxMpTemplateData dataName=new WxMpTemplateData("first","牛仔["+courseTemplate.getName()+"],待会儿你有课哟！\n","#173177");
        WxMpTemplateData dataCourseName=new WxMpTemplateData("keyword1",courseTemplate.getCourseName(),"#173177");
        WxMpTemplateData dataTime=new WxMpTemplateData("keyword2",courseTemplate.getBeginTime()+"-"+courseTemplate.getEndTime(),"#173177");
        WxMpTemplateData dataPlace=new WxMpTemplateData("keyword3",courseTemplate.getPlace(),"#173177");
        WxMpTemplateData dataEnd=new WxMpTemplateData("remark","\n"+TipsUtils.getTip(),"#173177");
        datas.add(dataName);
        datas.add(dataCourseName);
        datas.add(dataTime);
        datas.add(dataPlace);
        datas.add(dataEnd);
        return template;
    }
    /**
     封装活动模板消息对象
     * @param activityTemplate
     * @return
     */
    public static WxMpTemplateMessage getWXActivityTemplate(ActivityTemplate activityTemplate){
        WxMpTemplateMessage template=new WxMpTemplateMessage();
        template.setToUser(activityTemplate.getToUser());
        template.setTemplateId(ConstantUtils.TEMPLATE_ID_ACTIVITY);
        System.out.println(activityTemplate.getUrl());
        template.setUrl(activityTemplate.getUrl());
        List<WxMpTemplateData> datas = template.getDatas();
        WxMpTemplateData first=new WxMpTemplateData("first","哈喽，"+activityTemplate.getName()+"\n" +
                "明天就是四六级考试了，每年都有不少同学弄丢准考证从而查不到成绩，因此「哎哟铁牛」推出了准考证防丢失服务。\n","");
        WxMpTemplateData keyword1=new WxMpTemplateData("keyword1","未绑定","#173177");
        WxMpTemplateData keyword2=new WxMpTemplateData("keyword2","未绑定","#173177");
        WxMpTemplateData keyword3=new WxMpTemplateData("keyword3","预计2019年2月下旬","#173177");
        WxMpTemplateData  remark=new WxMpTemplateData("remark","\n回复关键词“四六级”进行绑定\n祝你明天牛气冲天，四六级躺过！","#173177");
       datas.add(first);
        datas.add(keyword1);
        datas.add(keyword2);
        datas.add(keyword3);
        datas.add(remark);
        return template;
    }
    /**
     * 封装客服消息对象
     *
     */
    public static WxMpCustomMessage getWxCustom(Custom custom){
        WxMpCustomMessage customMessage=new WxMpCustomMessage();
        customMessage.setToUser(custom.getToUser());
        customMessage.setMsgType("text");
        customMessage.setContent(custom.getContent());
        return customMessage;
    }
    /**
      封装菜单对象
      借助于第三方 可以界面化创建微信公众号菜单
      本方法发用于实现 代码创建微信公众号菜单
       */
    public static  WxMenu getWxMenu(){
        //第一个菜单
        WxMenu.WxMenuButton button1=   new WxMenu.WxMenuButton(); //校园生活菜单
        button1.setName("校园生活");

        WxMenu.WxMenuButton button1_0=   new WxMenu.WxMenuButton();//我的成绩
        button1_0.setName("我的成绩");
        button1_0.setType("view");

        String url = ConstantUtils.HTTPSURL;
        try {
            url = encode(ConstantUtils.HTTPSURL,"utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        button1_0.setUrl("https://open.weixin.qq.com/connect/oauth2/authorize?appid="+ConstantUtils.APPID+"&redirect_uri="+url+"/wx/toGrade.do&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect");

        //校园生活的子菜单
        WxMenu.WxMenuButton button1_1=   new WxMenu.WxMenuButton();//信息大厅
        button1_1.setName("信息大厅");
        button1_1.setType("view");
        button1_1.setUrl("https://mp.weixin.qq.com/s/qYFagNcJkQQycVvxXxR6mA");

        WxMenu.WxMenuButton button1_2=   new WxMenu.WxMenuButton(); //寝室报修
        button1_2.setName("寝室报修");
        button1_2.setType("click");
        button1_2.setKey("寝室报修");

        WxMenu.WxMenuButton button1_3=   new WxMenu.WxMenuButton();  //what ‘s
        button1_3.setName("What's New");
        button1_3.setType("view");
        button1_3.setUrl("http://mp.weiweixiao.net/index.php/Wap/Index/columns.html?token=3q09tJTp6BGAAAAWPwAVGQ&id=ii463qby6BGAAAAWPg36Gg");

        WxMenu.WxMenuButton button1_4=   new WxMenu.WxMenuButton();  //考研座位
        button1_4.setName("考研座位");
        button1_4.setType("view");
        button1_4.setUrl("http://mp.weixin.qq.com/s?__biz=MzU3Mjc0NzYxMA==&mid=100000166&idx=1&sn=a311bea665784a4e1305ffdc39333319&chksm=7ccd7b934bbaf2854c52b0653b947e716de725422b35063081f522b58aee6a3a856fb1a959c7&scene=18#wechat_redirect");


        List<WxMenu.WxMenuButton>  list=new ArrayList<WxMenu.WxMenuButton>();
        list.add(button1_0);
        list.add(button1_1);
        list.add(button1_4);
        list.add(button1_2);
        list.add(button1_3);
        button1.setSubButtons(list);
        //第二个菜单
        WxMenu.WxMenuButton button2=   new WxMenu.WxMenuButton(); //查TA课表
        button2.setName("查TA课表");
        //查TA课表的子菜单

        WxMenu.WxMenuButton button2_1=   new WxMenu.WxMenuButton();//在线图书馆
        button2_1.setName("在线图书馆");
        button2_1.setType("view");
        button2_1.setUrl("http://mp.weiweixiao.net/index.php/Wap/Index/columns.html?token=3q09tJTp6BGAAAAWPwAVGQ&id=lJL9kaLp6BGAAAAWPg36Gg&code=f35553d98cb33d4c3b346a1a8d10f18d/");

        WxMenu.WxMenuButton button2_2=   new WxMenu.WxMenuButton(); //一卡通充值
        button2_2.setName("一卡通充值");
        button2_2.setType("view");
        button2_2.setUrl("http://h5.axinfu.com/login/inputMobile");

        WxMenu.WxMenuButton button2_3=   new WxMenu.WxMenuButton();  //全网视频破解
        button2_3.setName("全网视频破解");
        button2_3.setType("view");
        button2_3.setUrl("http://www.eb89.com/web.php?id=EAbNWca/");

        WxMenu.WxMenuButton button2_4=   new WxMenu.WxMenuButton();  //Ta课表|空教室
        button2_4.setName("Ta课表|空教室");
        button2_4.setType("view");
        button2_4.setUrl("http://www.heikeqingshu.xyz/web/timetable.html");

        list=new ArrayList<WxMenu.WxMenuButton>();

        list.add(button2_1);
        list.add(button2_2);
//        list.add(button2_3);
        list.add(button2_4);
        button2.setSubButtons(list);

        //第3个菜单
        WxMenu.WxMenuButton button3=   new WxMenu.WxMenuButton(); //联系我们
        button3.setName("联系我们");
        //校园生活的子菜单
        WxMenu.WxMenuButton button3_1=   new WxMenu.WxMenuButton();//联系我们
        button3_1.setName("联系我们");
        button3_1.setType("view");
        button3_1.setUrl("https://mp.weixin.qq.com/s/K94Zw3taH_fywrWcQNXudA");

        WxMenu.WxMenuButton button3_2=   new WxMenu.WxMenuButton(); //关于我们
        button3_2.setName("关于我们");
        button3_2.setType("view");
        button3_2.setUrl("https://mp.weixin.qq.com/s/U-HYxkk8hqm3bdTue2UxtA");
        list=new ArrayList<WxMenu.WxMenuButton>();
        list.add(button3_1);
        list.add(button3_2);
        button3.setSubButtons(list);
        WxMenu  menu=new WxMenu();
        list=new ArrayList<WxMenu.WxMenuButton>();
        list.add(button1);
        list.add(button2);
        list.add(button3);
        menu.setButtons(list);
        return menu;
    }

}
