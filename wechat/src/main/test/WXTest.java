import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.WxMpServiceImpl;
import org.junit.jupiter.api.Test;
import pojo.template.CourseTemplate;
import service.MyWxService;
import serviceimpl.MyWxServiceImpl;

public class WXTest {
    @Test
    public void courseTemplateTest(){
        MyWxService service=new MyWxServiceImpl();
        CourseTemplate template=new CourseTemplate.Builder()
                .setToUser("oWs3z1FywzfIkF5Wljy6kTjHtOj8")
                .setName("张龙龙")
                .setCourseName("高等数学")
                .setBeginTime("8.30")
                .setEndTime("10.00")
                .setPlace("科厦")
                .build();

        service.sendTemplateForCourse(template);
    }
}
