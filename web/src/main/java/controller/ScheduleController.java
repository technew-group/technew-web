package controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pojo.Schedule;
import pojo.User;
import service.ScheduleService;
import service.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

/**
 * 上课提醒Controller
 *
 * @author 吕圣业
 * @since 3 三月 2019
 */
@Controller
@RequestMapping("/schedule")
public class ScheduleController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduleController.class);

    @Resource
    private UserService userService;
    @Resource
    private ScheduleService scheduleService;

    /**
     * 通过(模板，关键字)链接跳转，绑定之后跳转
     *
     * @param request
     * @param model
     * @return
     */
    @RequestMapping("/getSchedule")
    public String getSchedule(HttpServletRequest request, Model model) {

        User user = (User) request.getSession().getAttribute("user");

        List<Schedule> schedules = scheduleService.getCurrentDaySchedule(user);
            Object[] courses = new Object[5];
//            schedules.forEach(s->{
//                //0,1,2,3,4->1,3,5,7,9(映射)
//                courses[s.getTimePoint()/2]=s;
//            });
        if(schedules!=null){
            for (int i = schedules.size() - 1; i >= 0; i--) {
                courses[schedules.get(i).getTimePoint() / 2] = schedules.get(i);
            }
        }
            model.addAttribute("courses", courses);
            return "myclass";
    }

    /**
     * 上课提醒模板消息的url授权回调后跳转
     */
    @RequestMapping("/wxTextGetSchedule")
    public String getSchedule(String openid, Model model) {
        User user = userService.isExist(openid);
        List<Schedule> schedules = scheduleService.getCurrentDaySchedule(user);
            Object[] courses = new Object[5];
//            schedules.forEach(s->{
//                //0,1,2,3,4->1,3,5,7,9(映射)
//                courses[s.getTimePoint()/2]=s;
//            });
            //倒序遍历集合
        if(schedules!=null) {
            for (int i = schedules.size() - 1; i >= 0; i--) {
                courses[schedules.get(i).getTimePoint() / 2] = schedules.get(i);
            }
        }
            model.addAttribute("courses", courses);
            return "myclass";
    }

    /**
     * 根据下拉选框选择任意一天的课表（一星期内）
     */
    @RequestMapping(value = "/getanydaySchedule")
    @ResponseBody
    public List<Schedule> getanydaySchedule( Integer weekday,String openid) {

        User user = new User();
        user.setOpenId(openid);

        List<Schedule> schedules = scheduleService.getCurrentDaySchedule(user, weekday);
        Schedule schedule = new Schedule();
        schedule.setPlace(" ");
        schedule.setCourseName(" ");
        Schedule[] courses = new Schedule[5];
        if(schedules!=null){
        for (int i = schedules.size() - 1; i >= 0; i--) {
            courses[schedules.get(i).getTimePoint() / 2] = schedules.get(i);
        }
        }
        for (int j = 0; j < 5; j++) {
            if (courses[j] == null){
                courses[j] = schedule;
            }
        }
        return Arrays.asList(courses);
    }
}
