package controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pojo.Grade;
import pojo.Send_Grade;
import pojo.User;
import service.GradeService;
import service.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 成绩查询Controller，user一定是在库中存在的且能验证通过
 *
 * @author 吕圣业
 * @since 4 十二月 2018
 */
@Controller
@RequestMapping("/grade")
public class GradeController {
    @Resource
    private GradeService gradeService;
    @Resource
    private UserService userService;

    /**
     * 绑定关键字时调用
     *
     * @param session
     * @return
     */
    @RequestMapping("/getGrade")
    public String getGrade(HttpSession session, Model model) {
        User user = (User) session.getAttribute("user");
        List<Grade> grades = gradeService.getGrade(user);
        if (grades == null || grades.isEmpty()) {
            return "redirect:/nograde.html";
        } else {
            model.addAttribute("user", user);
            model.addAttribute("grades", grades);
            return "currentgrade";
        }
    }

    /**
     * 通过链接跳转，获取所有成绩
     *
     * @param user
     * @return
     */
    @RequestMapping("/getAllGrade")
    public String getAllGrade(User user, Model model) {
        List<Grade> grades = gradeService.getAllGrade(user);
        if (grades == null || grades.isEmpty()) {
            return "redirect:/nograde.html";
        } else {
            model.addAttribute("grades", grades);
            return "currentgrade";
        }
    }

    /**
     * 点我查看成绩链接或菜单中我的成绩时跳转
     *
     * @param openid
     * @return
     */
    @RequestMapping("/wxTextGetGrade")
    public String getGrade(String openid, Model model) {
        User user = userService.isExist(openid);
        List<Grade> grades = gradeService.getGrade(user);
        if (grades == null || grades.isEmpty()) {
            return "redirect:/nograde.html";
        } else {
            model.addAttribute("user", user);
            model.addAttribute("grades", grades);
            return "currentgrade";
        }
    }

}
