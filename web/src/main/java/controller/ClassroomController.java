package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import service.ClassroomService;
import vo.ClassroomVO;

import javax.annotation.Resource;
import java.util.List;

import static com.alibaba.fastjson.JSON.toJSONString;

/**
 * 空教室controller
 */
@Controller
@RequestMapping("/classroom")
public class ClassroomController {

    @Resource
    private ClassroomService classroomService;

    //获取空教室和有课的教室
    @RequestMapping(value = "/getClassroom", produces = {"application/json;charset=UTF-8;"})
    @ResponseBody
    public List<String> getClassroom(ClassroomVO classroomVO) {

        classroomVO.setPlace(classroomVO.getPlace() + "%");
        if (classroomVO.getTimeCase() % 2 == 0) {
            classroomVO.setTimeCase(classroomVO.getTimeCase() - 1);
        }
        List<String> list = classroomService.getEmptyClassroom(classroomVO);
        return list;
    }
}
