package controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pojo.User;
import service.ScheduleService;
import service.UserService;
import serviceimpl.UserServiceImpl;
import wxutils.ConstantUtils;
import wxutils.WxUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/wx")
/**
 *
 */
public class WXBindController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WXBindController.class);
    @Resource
    private UserService userService;
    @Resource
    private ScheduleService scheduleService;

    @RequestMapping("/bind")
    public String bind(HttpServletRequest request, User user, Model model,String openid){

        String state=  (String) request.getSession().getAttribute("state");
        user.setOpenId(openid);
        User userAccess=userService.isAccess(user);
        if(userAccess!=null){
            if(state ==null || "2".equals(state)) {
                scheduleService.getCurrentDaySchedule(userAccess);
                request.getSession().setAttribute("user", userAccess);
                return "redirect:/schedule/getSchedule.do";
            }
            else {
                request.getSession().setAttribute("user", userAccess);
                return "redirect:/grade/getGrade.do";
            }

        }else{
            model.addAttribute("user",user);
            model.addAttribute("message","用户名或密码错误！");
            request.getSession().setAttribute("123456","123456");
            return "forward:/bind.jsp";
        }
    }
    @RequestMapping("/check")
    public  String  check(HttpServletRequest request){
        String signature=request.getParameter("signature");
        String timestamp=request.getParameter("timestamp");
        String token= ConstantUtils.MYTOKEN;
        long time=Long.valueOf(timestamp);
        boolean flag= WxUtils.check(signature,token,timestamp);
        long time1=System.currentTimeMillis()-time;
        if(flag){
            if(time1<600000){
                request.getSession().setAttribute("openid",request.getParameter("openid"));
                return "redirect:/bind.jsp";
            }
        }
        return "redirect:/error.html";

    }

}
