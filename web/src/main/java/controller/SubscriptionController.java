package controller;

import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pojo.User;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;


/**
 * 用来订阅或取消订阅消息推送
 */

@Controller
@RequestMapping("wx")
public class SubscriptionController {
    @Autowired
    private service.SubService SubService;
    /**
     * 用于订阅或取消订阅的方法
     *
     * @param type 推送内容，1：成绩推送，2：上课提醒
     * @param flag 订阅状态，0：取消订阅，1：订阅
     */
    @RequestMapping("/updateSub")
    @ResponseBody
    public String updateSub(HttpServletRequest request, int type, int flag) {
         flag=flag^1;
        User user = (User) request.getSession().getAttribute("user");
        SubService.updateNewSub(user, type, flag);
        return "success";
    }


    /**
     * 前端ajax请求，没有参数
     * 返回此用户的当前订阅状态，返回Json串
     */
    @RequestMapping(value="/getSub",produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getSub(HttpServletRequest request, HttpServletResponse response) {
        response.setCharacterEncoding("UTF-8");
        User user= (User) request.getSession().getAttribute("user");
        user = SubService.getNowSub(user);
        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("name",user.getName());
        jsonObject.addProperty("flag1",user.getStatusGrade());
        jsonObject.addProperty("flag2",user.getStatusSchedule());
        return jsonObject.toString();
    }
}
