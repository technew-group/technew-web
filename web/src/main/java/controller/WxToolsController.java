package controller;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pojo.User;
import pojo.template.ActivityTemplate;
import service.MyWxService;
import service.UserService;

import serviceimpl.MyWxServiceImpl;
import wxutils.ConstantUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


/**
 * @author zhangll
 */
@Controller
@RequestMapping("/wx")
public class WxToolsController {

    @Resource
    private UserService userService;

    /**
     * 微信工具入口  微信授权回调路径
     * 网页授权  跳转成绩页面 获取用户openid
     *
     * @param request
     * @return
     */
    @RequestMapping("/toGrade")
    public String grade(HttpServletRequest request) {
        String code = request.getParameter("code");
        //1是成绩页面的请求，2是课表的请求
        String state = request.getParameter("state");
        request.getSession().setAttribute("state", state);
        WxMpInMemoryConfigStorage configStorage = new WxMpInMemoryConfigStorage();
        configStorage.setAppId(ConstantUtils.APPID);
        configStorage.setSecret(ConstantUtils.SECRET);
        WxMpService service = new WxMpServiceImpl();
        service.setWxMpConfigStorage(configStorage);
        try {
            if (null != code) {
                WxMpOAuth2AccessToken wxMpOAuth2AccessToken = service.oauth2getAccessToken(code);
                String openid = wxMpOAuth2AccessToken.getOpenId();
                System.out.println(openid + "================");
                User user = userService.isExist(openid);
                request.getSession().setAttribute("openid", openid);
                if (user != null && "1".equals(state)) {
                    request.getSession().setAttribute("user", user);
                    return "redirect:/grade/getGrade.do";
                } else if (user != null && "2".equals(state)) {
                    request.getSession().setAttribute("user", user);
                    return "redirect:/schedule/getSchedule.do";
                }
            }
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        return "redirect:/bind.jsp";
    }

    /**
     * 发送活动消息模板
     *
     * @param template
     * @param url
     * @param select
     * @return
     */
    @RequestMapping("/activity")
    public String sendActtivity(ActivityTemplate template, String url, int select, Model model) {
        template.setUrl(url);
        MyWxServiceImpl myWxService = new MyWxServiceImpl();
        boolean b = false;
        if (select == 1) {
            b = myWxService.sendTemplateForActivityAndForme(template);
        } else if (select == 2) {
            b = myWxService.sendTemplateForActivity(template);
        } else {
            b = false;
        }
        if (b) {
            model.addAttribute("template", template);
            model.addAttribute("message", "发送成功");
            return "forward:/SendTemplate.jsp";
        } else {
            model.addAttribute("message", "发送失败");
            return "forward:/SendTemplate.jsp";
        }
    }

    /**
     * 微信工具入口  微信授权回调路径
     * 网页授权  跳转成绩页面 获取用户openid
     *
     * @param request
     * @return
     */
    @RequestMapping("/toSchedule")
    public String schedule(HttpServletRequest request) {
        String code = request.getParameter("code");
        WxMpInMemoryConfigStorage configStorage = new WxMpInMemoryConfigStorage();
        configStorage.setAppId(ConstantUtils.APPID);
        configStorage.setSecret(ConstantUtils.SECRET);
        WxMpService service = new WxMpServiceImpl();
        service.setWxMpConfigStorage(configStorage);
        try {
            if (null != code) {
                WxMpOAuth2AccessToken wxMpOAuth2AccessToken = service.oauth2getAccessToken(code);
                String openid = wxMpOAuth2AccessToken.getOpenId();
                System.out.println(openid + "================");
                User user = userService.isExist(openid);
                request.getSession().setAttribute("openid", openid);
                if (user != null) {
                    request.getSession().setAttribute("user", user);
                    return "redirect:/schedule/wxTextGetSchedule";
                }
            }
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        return "redirect:/bind.jsp";
    }

    /**
     * 微信工具入口  微信授权回调路径
     * 网页授权  跳转订阅中心页面 获取用户openid
     *
     * @param request
     * @return
     */
    @RequestMapping("/toSubscribe")
    public String subscribe(HttpServletRequest request) {
        String code = request.getParameter("code");
        WxMpInMemoryConfigStorage configStorage = new WxMpInMemoryConfigStorage();
        configStorage.setAppId(ConstantUtils.APPID);
        configStorage.setSecret(ConstantUtils.SECRET);
        WxMpService service = new WxMpServiceImpl();
        service.setWxMpConfigStorage(configStorage);
        try {
            if (null != code) {
                WxMpOAuth2AccessToken wxMpOAuth2AccessToken = service.oauth2getAccessToken(code);
                String openid = wxMpOAuth2AccessToken.getOpenId();
                System.out.println(openid + "================");
                User user = userService.isExist(openid);
                request.getSession().setAttribute("openid", openid);
                if (user != null) {
                    request.getSession().setAttribute("user", user);
                    return "redirect:/subscribe.html";
                }
            }
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        return "redirect:/bind.jsp";
    }

    /**
     * 用于生成带参二维码的登录判断
     *
     * @param username 用户名
     * @param pass     密码
     * @return
     */
    @RequestMapping("/qrlogin")
    public String qrlogin(String username, String pass) {
        if (username.equals("Admin") && pass.equals("ayoniusth123")) {
            return "getQR";
        } else {
            return "qr.jsp";
        }
    }

    /**
     * 获得临时带参二维码
     *
     * @param id
     * @param second
     * @return
     */
    @RequestMapping("/gettemqr")
    public String getqr(@RequestParam String id, @RequestParam String second) {
        MyWxService wxMpService = new MyWxServiceImpl();
        String url = "";
        url = wxMpService.getQRcode(id, second);
        return "redirect:" + url;
    }

    @RequestMapping("/getperqr")
    public String getqr(String id) {
        MyWxService wxMpService = new MyWxServiceImpl();
        String url = "";
        url = wxMpService.getQRcode(id);
        return "redirect:" + url;
    }
}
