package controller;


import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.WxMpXmlMessage;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import service.WXEventReceiveService;
import service.WXTextReceiveService;
import wxutils.ConstantUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 微信入口  与用户交互
 */
@Controller
@RequestMapping("/wx")
public class WXController {

    @Resource
    private WXEventReceiveService wxEventReceiveService;
    @Resource
    private WXTextReceiveService wxTextReceiveService;

    //微信配置服务器 验证
    @RequestMapping(value = "/wxserver", method = {RequestMethod.GET})
    public void check(HttpServletRequest request, HttpServletResponse response) {
        //微信服务器get传递的参数
        String signature = request.getParameter("signature");
        String timestamp = request.getParameter("timestamp");
        String nonce = request.getParameter("nonce");
        String echostr = request.getParameter("echostr");

        //微信工具类
        WxMpService wxService = new WxMpServiceImpl();
        //注入token的配置参数
        /**
         * 生产环境 建议将WxMpInMemoryConfigStorage持久化
         */
        WxMpInMemoryConfigStorage wxConfigProvider = new WxMpInMemoryConfigStorage();
        //注入token值
        wxConfigProvider.setToken(ConstantUtils.ACCESS_TOKEN);//设置token
        wxService.setWxMpConfigStorage(wxConfigProvider);
        boolean flag = wxService.checkSignature(timestamp, nonce, signature); //验证token跟微信配置的是否一样
        PrintWriter out = null;
        try {
            out = response.getWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (flag) {
            out.print(echostr);
        }
        out.close();
    }

    //接受微信用户发过来的消息 和触发的事件
//    @RequestMapping(value="/wxserver",method={RequestMethod.POST})
    @ResponseBody()
    @RequestMapping(value = "/wxserver", produces = {"application/xml;charset=UTF-8;"}, method = {RequestMethod.POST})
    public String receive(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //设置字符集  统一使用UTF-8
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        //因为微信服务器返回的是xml  所以我们需要解析
        //获取消息流
        WxMpXmlMessage message = WxMpXmlMessage.fromXml(request.getInputStream());
        //消息类型
        String messageType = message.getMsgType();
        String res;
        if ("text".equals(messageType)) {
            res = wxTextReceiveService.receiveText(message);
            return res;
        } else if ("event".equals(messageType)) {
            if (message.getEvent().equals("subsubscribe") || message.getEvent().equals("SCAN")) {
                res = wxEventReceiveService.receivesubscribe(message);
            } else {
                res = wxEventReceiveService.receiveClick(message);
            }
            return res;
        }
        return "";
    }
}
