package controller;

import com.alibaba.fastjson.JSON;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import pojo.Lecture;
import service.LectureService;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * 查课表Controller
 *
 * @author 吕圣业
 * @since 4 十二月 2018
 */
@Controller
@RequestMapping("/lecture")
public class LectureController {

    @Resource
    private LectureService lectureService;

    @RequestMapping(value = "/getLecture", produces = "application/json; charset=utf-8",method = RequestMethod.POST)
    @ResponseBody
    public String getLecture(ModelAndView model, Lecture lecture, int currentWeek) {

        System.out.println(lecture+"----------");
        List<Lecture> lectureList = lectureService.getLectures(lecture, currentWeek);
        String jsonResult = "[]";
        if (lectureList != null) {
            Collections.sort(lectureList);
            jsonResult = JSON.toJSONString(lectureList);
        }
        return jsonResult;
    }
}

