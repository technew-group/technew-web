package biz;

import com.tools.TimeTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import pojo.*;
import pojo.template.CourseTemplate;
import service.*;
import serviceimpl.MyWxServiceImpl;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 负责课程爬取，发送模板消息
 *
 * @author 吕圣业
 * @since 3 十二月 2018
 */
@Service("schedulePushWorm")
public class SchedulePushWorm {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchedulePushWorm.class);

    @Resource
    private UserService userService;
    @Resource
    private ScheduleService scheduleService;


    /**
     * 每天凌晨执行，若当天课程不存在，则爬取本周所有课程
     */
    @Scheduled(cron = "0 0 1-6 * * ?")
    public void getAllSchedule() {
        List<User> users = userService.getUsersForSchedule();

        users.forEach(user -> {
            //获取当天的课
            List<Schedule> schedules = scheduleService.getCurrentWeekSchedule(user);
        });
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 日常成绩推送
     */
    @Scheduled(cron = "0 30 7-19 * * ?")
    public void getCurrentSchedule() {
        int count = 0;
        List<User> users = userService.getUsersForSchedule();
        //条件集合,节次
        ArrayList<Integer> timePoints = new ArrayList<>();
        int timePoint = TimeTool.getCurrentTimePoint();
        timePoints.add(timePoint);
        MyWxService service = new MyWxServiceImpl();

        for (User user : users) {

            //获取当天的课
            List<Schedule> schedules = scheduleService.getCurrentDaySchedule(user);
            if (schedules != null && !schedules.isEmpty()) {
                //结果集合
                List<Schedule> scheduleResult = null;
                scheduleResult = schedules.stream()
                        .filter((Schedule s) -> timePoints.contains(s.getTimePoint()))
                        .collect(Collectors.toList());

                for (Schedule s : scheduleResult) {

                    CourseTemplate template = new CourseTemplate.Builder()
                            .setCourseName(s.getCourseName())
                            .setName(user.getName())
                            .setBeginTime(s.getStartTime())
                            .setPlace(s.getPlace())
                            .setEndTime(s.getEndTime())
                            .setToUser(user.getOpenId())
                            .setUrl("http://ayoniusth.com/web/schedule/wxTextGetSchedule.do?openid=" + user.getOpenId())
                            .build();
                    service.sendTemplateForCourse(template);
                    LOGGER.info("template count:{},user:{}", ++count, user);
                }
            }
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
