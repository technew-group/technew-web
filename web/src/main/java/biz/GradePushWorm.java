package biz;

import constant.Constant;
import dao.GradeMapper;
import dao.Send_GradeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import pojo.*;
import pojo.template.GradeTemplate;
import service.*;
import serviceimpl.GradeServiceImpl;
import serviceimpl.MyWxServiceImpl;

import javax.annotation.Resource;
import java.util.List;

@Service("gradePushWorm")
public class GradePushWorm {
    private static final Logger LOGGER = LoggerFactory.getLogger(GradePushWorm.class);

    @Resource
    private UserService userService;
    @Resource
    private Send_GradeMapper send_gradeMapper;
    @Resource
    private GradeService gradeService;
    @Resource
    private HttpService httpService;
    @Resource
    private GradeMapper gradeMapper;


    //    @Scheduled(cron = "0 0 0/3 * * ? ")
    public void getAllGrade() {
        //使用python爬取成绩
        gradeService.getAllGradebyjw();

        //获取所有用户
        int count = 0;
        List<User> users = userService.getUsersForGrade();
        if (users.isEmpty() || users == null) {
            LOGGER.info("users is empty");
        } else {
            for (User user : users) {
                //去数据库获取这位用户的所有未发送成绩
                List<Send_Grade> httpGrades = gradeService.getGradeinDB(user);
                LOGGER.info("httpGrades{}", httpGrades);
                if (httpGrades != null && !httpGrades.isEmpty()) {
                    //以下为发送模板消息的代码
                    MyWxService service = new MyWxServiceImpl();
                    for (Send_Grade grade : httpGrades) {
                        GradeTemplate template = new GradeTemplate.Builder()
                                .setCourseName(grade.getCourseName())
                                .setName(user.getName())
                                .setGrade(grade.getScore())
                                .setToUser(user.getOpenId())
                                .setUrl("http://ayoniusth.com/web/grade/wxTextGetGrade.do?openid=" + user.getOpenId())
                                .build();
                        service.sendTemplateForGrade(template);
                        LOGGER.info("send grade is ok" + ++count);
                        //如果成绩输出，将数据库中update字段修改为1
                        grade.setSend(1);
                        send_gradeMapper.updateByPrimaryKey(grade);
                    }
                    ;
                }
            }
            ;
        }
        LOGGER.info("send grade is finished");
    }


    //爬m黑科技用的
//    @Scheduled(cron = "0 0 0 * * ?")
//        @Scheduled(cron = "10 * * * * ?")//测试用
@Scheduled(cron="0 0 0/12 * * ?")
    public void getAllGradeinm() {

        List<User> users = userService.getUsersForGrade();
        users.forEach(user -> {

            String token = httpService.login(user);
            //爬取该用户所有成绩
            List<Grade> httpGrades = null;

            httpGrades = httpService.getCurrentGradeByHttp(token);

            if (httpGrades != null && !httpGrades.isEmpty()) {
                //去库中该用户所有成绩
                GradeExample gradeExample = new GradeExample();
                GradeExample.Criteria criteria = gradeExample.createCriteria();
                criteria.andStuNoEqualTo(user.getStuNo())
                        .andTermEqualTo(Constant.CTERM)
                        .andYearEqualTo(Constant.CYEAR);
                List<Grade> databaseGrades = gradeMapper.selectByExample(gradeExample);

                //httpCurrentGrades是爬取的当前学期成绩，databaseGrades是数据库中当前学期成绩
                //httpCurrentGrades-databaseGrades就是需要更新的成绩
                httpGrades.removeAll(databaseGrades);
                //此时httpCurrentGrades是需要更新的成绩
//                httpCurrentGrades.forEach(grade -> //调模板方法);
                MyWxService service = new MyWxServiceImpl();
                httpGrades.forEach(grade -> {
                    gradeMapper.insert(grade);
                    GradeTemplate template = new GradeTemplate.Builder()
                            .setCourseName(grade.getCourseName())
                            .setName(user.getName())
                            .setGrade(grade.getScore())
                            .setToUser(user.getOpenId())
                            .setUrl("http://ayoniusth.com/web/grade/wxTextGetGrade.do?openid=" + user.getOpenId())
                            .build();
                    service.sendTemplateForGrade(template);
                });
            }
        });
    }
}


