import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pojo.Grade;
import pojo.Schedule;
import pojo.User;
import service.GradeService;
import service.HttpService;
import service.ScheduleService;
import service.UserService;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:applicationContext.xml"})
public class HttpServiceTest {

    @Resource
    private HttpService httpService;
    @Resource
    private GradeService gradeService;
    @Resource
    private UserService userService;

    @Resource
    private ScheduleService scheduleService;

    /**
     * 插入该用户所有成绩
     */
    @Test
    public void insertTest() throws Exception {
        List<User> users=userService.getAllUsers();
        System.out.println(userService.isExist("o7r2v59YtkGdD0N7_b2gY3lVMc3s"));
        for(int i=0;i<users.size();i++){
            String token=httpService.login(users.get(i));
            //获取该用户所有成绩
            List<Grade> grades=httpService.getAllGradeByHttp(token);
            System.out.println(grades.size());
            if(grades!=null&&!grades.isEmpty()){
                gradeService.insertAllGrade(grades);
            }
        }
    }

    @Test
    public void getScheduleUsers(){
        List<User> users=userService.getUsersForSchedule();
        System.out.println(users.size());
//        User user = new User();
//        user.setStuNo("2016024198");
//        List<Schedule> schedules = scheduleService.getCurrentDaySchedule(user);
//        System.out.println(schedules);
    }

    @Test
    public void loginTest(){
        User user=new User();
        user.setPasswd("2016024198");
        user.setStuNo("2016024198");
        String s=httpService.login(user);
        System.out.println(s);

//        List<Grade> grades=httpService.getAllGradeByHttp(s);
//        for (Grade grade : grades) {
//            System.out.println(grade);
//        }
    }
}
