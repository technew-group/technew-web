/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50016
Source Host           : localhost:3306
Source Database       : technew_study

Target Server Type    : MYSQL
Target Server Version : 50016
File Encoding         : 65001

Date: 2019-03-05 10:33:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL auto_increment,
  `stu_no` varchar(255) default NULL,
  `passwd` varchar(255) default NULL,
  `name` varchar(255) default NULL,
  `open_id` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
