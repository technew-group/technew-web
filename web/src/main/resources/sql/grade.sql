/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50016
Source Host           : localhost:3306
Source Database       : technew_study

Target Server Type    : MYSQL
Target Server Version : 50016
File Encoding         : 65001

Date: 2019-03-05 10:34:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `grade`
-- ----------------------------
DROP TABLE IF EXISTS `grade`;
CREATE TABLE `grade` (
  `id` int(11) NOT NULL auto_increment,
  `stu_no` varchar(11) default NULL,
  `course_no` varchar(255) default NULL,
  `year` varchar(255) default NULL,
  `score` varchar(255) default NULL,
  `course_type` varchar(255) default NULL,
  `term` int(11) default NULL,
  `course_name` varchar(255) default NULL,
  `count` varchar(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of grade
-- ----------------------------
