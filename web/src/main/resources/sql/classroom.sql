/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50016
Source Host           : localhost:3306
Source Database       : technew_study

Target Server Type    : MYSQL
Target Server Version : 50016
File Encoding         : 65001

Date: 2019-03-05 10:35:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `classroom`
-- ----------------------------
DROP TABLE IF EXISTS `classroom`;
CREATE TABLE `classroom` (
  `pid` int(11) NOT NULL auto_increment,
  `place` varchar(20) default NULL,
  `floor` int(1) default NULL,
  PRIMARY KEY  (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of classroom
-- ----------------------------
INSERT INTO `classroom` VALUES ('1', '主楼ES0102', '1');
INSERT INTO `classroom` VALUES ('2', '主楼WS0102', '1');
INSERT INTO `classroom` VALUES ('3', '主楼ES0105', '1');
INSERT INTO `classroom` VALUES ('4', '主楼ES0101', '1');
INSERT INTO `classroom` VALUES ('5', '主楼ES0106', '1');
INSERT INTO `classroom` VALUES ('6', '主楼WS0101', '1');
INSERT INTO `classroom` VALUES ('7', '主楼EN0103', '1');
INSERT INTO `classroom` VALUES ('8', '主楼WS0106', '1');
INSERT INTO `classroom` VALUES ('9', '科W109', '1');
INSERT INTO `classroom` VALUES ('10', '主楼EN0101', '1');
INSERT INTO `classroom` VALUES ('11', '主楼WS0105', '1');
INSERT INTO `classroom` VALUES ('12', '科W105', '1');
INSERT INTO `classroom` VALUES ('13', '主楼WN0101', '1');
INSERT INTO `classroom` VALUES ('14', '主楼WN0103', '1');
INSERT INTO `classroom` VALUES ('15', '科S101', '1');
INSERT INTO `classroom` VALUES ('16', '科N106', '1');
INSERT INTO `classroom` VALUES ('17', '科S104', '1');
INSERT INTO `classroom` VALUES ('18', '科W108', '1');
INSERT INTO `classroom` VALUES ('19', '科S108', '1');
INSERT INTO `classroom` VALUES ('20', '科S109', '1');
INSERT INTO `classroom` VALUES ('21', '科N108', '1');
INSERT INTO `classroom` VALUES ('22', '主楼E0218', '2');
INSERT INTO `classroom` VALUES ('23', '主楼ES0201', '2');
INSERT INTO `classroom` VALUES ('24', '主楼ES0202', '2');
INSERT INTO `classroom` VALUES ('25', '主楼ES0205', '2');
INSERT INTO `classroom` VALUES ('26', '主楼ES0206', '2');
INSERT INTO `classroom` VALUES ('27', '主楼WS0205', '2');
INSERT INTO `classroom` VALUES ('28', '科N206', '2');
INSERT INTO `classroom` VALUES ('29', '主楼EN0203', '2');
INSERT INTO `classroom` VALUES ('30', '主楼EN0201', '2');
INSERT INTO `classroom` VALUES ('31', '主楼WS0202', '2');
INSERT INTO `classroom` VALUES ('32', '主楼WS0206', '2');
INSERT INTO `classroom` VALUES ('33', '主楼WN0201', '2');
INSERT INTO `classroom` VALUES ('34', '主楼WN0203', '2');
INSERT INTO `classroom` VALUES ('35', '主楼WS0201', '2');
INSERT INTO `classroom` VALUES ('36', '科S204', '2');
INSERT INTO `classroom` VALUES ('37', '科N201', '2');
INSERT INTO `classroom` VALUES ('38', '科W208', '2');
INSERT INTO `classroom` VALUES ('39', '科S209', '2');
INSERT INTO `classroom` VALUES ('40', '科N212', '2');
INSERT INTO `classroom` VALUES ('41', '科S212', '2');
INSERT INTO `classroom` VALUES ('42', '科W205', '2');
INSERT INTO `classroom` VALUES ('43', '科E206', '2');
INSERT INTO `classroom` VALUES ('44', '科W209', '2');
INSERT INTO `classroom` VALUES ('45', '科S208', '2');
INSERT INTO `classroom` VALUES ('46', '科E202', '2');
INSERT INTO `classroom` VALUES ('47', '科E201', '2');
INSERT INTO `classroom` VALUES ('48', '科E208', '2');
INSERT INTO `classroom` VALUES ('49', '科S201', '2');
INSERT INTO `classroom` VALUES ('50', '主楼E0312', '3');
INSERT INTO `classroom` VALUES ('51', '主楼E0308', '3');
INSERT INTO `classroom` VALUES ('52', '主楼E0313', '3');
INSERT INTO `classroom` VALUES ('53', '科S308', '3');
INSERT INTO `classroom` VALUES ('54', '科S304', '3');
INSERT INTO `classroom` VALUES ('55', '科S301', '3');
INSERT INTO `classroom` VALUES ('56', '科E302', '3');
INSERT INTO `classroom` VALUES ('57', '科E306', '3');
INSERT INTO `classroom` VALUES ('58', '科N301', '3');
INSERT INTO `classroom` VALUES ('59', '科S309', '3');
INSERT INTO `classroom` VALUES ('60', '主楼ES0303', '3');
INSERT INTO `classroom` VALUES ('61', '科N306', '3');
INSERT INTO `classroom` VALUES ('62', '主楼W0321', '3');
INSERT INTO `classroom` VALUES ('63', '主楼W0318', '3');
INSERT INTO `classroom` VALUES ('64', '主楼W0307', '3');
INSERT INTO `classroom` VALUES ('65', '科N312', '3');
INSERT INTO `classroom` VALUES ('66', '主楼EN0301', '3');
INSERT INTO `classroom` VALUES ('67', '科S312', '3');
INSERT INTO `classroom` VALUES ('68', '科E308', '3');
INSERT INTO `classroom` VALUES ('69', '主楼WN0303', '3');
INSERT INTO `classroom` VALUES ('70', '主楼ES0304', '3');
INSERT INTO `classroom` VALUES ('71', '科E301', '3');
INSERT INTO `classroom` VALUES ('72', '主楼W0306', '3');
INSERT INTO `classroom` VALUES ('73', '主楼EN0303', '3');
INSERT INTO `classroom` VALUES ('74', '主楼E0305', '3');
INSERT INTO `classroom` VALUES ('75', '主楼E0309', '3');
INSERT INTO `classroom` VALUES ('76', '主楼E0318', '3');
INSERT INTO `classroom` VALUES ('77', '主楼W0310', '3');
INSERT INTO `classroom` VALUES ('78', '主楼WN0301', '3');
INSERT INTO `classroom` VALUES ('79', '主楼WS0303', '3');
INSERT INTO `classroom` VALUES ('80', '主楼WS0304', '3');
INSERT INTO `classroom` VALUES ('81', '主楼W0313', '3');
INSERT INTO `classroom` VALUES ('82', '主楼E0321', '3');
INSERT INTO `classroom` VALUES ('83', '科0309（高层）', '3');
INSERT INTO `classroom` VALUES ('84', '科0304（高层）', '3');
INSERT INTO `classroom` VALUES ('85', '科0305（高层）', '3');
INSERT INTO `classroom` VALUES ('86', '主楼E0415', '4');
INSERT INTO `classroom` VALUES ('87', '主楼E0418', '4');
INSERT INTO `classroom` VALUES ('88', '科401（高层）', '4');
INSERT INTO `classroom` VALUES ('89', '科405（高层）', '4');
INSERT INTO `classroom` VALUES ('90', '主楼W0411', '4');
INSERT INTO `classroom` VALUES ('91', '主楼E0412', '4');
INSERT INTO `classroom` VALUES ('92', '主楼W0413', '4');
INSERT INTO `classroom` VALUES ('93', '主楼E0405', '4');
INSERT INTO `classroom` VALUES ('94', '主楼W0406', '4');
INSERT INTO `classroom` VALUES ('95', '主楼W0410', '4');
INSERT INTO `classroom` VALUES ('96', '主楼E0408', '4');
INSERT INTO `classroom` VALUES ('97', '主楼W0418', '4');
INSERT INTO `classroom` VALUES ('98', '主楼W0405', '4');
INSERT INTO `classroom` VALUES ('99', '主楼E0409', '4');
INSERT INTO `classroom` VALUES ('100', '主楼EN0401', '4');
INSERT INTO `classroom` VALUES ('101', '科E401', '4');
INSERT INTO `classroom` VALUES ('102', '科E406', '4');
INSERT INTO `classroom` VALUES ('103', '科S401', '4');
INSERT INTO `classroom` VALUES ('104', '科W408', '4');
INSERT INTO `classroom` VALUES ('105', '科S404', '4');
INSERT INTO `classroom` VALUES ('106', '科S408', '4');
INSERT INTO `classroom` VALUES ('107', '主楼W0421', '4');
INSERT INTO `classroom` VALUES ('108', '主楼WN0401', '4');
INSERT INTO `classroom` VALUES ('109', '科N406', '4');
INSERT INTO `classroom` VALUES ('110', '科E408', '4');
INSERT INTO `classroom` VALUES ('111', '科S412', '4');
INSERT INTO `classroom` VALUES ('112', '科S409', '4');
INSERT INTO `classroom` VALUES ('113', '主楼E0421', '4');
INSERT INTO `classroom` VALUES ('114', '科N412', '4');
INSERT INTO `classroom` VALUES ('115', '科N401', '4');
INSERT INTO `classroom` VALUES ('116', '主楼E0520', '5');
INSERT INTO `classroom` VALUES ('117', '主楼E0521', '5');
INSERT INTO `classroom` VALUES ('118', '科501（高层）', '5');
INSERT INTO `classroom` VALUES ('119', '主楼E0509', '5');
INSERT INTO `classroom` VALUES ('120', '科E502', '5');
INSERT INTO `classroom` VALUES ('121', '科E501', '5');
INSERT INTO `classroom` VALUES ('122', '主楼E0513', '5');
INSERT INTO `classroom` VALUES ('123', '主楼E0512', '5');
INSERT INTO `classroom` VALUES ('124', '主楼E0501', '5');
INSERT INTO `classroom` VALUES ('125', '主楼E0505', '5');
INSERT INTO `classroom` VALUES ('126', '科505（高层）', '5');
INSERT INTO `classroom` VALUES ('127', '科S501', '5');
INSERT INTO `classroom` VALUES ('128', '科W505', '5');
INSERT INTO `classroom` VALUES ('129', '科S512', '5');
INSERT INTO `classroom` VALUES ('130', '科S509', '5');
INSERT INTO `classroom` VALUES ('131', '科S508', '5');
INSERT INTO `classroom` VALUES ('132', '主楼E0508', '5');
INSERT INTO `classroom` VALUES ('133', '主楼W0507(云机房)', '5');
INSERT INTO `classroom` VALUES ('134', '科S504', '5');
INSERT INTO `classroom` VALUES ('135', '科W510', '5');
INSERT INTO `classroom` VALUES ('136', '主楼W0513', '5');
INSERT INTO `classroom` VALUES ('137', '主楼W0514', '5');
INSERT INTO `classroom` VALUES ('138', '科E508', '5');
INSERT INTO `classroom` VALUES ('139', '科N501', '5');
INSERT INTO `classroom` VALUES ('140', '科N512', '5');
INSERT INTO `classroom` VALUES ('141', '科E506', '5');
INSERT INTO `classroom` VALUES ('142', '科N506', '5');
INSERT INTO `classroom` VALUES ('143', '科W508', '5');
INSERT INTO `classroom` VALUES ('144', '主楼E0625', '6');
INSERT INTO `classroom` VALUES ('145', '主楼E0613', '6');
INSERT INTO `classroom` VALUES ('146', '主楼W0618', '6');
INSERT INTO `classroom` VALUES ('147', '主楼W0617', '6');
INSERT INTO `classroom` VALUES ('148', '主楼W0611', '6');
INSERT INTO `classroom` VALUES ('149', '主楼W0614', '6');
INSERT INTO `classroom` VALUES ('150', '主楼E0612', '6');
INSERT INTO `classroom` VALUES ('151', '科0602（高层）', '6');
INSERT INTO `classroom` VALUES ('152', '科0601（高层）', '6');
INSERT INTO `classroom` VALUES ('153', '科0617（高层）', '6');
INSERT INTO `classroom` VALUES ('154', '科0612（高层）', '6');
INSERT INTO `classroom` VALUES ('155', '科0613（高层）', '6');
INSERT INTO `classroom` VALUES ('156', '科0614（高层）', '6');
INSERT INTO `classroom` VALUES ('157', '科0611（高层）', '6');
INSERT INTO `classroom` VALUES ('158', '主楼W0707', '7');
INSERT INTO `classroom` VALUES ('159', '主楼W0711', '7');
INSERT INTO `classroom` VALUES ('160', '科0702（高层）', '7');
INSERT INTO `classroom` VALUES ('161', '科0704（高层）', '7');
INSERT INTO `classroom` VALUES ('162', '主楼W0801', '8');
INSERT INTO `classroom` VALUES ('163', '主楼W0807', '8');
INSERT INTO `classroom` VALUES ('164', '主楼W0808', '8');
INSERT INTO `classroom` VALUES ('165', '主楼E0825', '8');
INSERT INTO `classroom` VALUES ('166', '主楼E0812', '8');
INSERT INTO `classroom` VALUES ('167', '主楼E0819', '8');
INSERT INTO `classroom` VALUES ('168', '主楼W0811', '8');
INSERT INTO `classroom` VALUES ('169', '主楼W0814', '8');
INSERT INTO `classroom` VALUES ('170', '主楼W0817', '8');
INSERT INTO `classroom` VALUES ('171', '主楼E0912', '9');
INSERT INTO `classroom` VALUES ('172', '主楼W0901', '9');
INSERT INTO `classroom` VALUES ('173', '主楼W0905', '9');
INSERT INTO `classroom` VALUES ('174', '主楼E0924', '9');
INSERT INTO `classroom` VALUES ('175', '主楼W0906', '9');
INSERT INTO `classroom` VALUES ('176', '主校区实验楼矿业学院机房', '0');
INSERT INTO `classroom` VALUES ('177', '主校区实验楼现代教育技术中心', '0');
INSERT INTO `classroom` VALUES ('178', 'LTE仿真实验室', '0');
INSERT INTO `classroom` VALUES ('179', '主校区实验楼机械学院仿真实验室', '0');
INSERT INTO `classroom` VALUES ('180', '主校区实验楼材料学院机房', '0');
INSERT INTO `classroom` VALUES ('181', '主校区实验楼建工学院机房', '0');
INSERT INTO `classroom` VALUES ('182', '主楼画室1', '0');
INSERT INTO `classroom` VALUES ('183', '主校区实验楼计算机实践基地', '0');
INSERT INTO `classroom` VALUES ('184', '主校区实验楼社工实践基地', '0');
INSERT INTO `classroom` VALUES ('185', '主校区实验楼数学实验室', '0');
INSERT INTO `classroom` VALUES ('186', '主校区实验楼力学专业实验室', '0');
INSERT INTO `classroom` VALUES ('187', '主校区操场操场1', '0');
INSERT INTO `classroom` VALUES ('188', '主校区实验楼机械画室', '0');
INSERT INTO `classroom` VALUES ('189', '主楼画室2', '0');
INSERT INTO `classroom` VALUES ('190', '0', '0');
INSERT INTO `classroom` VALUES ('191', '操场1', '0');
INSERT INTO `classroom` VALUES ('192', '科E402', '4');
INSERT INTO `classroom` VALUES ('193', '主校区实验楼W0603', '6');
INSERT INTO `classroom` VALUES ('194', '主校区实验楼W0608', '6');
