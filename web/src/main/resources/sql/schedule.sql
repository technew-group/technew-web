/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50016
Source Host           : localhost:3306
Source Database       : technew_study

Target Server Type    : MYSQL
Target Server Version : 50016
File Encoding         : 65001

Date: 2019-03-05 10:32:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `schedule`
-- ----------------------------
DROP TABLE IF EXISTS `schedule`;
CREATE TABLE `schedule` (
  `id` int(11) NOT NULL auto_increment,
  `teacher_name` varchar(255) default NULL,
  `course_name` varchar(255) default NULL,
  `place` varchar(255) default NULL,
  `time_point` int(255) default NULL,
  `succesive_week` int(255) default NULL,
  `week` int(11) unsigned zerofill default NULL,
  `end_time` varchar(255) default NULL,
  `start_time` varchar(255) default NULL,
  `stu_no` varchar(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of schedule
-- ----------------------------
