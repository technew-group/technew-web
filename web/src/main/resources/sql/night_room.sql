/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : technew_study

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2019-03-10 14:31:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `night_room`
-- ----------------------------
DROP TABLE IF EXISTS `night_room`;
CREATE TABLE `night_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classroom` varchar(255) DEFAULT NULL,
  `floor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of night_room
-- ----------------------------
INSERT INTO `night_room` VALUES ('1', '主楼EN0301', '3');
INSERT INTO `night_room` VALUES ('2', '主楼EN0303', '3');
INSERT INTO `night_room` VALUES ('3', '主楼E0308', '3');
INSERT INTO `night_room` VALUES ('4', '主楼E0309', '3');
INSERT INTO `night_room` VALUES ('5', '主楼E0409', '4');
INSERT INTO `night_room` VALUES ('6', '主楼E0305', '3');
INSERT INTO `night_room` VALUES ('7', '主楼ES0202', '2');
INSERT INTO `night_room` VALUES ('8', '主楼ES0206', '2');
INSERT INTO `night_room` VALUES ('9', '主楼ES0218', '2');
INSERT INTO `night_room` VALUES ('10', '主楼ES0313', '3');
INSERT INTO `night_room` VALUES ('11', '主楼ES0312', '3');
INSERT INTO `night_room` VALUES ('12', '主楼WS0105', '1');
INSERT INTO `night_room` VALUES ('13', '主楼WS0205', '2');
INSERT INTO `night_room` VALUES ('14', '主楼WS0206', '2');
INSERT INTO `night_room` VALUES ('15', '主楼WS0202', '2');
INSERT INTO `night_room` VALUES ('16', '主楼WS0102', '1');
INSERT INTO `night_room` VALUES ('17', '主楼W0909', '9');
INSERT INTO `night_room` VALUES ('18', '主楼W0910', '9');
INSERT INTO `night_room` VALUES ('19', '主楼W1003', '10');
INSERT INTO `night_room` VALUES ('20', '主楼W1005', '10');
INSERT INTO `night_room` VALUES ('21', '主楼W1010', '10');
INSERT INTO `night_room` VALUES ('22', '主楼W1006', '10');
INSERT INTO `night_room` VALUES ('23', '主楼EN0201', '2');
INSERT INTO `night_room` VALUES ('24', '主楼EN0203', '2');
INSERT INTO `night_room` VALUES ('25', '主楼E0501', '5');
INSERT INTO `night_room` VALUES ('26', '主楼E0505', '5');
INSERT INTO `night_room` VALUES ('27', '主楼E0509', '5');
INSERT INTO `night_room` VALUES ('28', '主楼E0508', '5');
INSERT INTO `night_room` VALUES ('29', '主楼E0512', '5');
INSERT INTO `night_room` VALUES ('30', '主楼E0513', '5');
INSERT INTO `night_room` VALUES ('31', '主楼E0520', '5');
INSERT INTO `night_room` VALUES ('32', '主楼E0521', '5');
INSERT INTO `night_room` VALUES ('33', '主楼E0418', '4');
INSERT INTO `night_room` VALUES ('34', '主楼E0415', '4');
INSERT INTO `night_room` VALUES ('35', '主楼E0412', '4');
INSERT INTO `night_room` VALUES ('36', '科S508', '5');
INSERT INTO `night_room` VALUES ('37', '科S509', '5');
INSERT INTO `night_room` VALUES ('38', '科S512', '5');
INSERT INTO `night_room` VALUES ('39', '主楼EN0101', '1');
INSERT INTO `night_room` VALUES ('40', '主楼EN0103', '1');
INSERT INTO `night_room` VALUES ('41', '主楼W0318', '3');
INSERT INTO `night_room` VALUES ('42', '主楼W0321', '3');
INSERT INTO `night_room` VALUES ('43', '主楼W0310', '3');
INSERT INTO `night_room` VALUES ('44', '主楼W0418', '4');
INSERT INTO `night_room` VALUES ('45', '主楼W0410', '4');
INSERT INTO `night_room` VALUES ('46', '主楼W0413', '4');
INSERT INTO `night_room` VALUES ('47', '主楼W0406', '4');
INSERT INTO `night_room` VALUES ('48', '主楼W0405', '4');
INSERT INTO `night_room` VALUES ('49', '主楼W0411', '4');
INSERT INTO `night_room` VALUES ('50', '主楼W0421', '4');
INSERT INTO `night_room` VALUES ('51', '主楼WN0101', '1');
INSERT INTO `night_room` VALUES ('52', '主楼WN0103', '1');
INSERT INTO `night_room` VALUES ('53', '主楼WN0201', '2');
INSERT INTO `night_room` VALUES ('54', '主楼WN0301', '3');
INSERT INTO `night_room` VALUES ('55', '主楼WN0303', '3');
INSERT INTO `night_room` VALUES ('56', '主楼ES0201', '2');
INSERT INTO `night_room` VALUES ('57', '主楼ES0205', '2');
INSERT INTO `night_room` VALUES ('58', '主楼E0405', '4');
INSERT INTO `night_room` VALUES ('59', '主楼WS0101', '1');
INSERT INTO `night_room` VALUES ('60', ' 主楼W0313', '3');
INSERT INTO `night_room` VALUES ('61', '主楼W0306', '3');
INSERT INTO `night_room` VALUES ('62', '主楼W0307', '3');
INSERT INTO `night_room` VALUES ('63', '主楼WS0201', '2');
INSERT INTO `night_room` VALUES ('64', '主楼ES0102', '1');
INSERT INTO `night_room` VALUES ('65', '主楼ES0106', '1');
INSERT INTO `night_room` VALUES ('66', '科E204', '2');
INSERT INTO `night_room` VALUES ('67', '科E212', '2');
INSERT INTO `night_room` VALUES ('68', '科E312', '3');
