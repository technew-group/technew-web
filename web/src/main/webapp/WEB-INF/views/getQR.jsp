<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2019/4/26
  Time: 13:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>获得带参二维码</title>
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    %>
    <!--<base href=<%=basePath %>>-->
</head>
<body>
<form method="post" action="wx/gettemqr.do">
    生成临时二维码<br>
    临时二维码只能使用数字做参数<br>
    二维码参数：<input type="text" value="" name="id"><br>
    二维码有效时间：<input type="text" value="" name="second">天<br>
    最高30天<br>
    <input type="submit" value="生成临时二维码">
</form>
<form method="get" action="/web/wx/getperqr.do">
    生成永久二维码<br>
    二维码参数：<input type="text" value="" name="id"><br>
    <input type="submit" value="生成永久二维码">
</form>
</body>
</html>
