<%@ page import="pojo.User" %>
<!DOCTYPE html>
<html lang="en">
<%@page pageEncoding="UTF-8" %>
<meta http-equiv="Content-Type"
      content="text/html; charset=utf-8" />

<%
    String openid=(String) session.getAttribute("openid");
%>
<head>
    <title>我的课程</title>

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
      
    


    <style type="text/css">
        body {
            display: flex;
            min-height: 100vh;
            flex-direction: column;
        }

        main {
            flex: 1 0 auto;
        }

        div.card-action {
            padding-left: 600px;
        }

    </style>

    <script type="text/javascript">





    </script>

</head>

<body style="margin:50px">
<br>




<!--<br><br>-->
<!--<div>-->
<!--<blockquote>-->
<!--<font color="#44cef6" size="10">${user.name}同学</font>-->
<!--</blockquote>-->

<!--</div>-->

<!--  遍历从这里开始 -->
<!-- ╭╮ ＿＿＼｜／＿＿╭╮
 │　　　　　　　　~│　　　　
 │　≥　╭———╮　≤　│ 　　
 │／／│０　０│／／.│
 │　　╰—— — ╯　　│
 ╰——┬Ｏ————┬—Ｏ╯　　　　
 　　●│　　　　│
 　　╰│　　Ｏ　│　
 　　╰｜｜-｜｜╯

 每处传参会有标识
-->
<nav class="nav-extended">
    <div class="nav-wrapper">
        <a href="#"
           class="brand-logo"></a>
        <a href="#"
           data-activates="mobile-demo"
           class="button-collapse"><i class="material-icons">
            <font size="6">${user.name}同学</font>
        </i></a>
    </div>

    <div class="nav-content">
        <br><br>

        <!--获取当前星期-->

        <script>


            function GetWeek(id) {

                var trId = id;


                // alert(trId);
                $.ajax({
                    type: 'post',
                    url: '/web/schedule/getanydaySchedule.do?openid=${openid}',
                    data: {
                        weekday: trId
                    },
                    success(showResult1){
                        // alert(showResult1[0].courseName);

                            document.getElementById ("cc0").innerText=showResult1[0].courseName;
                            document.getElementById ("pp0").innerText=showResult1[0].place;
                            document.getElementById ("cc1").innerText=showResult1[1].courseName;
                            document.getElementById ("pp1").innerText=showResult1[1].place;
                            document.getElementById ("cc2").innerText=showResult1[2].courseName;
                            document.getElementById ("pp2").innerText=showResult1[2].place;
                            document.getElementById ("cc3").innerText=showResult1[3].courseName;
                            document.getElementById ("pp3").innerText=showResult1[3].place;
                            document.getElementById ("cc4").innerText=showResult1[4].courseName;
                            document.getElementById ("pp4").innerText=showResult1[4].place;

                    },
                    error: function(
                        XMLHttpRequest,
                        textStatus,
                        errorThrown
                    ) {
                        alert(
                            XMLHttpRequest.readyState +
                            XMLHttpRequest.status +
                            XMLHttpRequest.responseText+
                            "您输入有误或未找到课程！"
                        )
                    }

                })




            }


            // 根据当前时间自动选择星期几
            var week = new Date().getDay();
            window.addEventListener('load', () => {
                switch (week) {
                    case 0:
                        document.getElementById("0").classList.add('active');
                        break;
                    case 1:
                        document.getElementById('1').classList.add('active');
                        break;
                    case 2:
                        document.getElementById("2").classList.add('active');
                        break;
                    case 3:
                        document.getElementById("3").classList.add('active');
                        break;
                    case 4:
                        document.getElementById("4").classList.add('active');
                        break;
                    case 5:
                        document.getElementById("5").classList.add('active');
                        break;
                    case 6:
                        document.getElementById("6").classList.add('active');
                        break;
                }
            })



        </script>

        <ul class="tabs tabs-transparent"
            id="test">
            <div id="i0">
                <li class="tab"><a href="javascript:void(0);"
                                   id="0"
                                   onclick="GetWeek(this.id)">
                    <font size="6">星期天</font>
                </a></li>
            </div>
            <div id="i1">
                <li class="tab"><a href="javascript:void(0);"
                                   id="1"
                                   onclick="GetWeek(this.id)">
                    <font size="6">星期一</font>
                </a></li>
            </div>
            <div id="i2">
                <li class="tab"><a href="javascript:void(0);"
                                   id="2"
                                   onclick="GetWeek(this.id)">
                    <font size="6">星期二</font>
                </a></li>
            </div>
            <div id="i3">
                <li class="tab"><a href="javascript:void(0);"
                                   id="3"
                                   onclick="GetWeek(this.id)">
                    <font size="6">星期三</font>
                </a></li>
            </div>
            <div id="i4">
                <li class="tab"><a href="javascript:void(0);"
                                   id="4"
                                   onclick="GetWeek(this.id)">
                    <font size="6">星期四</font>
                </a></li>
            </div>
            <div id="i5">
                <li class="tab"><a href="javascript:void(0);"
                                   id="5"
                                   onclick="GetWeek(this.id)">
                    <font size="6">星期五</font>
                </a></li>
            </div>
            <div id="i6">
                <li class="tab"><a href="javascript:void(0);"
                                   id="6"
                                   onclick="GetWeek(this.id)">
                    <font size="6">星期六</font>
                </a></li>
            </div>
        </ul>
    </div>
</nav>
<br>
<br>
<div>
    <center>

        <font size="8">上午</font>
    </center>
    <div class="col s12 m7">

        <div class="card horizontal">
            <div class="card blue-grey darken-1">
                <div class="card-content white-text">

                    <!-- ╭╮ ＿＿＼｜／＿＿╭╮
                │　　　　　　　　~│　　　　
                │　≥　╭———╮　≤　│ 　　
                │／／│０　０│／／.│
                │　　╰—— — ╯　　│
                ╰——┬Ｏ————┬—Ｏ╯　　　　
                　　●│　　　　│
                　　╰│　　Ｏ　│　
                　　╰｜｜-｜｜╯
            -->

                    <font size="6">第一节 : </font>
                </div>
            </div>
            <div class="card-stacked">
                <div class="card-content" id="c0">
                    <font size="6" id="cc0">${courses[0].courseName}</font>
                </div>

            </div>

        </div>
        <div class="card-action" id="p0">
                <span class="blue-text text-darken-2">
                    <font size="5" >教室:</font>
                </span>
                <span class="blue-text text-darken-2">
                    <font size="5" id="pp0">${courses[0].place}</font>
                </span>
        </div>


        <div class="col s12 m7">

            <div class="card horizontal">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <!-- ╭╮ ＿＿＼｜／＿＿╭╮
                     │　　　　　　　　~│　　　　
                     │　≥　╭———╮　≤　│ 　　
                     │／／│０　０│／／.│
                     │　　╰—— — ╯　　│
                     ╰——┬Ｏ————┬—Ｏ╯　　　　
                     　　●│　　　　│
                     　　╰│　　Ｏ　│　
                     　　╰｜｜-｜｜╯
                 -->
                        <font size="6">第二节 : </font>
                    </div>
                </div>
                <div class="card-stacked">
                    <div class="card-content" id="c1">
                        <font size="6" id="cc1">${courses[1].courseName}</font>
                    </div>

                </div>
            </div>
        </div>
        <div class="card-action" id="p1">
                <span class="blue-text text-darken-2">
                    <font size="5" >教室:</font>
                </span>
                <span class="blue-text text-darken-2">
                    <font size="5" id="pp1">${courses[1].place}</font>
                </span>
        </div>


        <br><br>
        <center>
            <font size="8">下午</font>
        </center>
        <div class="col s12 m7">

            <div class="card horizontal">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">

                        <!-- ╭╮ ＿＿＼｜／＿＿╭╮
                     │　　　　　　　　~│　　　　
                     │　≥　╭———╮　≤　│ 　　
                     │／／│０　０│／／.│
                     │　　╰—— — ╯　　│
                     ╰——┬Ｏ————┬—Ｏ╯　　　　
                     　　●│　　　　│
                     　　╰│　　Ｏ　│　
                     　　╰｜｜-｜｜╯
                 -->

                        <font size="6">第三节 : </font>
                    </div>
                </div>
                <div class="card-stacked">
                    <div class="card-content" id="c2">
                        <font size="6" id="cc2">${courses[2].courseName}</font>
                    </div>

                </div>
            </div>
        </div>
        <div class="card-action" id="p2">
                <span class="blue-text text-darken-2">
                    <font size="5" >教室:</font>
                </span>
                <span class="blue-text text-darken-2">
                    <font size="5" id="pp2">${courses[2].place}</font>
                </span>
        </div>


        <div class="col s12 m7">

            <div class="card horizontal">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">

                        <!-- ╭╮ ＿＿＼｜／＿＿╭╮
                     │　　　　　　　　~│　　　　
                     │　≥　╭———╮　≤　│ 　　
                     │／／│０　０│／／.│
                     │　　╰—— — ╯　　│
                     ╰——┬Ｏ————┬—Ｏ╯　　　　
                     　　●│　　　　│
                     　　╰│　　Ｏ　│　
                     　　╰｜｜-｜｜╯
                 -->

                        <font size="6">第四节 : </font>
                    </div>
                </div>
                <div class="card-stacked">
                    <div class="card-content" id="c3">
                        <font size="6" id="cc3">${courses[3].courseName}</font>
                    </div>

                </div>
            </div>
        </div>
        <div class="card-action" id="p3">
                <span class="blue-text text-darken-2">
                    <font size="5" >教室:</font>
                </span>
                <span class="blue-text text-darken-2">
                    <font size="5" id="pp3">${courses[3].place}</font>
                </span>
        </div>
    </div>


    <br><br>

    <center>
        <font size="8"> &nbsp;晚上</font>
    </center>
    <div class="col s12 m7">

        <div class="card horizontal">
            <div class="card blue-grey darken-1">
                <div class="card-content white-text">
                    <font size="6">&nbsp;晚&nbsp;课&nbsp; &nbsp;: </font>
                </div>
            </div>
            <div class="card-stacked">
                <div class="card-content" id="c4">

                    <!-- ╭╮ ＿＿＼｜／＿＿╭╮
                     │　　　　　　　　~│　　　　
                     │　≥　╭———╮　≤　│ 　　
                     │／／│０　０│／／.│
                     │　　╰—— — ╯　　│
                     ╰——┬Ｏ————┬—Ｏ╯　　　　
                     　　●│　　　　│
                     　　╰│　　Ｏ　│　
                     　　╰｜｜-｜｜╯
                 -->

                    <font size="6" id="cc4">${courses[4].courseName}</font>
                </div>

            </div>
        </div>
    </div>
    <div class="card-action" id="p4">
            <span class="blue-text text-darken-2">
                    <font size="5" >教室:</font>
                </span>
            <span class="blue-text text-darken-2">
                <font size="5" id="pp4">${courses[4].place}</font>
            </span>
    </div>

    <br> <br>

</div>
<!--<footer class="page-footer">-->
    <!--<div class="container">-->
        <!--<div class="row">-->
            <!--<div class="col l6 s12">-->
                <!--<h5 class="white-text">哎呦铁牛</h5>-->
                <!--<p class="grey-text text-lighten-4">如有课程信息与实际不符，请及时在公众号内反馈</p>-->
            <!--</div>-->
            <!--<div class="col l4 offset-l2 s12">-->
                <!--<h5 class="white-text">链接</h5>-->
                <!--<ul>-->
                    <!--<li><a class="grey-text text-lighten-3"-->
                           <!--href="#!">链接 1</a></li>-->
                    <!--<li><a class="grey-text text-lighten-3"-->
                           <!--href="#!">链接 2</a></li>-->
                    <!--<li><a class="grey-text text-lighten-3"-->
                           <!--href="#!">链接 3</a></li>-->
                    <!--<li><a class="grey-text text-lighten-3"-->
                           <!--href="#!">链接 4</a></li>-->
                <!--</ul>-->
            <!--</div>-->
        <!--</div>-->
    <!--</div>-->
    <!--<div class="footer-copyright">-->
        <!--<div class="container">-->
            <!--© 2019 哎呦铁牛-->
        <!--</div>-->
    <!--</div>-->
<!--</footer>-->



</body>

</html>
