<%--
  Created by IntelliJ IDEA.
  User: Latent
  Date: 2018/12/12
  Time: 18:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
  <title>我的成绩</title>
  <%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
  %>
  <base href="<%=basePath%>">
  <script src="https://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>
  <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!-- 可选的 Bootstrap 主题文件（一般不用引入） -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

  <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


  <script type="text/javascript">

      // 动态设置id的方法

       function setStyle(sc,myId){


              if(sc<60){
                  //变成红色
                  document.getElementById(myId).className= "progress-bar progress-bar-danger";
                  //改style
                  var test = document.getElementById(myId);
                  test.style.cssText = 'width: '+sc+'%';
              }else if(sc<70){
                  //黄色
                  document.getElementById(myId).className = "progress-bar progress-bar-warning";

                  var test = document.getElementById(myId);
                  test.style.cssText = 'width: '+sc+'%';
              }else if(sc<80){
                  //蓝色
                  document.getElementById(myId).className = "progress-bar progress-bar-info";

                  var test = document.getElementById(myId);
                  test.style.cssText = 'width: '+sc+'%';
              }else if(sc<=100){
                  //绿色
                  document.getElementById(myId).className = "progress-bar progress-bar-success";

                  var test = document.getElementById(myId);
                  test.style.cssText = 'width: '+sc+'%';
              }



      }
  </script>

</head>
<body style="margin:50px">
<br><br><br>
<div >
  <blockquote>
    <font color="#44cef6" size="10">${user.name}同学</font>
  </blockquote>
  <br>

  <c:forEach var="item" items="${grades}">

    <div style="font-size:30px;float:left;" class="courseName"><font
            face="Microsoft YaHei" size=6 >${item.courseName}</font></div>
    <div style="font-size:26px;float:right;" class="courseScore">${item.score}</div>
<br><br><br>
  <div class="progress" style="height:40px" >

      <div id="${item.id}" class=""  role="progressbar" aria-valuenow="100"
           aria-valuemin="0" aria-valuemax="100" style="width: 100%">

    </div>

  </div>

  <div style="font-size:22px;float:left;" >课程性质：${item.courseType}</div>
  <div style="font-size:22px;float:right;">学分：${item.count}</div>
  <br>

    <SCRIPT>
        setStyle(${item.score},${item.id});
    </SCRIPT>

    <hr width=100% size=1 color=#5151A2 style="border:1 dashed #5151A2">
  </c:forEach>
</body>
</html>