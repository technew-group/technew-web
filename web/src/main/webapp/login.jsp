<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>绑定教务网</title>
	<link rel="stylesheet" href="static/css/login.css">
	<script>
        //同步密码框输入
        function cop(){
            var str=document.getElementById("stuNo").value;
            document.getElementById("passwd").value = str;
        }
        //清空密码框
        function copd(){
            document.getElementById("passwd").value = "";
        }
        //客户端防止二次提交
        var flag=true;
        function check(){
            var stuNo=document.getElementById("stuNo").value;
            var passwd=document.getElementById("passwd").value;
            if(stuNo==null||stuNo==""){
                alert("学号不能为空");
                return;
            }
            if(passwd==null||passwd==""){
                alert("密码不能为空");
                return;
            }
            if(flag==true){
                flag=false;
                //通过表单id来提交
                document.getElementById("myform").submit();
            }else{
                alert("正在提交，请稍等。。。");
            }

        }
	</script>
</head>

<body>
<div class="lowin lowin-blue">
	<div class="lowin-brand lowin-animated">
		<img src="static/logo.png" alt="logo">
	</div>
	<div class="lowin-wrapper">
		<div class="lowin-box lowin-login">
			<div class="lowin-box-inner">
				<form action="/web/wx/bind.do?openid=${openid}" method="post" id="myform">
					<h1>教务绑定</h1>
					<div class="lowin-group">
						<label><h2>学号</h2></label>
						<input id="stuNo" type="text" class="lowin-input" name="stuNo" onpropertychange="cop()" oninput="cop()">
					</div>
					<div class="lowin-group password-group">
						<label><h2>密码</h2></label>
						<input id="passwd" type="password" class="lowin-input" name="passwd" onclick="copd()">
						<span class="darkfont"><small>#自动填充默认密码，点击可自行输入</small></span><br>
						<font color="red">${message}</font>
					</div>
					<button class="lowin-btn login-btn" type="button" onclick="check()">
						绑定
					</button>
				</form>
			</div>
		</div>
	</div>
	<footer class="lowin-footer">
		© 2018 Copyright 哎呦铁牛技术组
	</footer>
</div>
</body>
</html>