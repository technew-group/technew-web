import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import pojo.Grade;
import pojo.Schedule;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;

public class JsonTest {
    @Test
    public void getCurrentScheduleTest() {
        String json = "{\"data\":{\"xl\":{\"xqj\":7,\"zjc\":10,\"djz\":1,\"xn\":\"2018\",\"nyr\":\"2019-3-3\",\"count\":27,\"xq\":\"2\",\"id\":\"f772af0700d4415f986c30364305b204\"},\"slist\":[],\"wlist\":[{\"skjc\":1,\"skcd\":2,\"skdd\":\"ES0206\",\"kssj\":\"8 :30\",\"jsxm\":\"杨丽敏\",\"xqj\":1,\"jssj\":\"10:05\",\"kcdm\":\"2801004\",\"teacherId\":\"52abce56bc775873e050007f01003ed8\",\"skzc\":1,\"id\":1,\"kcmc\":\"毛泽东思想和中国特色社会主义理论体系概论\",\"bid\":\"4dab6958810579d88f136921349c1511\",\"jsh\":\"2006801830\"},{\"skjc\":5,\"skcd\":2,\"skdd\":\"ES0205\",\"kssj\":\"13:30\",\"jsxm\":\"孙丽\",\"xqj\":1,\"jssj\":\"14:55\",\"kcdm\":\"2501034\",\"teacherId\":\"52abce56be2d5873e050007f01003ed8\",\"skzc\":1,\"id\":1,\"kcmc\":\"交际英语\",\"bid\":\"967b295bffb3936afa526b7981bbc131\",\"jsh\":\"2002802327\"},{\"skjc\":5,\"skcd\":2,\"skdd\":\"W0410\",\"kssj\":\"13:30\",\"jsxm\":\"杨琪\",\"xqj\":1,\"jssj\":\"14:55\",\"kcdm\":\"2501035\",\"teacherId\":\"52abce56bd9d5873e050007f01003ed8\",\"skzc\":1,\"id\":1,\"kcmc\":\"科技英语\",\"bid\":\"cd7398812fa0cfbce004d72d97363186\",\"jsh\":\"2005802390\"},{\"skjc\":5,\"skcd\":2,\"skdd\":\"ES0206\",\"kssj\":\"13:30\",\"jsxm\":\"刘莎\",\"xqj\":1,\"jssj\":\"14:55\",\"kcdm\":\"2501031\",\"teacherId\":\"52abce56ba5a5873e050007f01003ed8\",\"skzc\":1,\"id\":1,\"kcmc\":\"英语四级综合\",\"bid\":\"6446f355bb7107db844d37de143b01e0\",\"jsh\":\"2010802325\"},{\"skjc\":5,\"skcd\":2,\"skdd\":\"W0321\",\"kssj\":\"13:30\",\"jsxm\":\"张云玲\",\"xqj\":1,\"jssj\":\"14:55\",\"kcdm\":\"2501032\",\"teacherId\":\"52abce56baa05873e050007f01003ed8\",\"skzc\":1,\"id\":1,\"kcmc\":\"英语六级综合\",\"bid\":\"13ebacf61ac51f9a43fa9a77751adbda\",\"jsh\":\"2003802330\"},{\"skjc\":7,\"skcd\":2,\"skdd\":\"WN0101\",\"kssj\":\"15:10\",\"jsxm\":\"程雪连\",\"xqj\":1,\"jssj\":\"16:35\",\"kcdm\":\"2501033\",\"teacherId\":\"52abce56bed25873e050007f01003ed8\",\"skzc\":1,\"id\":1,\"kcmc\":\"英语考研综合\",\"bid\":\"7b3375a2f3913412d7c19e1a37a94b7e\",\"jsh\":\"2001802388\"},{\"skjc\":5,\"skcd\":2,\"skdd\":\"E0415\",\"kssj\":\"13:30\",\"jsxm\":\"孙昌立\",\"xqj\":2,\"jssj\":\"14:55\",\"kcdm\":\"1702366\",\"teacherId\":\"52abce56bc7b5873e050007f01003ed8\",\"skzc\":1,\"id\":1,\"kcmc\":\"数字图像处理II\",\"bid\":\"212981ac9f38a113d9ec6a31cfac756e\",\"jsh\":\"1999801257\"},{\"skjc\":1,\"skcd\":2,\"skdd\":\"E0415\",\"kssj\":\"8 :30\",\"jsxm\":\"孙昌立\",\"xqj\":4,\"jssj\":\"10:05\",\"kcdm\":\"1702366\",\"teacherId\":\"52abce56bc7b5873e050007f01003ed8\",\"skzc\":1,\"id\":1,\"kcmc\":\"数字图像处理II\",\"bid\":\"212981ac9f38a113d9ec6a31cfac756e\",\"jsh\":\"1999801257\"},{\"skjc\":5,\"skcd\":2,\"skdd\":\"ES0206\",\"kssj\":\"13:30\",\"jsxm\":\"刘莎\",\"xqj\":4,\"jssj\":\"14:55\",\"kcdm\":\"2501031\",\"teacherId\":\"52abce56ba5a5873e050007f01003ed8\",\"skzc\":1,\"id\":1,\"kcmc\":\"英语四级综合\",\"bid\":\"6446f355bb7107db844d37de143b01e0\",\"jsh\":\"2010802325\"},{\"skjc\":5,\"skcd\":2,\"skdd\":\"W0321\",\"kssj\":\"13:30\",\"jsxm\":\"张云玲\",\"xqj\":4,\"jssj\":\"14:55\",\"kcdm\":\"2501032\",\"teacherId\":\"52abce56baa05873e050007f01003ed8\",\"skzc\":1,\"id\":1,\"kcmc\":\"英语六级综合\",\"bid\":\"13ebacf61ac51f9a43fa9a77751adbda\",\"jsh\":\"2003802330\"},{\"skjc\":5,\"skcd\":2,\"skdd\":\"ES0205\",\"kssj\":\"13:30\",\"jsxm\":\"孙丽\",\"xqj\":4,\"jssj\":\"14:55\",\"kcdm\":\"2501034\",\"teacherId\":\"52abce56be2d5873e050007f01003ed8\",\"skzc\":1,\"id\":1,\"kcmc\":\"交际英语\",\"bid\":\"967b295bffb3936afa526b7981bbc131\",\"jsh\":\"2002802327\"},{\"skjc\":5,\"skcd\":2,\"skdd\":\"W0410\",\"kssj\":\"13:30\",\"jsxm\":\"杨琪\",\"xqj\":4,\"jssj\":\"14:55\",\"kcdm\":\"2501035\",\"teacherId\":\"52abce56bd9d5873e050007f01003ed8\",\"skzc\":1,\"id\":1,\"kcmc\":\"科技英语\",\"bid\":\"cd7398812fa0cfbce004d72d97363186\",\"jsh\":\"2005802390\"},{\"skjc\":7,\"skcd\":2,\"skdd\":\"WN0101\",\"kssj\":\"15:10\",\"jsxm\":\"程雪连\",\"xqj\":4,\"jssj\":\"16:35\",\"kcdm\":\"2501033\",\"teacherId\":\"52abce56bed25873e050007f01003ed8\",\"skzc\":1,\"id\":1,\"kcmc\":\"英语考研综合\",\"bid\":\"7b3375a2f3913412d7c19e1a37a94b7e\",\"jsh\":\"2001802388\"},{\"skjc\":5,\"skcd\":2,\"skdd\":\"ES0206\",\"kssj\":\"13:30\",\"jsxm\":\"杨丽敏\",\"xqj\":5,\"jssj\":\"14:55\",\"kcdm\":\"2801004\",\"teacherId\":\"52abce56bc775873e050007f01003ed8\",\"skzc\":1,\"id\":1,\"kcmc\":\"毛泽东思想和中国特色社会主义理论体系概论\",\"bid\":\"4dab6958810579d88f136921349c1511\",\"jsh\":\"2006801830\"},{\"skjc\":7,\"skcd\":2,\"skdd\":\"ES0205\",\"kssj\":\"15:10\",\"jsxm\":\"李强\",\"xqj\":5,\"jssj\":\"16:35\",\"kcdm\":\"3701001\",\"teacherId\":\"52abce56bc965873e050007f01003ed8\",\"skzc\":1,\"id\":1,\"kcmc\":\"就业指导\",\"bid\":\"078246f942d74bf943d60fedb123e957\",\"jsh\":\"2003801249\"}]},\"message\":\"成功\",\"state\":200}";
        JSONObject jsonObject = JSON.parseObject(json);
        List<Schedule> schedules = new ArrayList<>();
//        if ("200".equals(jsonObject.getString("state"))) {
//            JSONObject dataObj = (JSONObject) jsonObject.get("data");
//            JSONArray wlistArray = (JSONArray) dataObj.get("wlist");
//            schedules = JSON.parseArray(wlistArray.toJSONString(), Schedule.class);
//            JSONArray slistArray = (JSONArray) dataObj.get("slist");
//            schedules.addAll(JSON.parseArray(slistArray.toJSONString(), Schedule.class));
//        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());

        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        System.out.println(w);
    }

    @Test
    public void getGradeFromPy(){
        String json="{data:[{\"id\": \"2016026358\", \"kcdm\": \"1702381\", \"cj\": \"77\", \"kcxz\": \"选修\", \"kcmc\": \"单片机基础概论\", \"xf\": \"3\"}, {\"id\": \"2016026358\", \"kcdm\": \"1703027\", \"cj\": \"73\", \"kcxz\": \"必修\", \"kcmc\": \"软件工程\", \"xf\": \"2.5\"}, {\"id\": \"2016026358\", \"kcdm\": \"1704007\", \"cj\": \"73\", \"kcxz\": \"选修\", \"kcmc\": \"离散数学\", \"xf\": \"3\"}, {\"id\": \"2016026358\", \"kcdm\": \"1702010\", \"cj\": \"60\", \"kcxz\": \"必修\", \"kcmc\": \"数据库原理及应用1\", \"xf\": \"4\"}, {\"id\": \"2016026358\", \"kcdm\": \"2304002\", \"cj\": \"61\", \"kcxz\": \"必修\", \"kcmc\": \"Systems Analysis and Design2\", \"xf\": \"4.5\"}, {\"id\": \"2016026358\", \"kcdm\": \"2301005\", \"cj\": \"69\", \"kcxz\": \"选修\", \"kcmc\": \"大学英语5（中加计算机）\", \"xf\": \"4\"}, {\"id\": \"2016026358\", \"kcdm\": \"1701204\", \"cj\": \"86\", \"kcxz\": \"必修\", \"kcmc\": \"Systems Analysis and Design 1(数据结构)\", \"xf\": \"3.5\"}],\n" +
                "context:\"grade\"}";
        JSONObject dataObject=JSON.parseObject(json);
        if ("grade".equals(dataObject.getString("context"))) {
            //调用grade解析方法
            List<Grade> grades=new ArrayList<>();
            JSONArray gradeArray =  (JSONArray) dataObject.get("data");
            grades.addAll(JSON.parseArray(gradeArray.toJSONString(), Grade.class));
            System.out.println(grades);
        }
        if ("schedule".equals(dataObject.getString("context"))) {
            //调用schedule解析方法
        }
        Executors.newFixedThreadPool(10);

    }


}
