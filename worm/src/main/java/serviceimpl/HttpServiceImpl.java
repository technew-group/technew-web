package serviceimpl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tools.TimeTool;
import constant.Constant;
import dao.GradeMapper;
import dao.UserMapper;
import okhttp3.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pojo.Grade;
import pojo.Schedule;
import pojo.User;
import pojo.UserExample;
import service.HttpService;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 爬取数据的实现
 *
 * @author 吕圣业
 * @since 30 十一月 2018
 */
@Service("httpService")
public class HttpServiceImpl implements HttpService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpServiceImpl.class);

    private OkHttpClient client;
    @Resource
    private UserMapper userMapper;

    /**
     * 已登陆用户的信息
     */
    private User mUser;

    public static final MediaType headers = MediaType.parse("application/json; charset=utf-8");


    public HttpServiceImpl() {
//        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("111.177.171.10", 9999));
//        client = new OkHttpClient.Builder().proxy(proxy)
//                .connectTimeout(10, TimeUnit.SECONDS)
//                .build();
        client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .build();
    }

    @Override
    public String doGet(String url, String charset) {


        Request request = new Request.Builder().url(url).build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                return response.body().string();
            } else {
                throw new IOException("Unexpected code " + response);
            }
        } catch (IOException e) {
            LOGGER.error("doGet failed Unexpected code{}", response);
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public String doPost(String url, String json) {


        RequestBody body = RequestBody.create(headers, json);
        Request request = new Request.Builder()
                .url(url)
                .removeHeader("User-Agent")
                .addHeader("User-Agent", "Mozilla/5.0 (Linux; Android 8.1; PAR-AL00 Build/HUAWEIPAR-AL00; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/57.0.2987.132 MQQBrowser/6.2 TBS/044304 Mobile Safari/537.36 MicroMessenger/6.7.3.1360(0x26070333) NetType/WIFI Language/zh_CN Process/tools")
                .post(body)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                return response.body().string();
            } else {
                throw new IOException("Unexpected code " + response);
            }
        } catch (IOException e) {
            LOGGER.error("doGet failed Unexpected code {}", response);
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public String login(User user) {

        //组织模拟登陆请求参数
        String password = "";
        String token = "";
        String jsonResult = null;
        try {
            password = DigestUtils.md5Hex((user.getPasswd() + "murp").getBytes("UTF-8"));

            Map<String, String> postData = new HashMap<>(16);
            postData.put("u", user.getStuNo());
            postData.put("p", password);
            postData.put("tec", "web");
            postData.put("ver", "1");
            postData.put("uuid", "");
            String json = JSON.toJSONString(postData);

            //发送请求
            jsonResult = doPost(Constant.LOGIN_URL, json);
            JSONObject jsonObject = JSON.parseObject(jsonResult);

            if (jsonObject.get("data") != null) {
                LOGGER.info("login success");
                //保存token，以便以后访问其他页面
                JSONObject obj = (JSONObject) (jsonObject.get("data"));
                token = obj.getString("token");

                if (StringUtils.isNotBlank(token)) {
                    user.setName(obj.getString("name"));
                    //记录登陆的用户
                    mUser = user;
                }
            }
        } catch (Exception e) {
            LOGGER.error("login failed user : {}, userInfo : {}", jsonResult, user);
            e.printStackTrace();
        }
        return token;
    }

    @Override
    public Map<String, Object> checkAndLogin(User user) {
        UserExample example = new UserExample();
        UserExample.Criteria criteria = example.createCriteria();
        Map<String, Object> result = new HashMap<>();
        if (StringUtils.isNotBlank(user.getOpenId())) {
            criteria.andOpenIdEqualTo(user.getOpenId());
        }
        if (StringUtils.isNotBlank(user.getStuNo())) {
            criteria.andStuNoEqualTo(user.getStuNo());
        }
        List<User> users = userMapper.selectByExample(example);
        //表中存在该用户
        if (!users.isEmpty()) {
            result.put("message", "exist");
            return result;
        } else {
            String token = login(user);
            //用户名或密码错误导致未获取到token
            if ("".equals(token) || StringUtils.isBlank(token)) {
                result.put("message", "error");
                return result;
            } else {
                userMapper.insert(this.mUser);
                result.put("message", "ok");
                result.put("token", token);
                return result;
            }
        }
    }

    @Override
    public List<Grade> getAllGradeByHttp(String token) {

        String json = doGet(Constant.GRADLE_URL + "?token=" + token, "utf-8");//发送请求
        JSONObject jsonObject = JSON.parseObject(json);
        List<Grade> grades = new ArrayList<>();
        if ("200".equals(jsonObject.getString("state"))) {
            String data = jsonObject.getString("data");
            List dataList = JSON.parseArray(data);
            for (int i = 0; i < dataList.size(); i++) {
                data = ((JSONArray) dataList).getString(i);
                data = JSON.parseObject(data).getString("items");
                grades.addAll(JSON.parseArray(data, Grade.class));
            }
        }
        return grades;
    }

    @Override
    public List<Grade> getCurrentGradeByHttp(String token) {
        List<Grade> gradeList = getAllGradeByHttp(token);
        //条件集合
        ArrayList<String> year = new ArrayList<>();
        year.add(Constant.CYEAR);
        ArrayList<Integer> term = new ArrayList<>();
        term.add(Constant.CTERM);
        //结果集合
        List<Grade> result = null;
        result = gradeList.stream()
                .filter((Grade g) -> year.contains(g.getYear()))
                .filter((Grade g) -> term.contains(g.getTerm()))
                .collect(Collectors.toList());
        return result;
    }

    @Override
    public User getmUser() {
        return mUser;
    }

    @Override
    public List<Schedule> getSchedule(String token) {
        int successiveWeek = TimeTool.getSuccessiveWeek();
        String json = doGet(Constant.SCHELDULE_URL + "?token=" + token + "&week=" + successiveWeek, "utf-8");
        JSONObject jsonObject = JSON.parseObject(json);
        List<Schedule> schedules = new ArrayList<>();
        if ("200".equals(jsonObject.getString("state"))) {
            JSONObject dataObj = (JSONObject) jsonObject.get("data");
            JSONArray wlistArray = (JSONArray) dataObj.get("wlist");
            schedules = JSON.parseArray(wlistArray.toJSONString(), Schedule.class);
            JSONArray slistArray = (JSONArray) dataObj.get("slist");
            schedules.addAll(JSON.parseArray(slistArray.toJSONString(), Schedule.class));
        }
        return schedules;
    }
}

















































