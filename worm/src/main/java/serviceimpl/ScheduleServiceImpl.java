package serviceimpl;

import com.tools.TimeTool;
import dao.ScheduleMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pojo.Schedule;
import pojo.ScheduleExample;
import pojo.User;
import service.HttpService;
import service.ScheduleService;
import service.UserService;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 上课提醒的接口
 *
 * @author 吕圣业
 * @since 3 三月 2019
 */
@Service("scheduleService")
public class ScheduleServiceImpl implements ScheduleService {

    @Resource
    private ScheduleMapper scheduleMapper;

    @Resource
    private HttpService httpService;

    @Resource
    private UserService userService;

    @Override
    public List<Schedule> getCurrentWeekSchedule(User user) {

        User userinfo= userService.isAccess(user);

        //计算周次
        int successiveWeek = TimeTool.getSuccessiveWeek();
        //先去库中查,查一周的课
        ScheduleExample example = new ScheduleExample();
        ScheduleExample.Criteria criteria = example.createCriteria();
        criteria.andSuccesiveWeekEqualTo(successiveWeek)
                .andStuNoEqualTo(userinfo.getStuNo());
        List<Schedule> schedules = scheduleMapper.selectByExample(example);

        if (schedules==null||schedules.isEmpty()) {
                String token = httpService.login(userinfo);
                //库中没有，开始爬取
                schedules = httpService.getSchedule(token);
                if (!schedules.isEmpty()) {
                    //存入库中
                    schedules.forEach(s -> {
                        s.setId(null);
                        s.setStuNo(userinfo.getStuNo());
                        scheduleMapper.insert(s);
                    });
                }
        }
        return schedules;
    }

    @Override
    public List<Schedule> getCurrentDaySchedule(User user) {

        List<Schedule> schedules = getCurrentWeekSchedule(user);

        //过滤数据，返回一天的课
        if (!schedules.isEmpty()) {
            return schedulesFileter(schedules);
        }
        return null;
    }


    @Override
    public List<Schedule> getCurrentDaySchedule(User user, int week) {
        List<Schedule> schedules = getCurrentWeekSchedule(user);
        if (!schedules.isEmpty()) {
            return anydayschedulesFileter(schedules, week);
        }
        return null;
    }

    @Override
    public List<Schedule> schedulesFileter(List<Schedule> schedules) {

        //条件集合，周次
        ArrayList<Integer> successive = new ArrayList<>();
        int successiveWeek = TimeTool.getSuccessiveWeek();
        successive.add(successiveWeek);

        //星期
        ArrayList<Integer> weeks = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int week = cal.get(Calendar.DAY_OF_WEEK) - 1;
        weeks.add(week);

        //结果集合
        List<Schedule> scheduleResult = null;
        scheduleResult = schedules.stream()
                .filter((Schedule s) -> successive.contains(s.getSuccesiveWeek()))
                .filter((Schedule s) -> weeks.contains(s.getWeek()))
                .collect(Collectors.toList());

        return scheduleResult;
    }

    @Override
    public List<Schedule> anydayschedulesFileter(List<Schedule> schedules, int week) {

        ArrayList<Integer> successive = new ArrayList<>();
        int successiveWeek = TimeTool.getSuccessiveWeek();
        successive.add(successiveWeek);

        ArrayList<Integer> weeks = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        if (week == 0) {
            weeks.add(7);
        } else {
            weeks.add(week);
        }

        List<Schedule> scheduleResult = null;
        scheduleResult = schedules.stream()
                .filter((Schedule s) -> successive.contains(s.getSuccesiveWeek()))
                .filter((Schedule s) -> weeks.contains(s.getWeek()))
                .collect(Collectors.toList());
        return scheduleResult;
    }
}
