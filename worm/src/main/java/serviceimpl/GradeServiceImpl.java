package serviceimpl;

import constant.Constant;
import dao.GradeMapper;
import dao.Send_GradeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pojo.*;
import service.GradeService;
import service.HttpService;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 提供成绩查询的实现
 *
 * @author 吕圣业
 * @since 4 十二月 2018
 */
@Service("gradeService")
public class GradeServiceImpl implements GradeService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GradeServiceImpl.class);
    @Resource
    private GradeMapper gradeMapper;

    @Resource
    private HttpService httpService;

    @Resource
    private Send_GradeMapper sendGradeMapper;

    @Override
//    public List<Send_Grade> getGrade(User user) {
    public List<Grade> getGrade(User user) {


        //先去库中查
//        Send_GradeExample send_gradeExample = new Send_GradeExample();
//        Send_GradeExample.Criteria criteria = send_gradeExample.createCriteria();
//        criteria.andStuNoEqualTo(user.getStuNo())
//                .andScoreNotEqualTo("");

        GradeExample gradeExample = new GradeExample();
        GradeExample.Criteria criteria = gradeExample.createCriteria();
        criteria.andStuNoEqualTo(user.getStuNo())
                .andYearEqualTo(Constant.CYEAR)
                .andTermEqualTo(Constant.CTERM);
//        List<Send_Grade> grades = sendGradeMapper.selectByExample(send_gradeExample);
        List<Grade> grades = gradeMapper.selectByExample(gradeExample);

        //条件集合
        ArrayList<String> year = new ArrayList<>();
        year.add(Constant.CYEAR);
        ArrayList<Integer> term = new ArrayList<>();
        term.add(Constant.CTERM);
        //结果集合
        List<Grade> gradesResult = null;
        gradesResult = grades.stream()
                .filter((Grade g) -> year.contains(g.getYear()))
                .filter((Grade g) -> term.contains(g.getTerm()))
                .collect(Collectors.toList());
//        if (grades.isEmpty() || grades == null) {
//            return null;
//        } else {
//            List<Send_Grade> showgrades = new ArrayList<Send_Grade>();
//            boolean flag = true;
//            for (int i = 0; i < grades.size(); i++) {
//                flag = true;
//                for (int j = 0; j < showgrades.size(); j++) {
//                    if (grades.get(i).getCourseNo().equals(showgrades.get(j).getCourseNo())) {
//                        flag = false;
//                    }
//                }
//                if (flag == true) {
//                    showgrades.add(grades.get(i));
//                }
//            }
//            LOGGER.info("showgrades{}", showgrades);
//            LOGGER.info("grades{}", grades);
//        return grades;
//    }
//            //库中没有，尝试爬取
        try {
            String token = httpService.login(user);
            gradesResult = httpService.getCurrentGradeByHttp(token);
            if (!gradesResult.isEmpty()) {
                //将数据存入库中
                gradesResult.forEach(g -> gradeMapper.insert(g));
                return grades;
            } else {
                //未爬到
                return grades;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return grades;
    }

    @Override
    public List<Send_Grade> getGradeinDB(User user) {
        Send_GradeExample send_gradeExample = new Send_GradeExample();
        Send_GradeExample.Criteria criteria = send_gradeExample.createCriteria();
        criteria.andStuNoEqualTo(user.getStuNo()).andSendEqualTo(0).andScoreNotEqualTo("");
        List<Send_Grade> httpGrades = sendGradeMapper.selectByExample(send_gradeExample);
        return httpGrades;
    }

    @Override
    public List<Grade> getAllGrade(User user) {
        String token = httpService.login(user);
        List<Grade> grades = null;
        try {
            grades = httpService.getAllGradeByHttp(token);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return grades;
    }

    @Override
    public void getAllGradebyjw() {
        try {
            Process proc = Runtime.getRuntime().exec("python /root/Demo.py");
            LOGGER.info("get grade1 in py is begin");
            Process proc2 = Runtime.getRuntime().exec("python /root/Demo2.py");
            LOGGER.info("get grade2 in py is begin");
            Process proc3 = Runtime.getRuntime().exec("python /root/Demo3.py");
            LOGGER.info("get grade3 in py is begin");
            //将信息输出到控制台
            BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream(), "GBK"));
            BufferedReader in2 = new BufferedReader(new InputStreamReader(proc2.getInputStream(), "GBK"));
            BufferedReader in3 = new BufferedReader(new InputStreamReader(proc3.getInputStream(), "GBK"));
            String line = null;
            while ((line = in.readLine()) != null) {
                LOGGER.info("Demo  " + line);
            }
            line = null;
            while ((line = in2.readLine()) != null) {
                LOGGER.info("Demo2  " + line);
            }
            line = null;
            while ((line = in3.readLine()) != null) {
                LOGGER.info("Demo3  " + line);
            }
            in.close();
            in2.close();
            in3.close();
            proc.waitFor();
            proc2.waitFor();
            proc3.waitFor();
            LOGGER.info("get grade in py is finsh");
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error("IOException");
        } catch (InterruptedException e) {
            e.printStackTrace();
            LOGGER.error("InterruptedException");
        }
    }


    @Override
    public void insertAllGrade(List<Grade> grades) {
        if (grades != null) {
            grades.forEach(grade -> gradeMapper.insert(grade));
        }
    }

}
