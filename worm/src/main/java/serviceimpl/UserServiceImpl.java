package serviceimpl;

import dao.ScheduleMapper;
import dao.UserMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pojo.ScheduleExample;
import pojo.User;
import pojo.UserExample;
import service.HttpService;
import service.UserService;

import javax.annotation.Resource;
import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Resource
    private UserMapper userMapper;
    @Resource
    private HttpService httpService;
    @Resource
    private ScheduleMapper scheduleMapper;

    @Override
    public List<User> getAllUsers() {
        UserExample example = new UserExample();
        return userMapper.selectByExample(example);
    }

    @Override
    public User isAccess(User user) {
        if (StringUtils.isNotBlank(user.getOpenId())) {
            UserExample userExample = new UserExample();
            UserExample.Criteria criteria = userExample.createCriteria();
            criteria.andOpenIdEqualTo(user.getOpenId());
            List<User> currentUser = userMapper.selectByExample(userExample);

            if (currentUser != null && !currentUser.isEmpty()) {
                return currentUser.get(0);
            } else {
                String token = httpService.login(user);
                //用户名或密码错误导致未获取到token
                if ("".equals(token) || StringUtils.isBlank(token)) {
                    LOGGER.error("user check fail，用户名或密码错误或M黑科技崩溃 : {}", user);
                    return null;
                } else {
                    user = httpService.getmUser();
                    user.setStatusGrade(1);
                    user.setStatusSchedule(1);
                    userMapper.insert(user);
                    return user;
                }
            }
        }else {
            LOGGER.info("有人入侵，无openid");
            return null;
        }
    }

    @Override
    public User isExist(String openid) {
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andOpenIdEqualTo(openid);
        List<User> currentUser = userMapper.selectByExample(userExample);
        if (!currentUser.isEmpty()) {
            return currentUser.get(0);
        }
        return null;
    }

    @Override
    public void deleteUserByOpenid(String openid) {
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andOpenIdEqualTo(openid);
        List<User> users = userMapper.selectByExample(userExample);
        if (users.isEmpty()) {
            return;
        }
        userMapper.deleteByExample(userExample);
        ScheduleExample scheduleExample = new ScheduleExample();
        ScheduleExample.Criteria criteria1 = scheduleExample.createCriteria();
        criteria1.andStuNoEqualTo(users.get(0).getStuNo());
        scheduleMapper.deleteByExample(scheduleExample);

    }

    @Override
    public List<User> getUsersForSchedule() {
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andStatusScheduleEqualTo(1);
        List<User> users=userMapper.selectByExample(userExample);
        return users;
    }

    @Override
    public List<User> getUsersForGrade() {
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andStatusGradeEqualTo(1);
        List<User> users=userMapper.selectByExample(userExample);
        return users;
    }

}
