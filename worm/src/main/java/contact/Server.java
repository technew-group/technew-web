package contact;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;

/**
 * 接收python端所要更新的数据，以json的形式传过来
 * 不忍删除的NIO服务端
 *
 * @author 吕圣业
 * @since 19 三月 2019
 */
@Deprecated
public class Server implements Runnable{

    private Selector selector;
    private ByteBuffer readBuffer=ByteBuffer.allocate(1024);
    private ByteBuffer writeBuffer=ByteBuffer.allocate(1024);

    public Server(int port) {
        init(port);
    }

    private void init(int port){
        try {
            //开启多路复用器
            this.selector = Selector.open();
            //开启服务通道
            ServerSocketChannel serverChannel = ServerSocketChannel.open();
            //设置为非阻塞模式
            serverChannel.configureBlocking(false);
            //绑定端口
            serverChannel.bind(new InetSocketAddress(port));
            //将通道注册到多路复用器，设置连接成功标志
            serverChannel.register(this.selector, SelectionKey.OP_ACCEPT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 服务端监听方法
     * @param key
     */
    private void accept(SelectionKey key){
        try {
            //获取通过init注册到复用器的serverchannel
            ServerSocketChannel serverChannel=(ServerSocketChannel) key.channel();
            //开始监听，一个ServerSocketChannel对应一个客户端的SocketChannel
            SocketChannel socketChannel=serverChannel.accept();
            //设置为非阻塞
            socketChannel.configureBlocking(false);
            //打上可读标志，读取对应客户端的内容时使用
            socketChannel.register(selector,SelectionKey.OP_READ);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void read(SelectionKey key){

        try {
            //清空缓冲区
            readBuffer.clear();
            //获取客户端通道
            SocketChannel channel = (SocketChannel)key.channel();
            int readLength = channel.read(readBuffer);
            //客户端没有写入数据
            if(readLength == -1){
                // 关闭通道
                key.channel().close();
                // 关闭连接
                key.cancel();
                return;
            }
            readBuffer.flip();
            byte[] datas = new byte[readBuffer.remaining()];
            readBuffer.get(datas);
            //执行业务逻辑，bytebuffer中存放的是客户端传来的字符串
            System.out.println("java服务端："+datas.toString());


            //注册通道，标记为写操作
            channel.register(this.selector, SelectionKey.OP_WRITE);
        } catch (IOException e) {
            e.printStackTrace();
            try {
                key.channel().close();
                key.cancel();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    private void write(SelectionKey key){

        try {
            //清空缓冲区
            writeBuffer.clear();
            SocketChannel channel = (SocketChannel)key.channel();
            //开始处理业务逻辑，返回json
            String jsonResult="";



            writeBuffer.put(jsonResult.getBytes("UTF-8"));
            writeBuffer.flip();
            channel.write(writeBuffer);
            channel.register(selector, SelectionKey.OP_READ);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClosedChannelException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
        while(true){
            try {
                //阻塞方法，至少选中一个或一个以上个通道才会返回
                selector.select();
                //返回选中通道标记集合
                Iterator<SelectionKey> keys=selector.selectedKeys().iterator();
                while(keys.hasNext()){
                    SelectionKey key=keys.next();
                    // 将本次要处理的通道从集合中删除，下次循环根据新的通道列表再次执行必要的业务逻辑
                    keys.remove();
                    if(key.isValid()){
                        if(key.isAcceptable()){
                            accept(key);
                        }
                        if(key.isReadable()){
                            read(key);
                        }
                        if(key.isWritable()){
                            write(key);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        new Thread(new Server(9999)).start();
    }
}
