package contact;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * 请求python端，用户刚绑定，需要帮忙爬取数据
 *
 * @author 吕圣业
 * @since 5 四月 2019
 */
@Component
public class RequestClient {

    private String hostName = "localhost";
    private int port = 9999;
    private ByteBuffer buffer = ByteBuffer.allocate(1024);
    private SocketChannel channel = null;

    public void connection() {

        try {
            //创建远程地址
            InetSocketAddress remote = new InetSocketAddress(hostName, port);
            //开启通道
            channel = SocketChannel.open();
            // 连接远程服务器。
            channel.connect(remote);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 将请求发送给对应端口
     * @param requestJson
     */
    public void dispacher(String requestJson) {
        this.connection();

        try {
            buffer.put("requestJson".getBytes("UTF-8"));
            // 重置缓存游标
            buffer.flip();
            // 将数据发送给服务器
            channel.write(buffer);
            // 清空缓存数据。
            buffer.clear();
//                //读取服务端返回数据到buffer中
//                int readLength = channel.read(buffer);
//                if (readLength == -1) {
//                    break;
//                }
//                // 重置缓存游标
//                buffer.flip();
//                byte[] datas = new byte[buffer.remaining()];
//                // 读取数据到字节数组。
//                buffer.get(datas);
//                //datas是服务端返回的东西
//                //开始处理业务逻辑
//                System.out.println("java客户端：" + datas.toString());
//
//                // 清空缓存。
//                buffer.clear();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
