package constant;

/**
 * 常量类
 * @author 吕圣业
 *
 */
public class Constant {
	
	/**
	 * 密码错误
	 */
	public static final String PASSWD_ERROR="密码错误！！";
	
	/**
	 * 字符编码
	 */
	public static final String ENCODING="utf-8";
	
	/**
	 * 用户类型
	 */
	public static final String RADIO_BUTTON_LIST="学生";
	
	
	/**
	 * 基础地址
	 */
	public static final String BASE_URL="http://222.171.107.108";
	
	/**
	 * 登陆URL
	 */
	public static final String LOGIN_URL=BASE_URL+"/university-facade/Murp/Login";
	/**
	 * 课表URL
	 * ?token=a63444779e7d4261ad6aa58eefc06435&week=
	 */
	public static final String SCHELDULE_URL=BASE_URL+"/university-facade/Schedule/ScheduleList";

	/**
	 * 登陆后主页面URL
	 */
	public static final String INDEX_URL=BASE_URL+"/static/html/main/mycollege/index.html";
	/**
	 * 成绩URL
	 */
	public static final String GRADLE_URL=BASE_URL+"/university-facade/MyUniversity/MyGrades";

	/**
	 * 当前学年
	 */
	public static final String CYEAR="2018-2019";
	/**
	 * 当前学期
	 */
	public static final Integer CTERM=2;


}
