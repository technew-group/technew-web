package service;

import pojo.Grade;
import pojo.Schedule;
import pojo.User;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 爬取数据的接口
 *
 * @author 吕圣业
 * @since 30 十一月 2018
 */
public interface HttpService {

	/**
	 * 根据指定url发送给请求
	 * @param url 请求url
	 * @param charset 指定字符集
	 * @return 响应页面的HTML文档
	 */
	 String doGet(String url, String charset)throws Exception;
	
	/**
	 * 根据指定url和参数值发送post请求
	 * @param url 请求url
	 * @param json 参数列表
	 * @return 响应页面的HTML文档
	 */
	 String doPost(String url, String json)throws Exception;

	/**
	 * 模拟登陆，返回token
	 * @param user 用户信息
	 * @return 返回登陆是否成功
	 */
	 String login(User user) ;
	
	/**
	 * 爬取所有学期的成绩
	 */
	List<Grade> getAllGradeByHttp(String token);

	/**
	 * 爬取当前学期的成绩
	 * @param token
	 * @return
	 * @throws IOException
	 */
	List<Grade> getCurrentGradeByHttp(String token);

	/**
	 * 用户绑定和主动查信息时调用
	 * 若库中存在该用户则返回exist，若不存在则尝试获取token，返回token
	 * @param user
	 * @return
	 */
	Map<String,Object> checkAndLogin(User user);

	User getmUser();

	/**
	 * 爬取当前周的课表
	 * @param token
	 * @return
	 * @throws IOException
	 */
	 List<Schedule> getSchedule(String token);


}















































