package service;

import pojo.Schedule;
import pojo.User;

import java.util.List;
/**
 * 上课提醒的接口
 *
 * @author 吕圣业
 * @since 3 三月 2019
 */
public interface ScheduleService {
    /**
     * 获取用户一周的课并存入库，过滤后最终返回当天的课
     * @param user 用户
     * @return
     */
    List<Schedule> getCurrentDaySchedule(User user);

    /**
     * 查一周内任意课表专用重写方法
     * @param user
     * @param week
     * @return
     */
    List<Schedule> getCurrentDaySchedule(User user, int week);


    /**
     * 课程过滤器，返回当天的课
     * @param schedules
     * @return
     */
    List<Schedule> schedulesFileter(List<Schedule> schedules);

    /**
     * 去库中查用于本周的课程
     * @param user
     * @return
     */
    List<Schedule> getCurrentWeekSchedule(User user);


    /**
     * 课程过滤器
     * 返回一周内任意一天的课表
     * @param schedules
     * @param week
     * @return
     */
    List<Schedule> anydayschedulesFileter(List<Schedule> schedules, int week);


}
