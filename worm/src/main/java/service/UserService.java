package service;

import pojo.User;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();

    /**
     * 判断用户是否存在库中，若不存在，则尝试获取token。验证错误返回null
     * @param user
     * @return
     */
    User isAccess(User user);

    /**
     * 查询用户是否在库中，存在即返回user实体，不存在则返回null
     * @param openid
     * @return
     */
    User isExist(String openid);

    void deleteUserByOpenid(String openid);

    /**
     * 返回订阅上课提醒的用户
     * @return
     */
    List<User> getUsersForSchedule();

    /**
     * 返回订阅成绩推送的用户
     * @return
     */
    List<User> getUsersForGrade();
}
