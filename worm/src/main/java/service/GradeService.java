package service;

import pojo.Grade;
import pojo.Send_Grade;
import pojo.User;

import java.util.List;

/**
 * 提供成绩查询的服务
 *
 * @author 吕圣业
 * @since 4 十二月 2018
 */
public interface GradeService {
    /**
     * 获取用户本学期成绩,先去库中查，如果没有则尝试爬取，爬取成功后再插入到库
     *
     * @param user
     * @return
     */
    List<Grade> getGrade(User user);

    /**
     * 从数据库中查询此用户的所有成绩
     * @param user  用户对象
     * @return  成绩集合
     */
    List<Send_Grade> getGradeinDB(User user);

    /**
     * 获取用户所有成绩，默认从m黑科技上爬
     *
     * @param user
     * @return
     */
    List<Grade> getAllGrade(User user);

    /**
     * 获取所有用户成绩，从教务网上查
     *
     */
    void getAllGradebyjw();

    /**
     * 插入用户所有成绩
     *
     * @param grades
     */
    void insertAllGrade(List<Grade> grades);

}
