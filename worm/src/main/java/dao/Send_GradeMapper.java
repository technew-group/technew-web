package dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import pojo.Send_Grade;
import pojo.Send_GradeExample;

public interface Send_GradeMapper {
    int countByExample(Send_GradeExample example);

    int deleteByExample(Send_GradeExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Send_Grade record);

    int insertSelective(Send_Grade record);

    List<Send_Grade> selectByExample(Send_GradeExample example);

    Send_Grade selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Send_Grade record, @Param("example") Send_GradeExample example);

    int updateByExample(@Param("record") Send_Grade record, @Param("example") Send_GradeExample example);

    int updateByPrimaryKeySelective(Send_Grade record);

    int updateByPrimaryKey(Send_Grade record);
}