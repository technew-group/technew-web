package pojo;

import java.io.Serializable;

public class User implements Serializable {
    private Integer id;

    private String stuNo;

    private String passwd;

    private String name;

    private String openId;

    private Integer statusGrade;

    private Integer statusSchedule;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStuNo() {
        return stuNo;
    }

    public void setStuNo(String stuNo) {
        this.stuNo = stuNo == null ? null : stuNo.trim();
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd == null ? null : passwd.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId == null ? null : openId.trim();
    }

    public Integer getStatusGrade() {
        return statusGrade;
    }

    public void setStatusGrade(Integer statusGrade) {
        this.statusGrade = statusGrade;
    }

    public Integer getStatusSchedule() {
        return statusSchedule;
    }

    public void setStatusSchedule(Integer statusSchedule) {
        this.statusSchedule = statusSchedule;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", stuNo='" + stuNo + '\'' +
                ", passwd='" + passwd + '\'' +
                ", name='" + name + '\'' +
                ", openId='" + openId + '\'' +
                ", statusGrade=" + statusGrade +
                ", statusSchedule=" + statusSchedule +
                '}';
    }
}