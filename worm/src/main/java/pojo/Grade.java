package pojo;

import com.alibaba.fastjson.annotation.JSONField;
import org.apache.commons.lang3.StringUtils;

//@JSONType(orders = {"stuNo", "courseNo", "year", "garde", "courseType", "term", "courseName", "count"})
public class Grade implements Comparable{
    private Integer id;
    //  学号
    @JSONField(name = "xh")
    private String stuNo;
    //kcdm  课程号
    @JSONField(name = "kcdm")
    private String courseNo;
    //xn  学年
    @JSONField(name = "xn")
    private String year;
    // cj  成绩
    @JSONField(name = "cj")
    private String score;
    //kcxz 课程性质
    @JSONField(name = "kcxz")
    private String courseType;
    //  xq  学期
    @JSONField(name = "xq")
    private Integer term;
    //kcmc  课程名
    @JSONField(name = "kcmc")
    private String courseName;
    //xf    学分
    @JSONField(name = "xf")
    private String count;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStuNo() {
        return stuNo;
    }

    public void setStuNo(String stuNo) {
        this.stuNo = stuNo == null ? null : stuNo.trim();
    }

    public String getCourseNo() {
        return courseNo;
    }

    public void setCourseNo(String courseNo) {
        this.courseNo = courseNo == null ? null : courseNo.trim();
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year == null ? null : year.trim();
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score == null ? null : score.trim();
    }

    public String getCourseType() {
        return courseType;
    }

    public void setCourseType(String courseType) {
        this.courseType = courseType == null ? null : courseType.trim();
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName == null ? null : courseName.trim();
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "id=" + id +
                ", stuNo='" + stuNo + '\'' +
                ", courseNo='" + courseNo + '\'' +
                ", year='" + year + '\'' +
                ", score='" + score + '\'' +
                ", courseType='" + courseType + '\'' +
                ", term=" + term +
                ", courseName='" + courseName + '\'' +
                ", count=" + count +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        Grade g = (Grade) o;
        String tYear=this.getYear();
        String gYear=g.getYear();
        String tbeforeHyphen= StringUtils.substringBefore(tYear,"-");
        String gbeforeHyphen=StringUtils.substringBefore(gYear,"-");
        //比较学年
        if(Integer.parseInt(tbeforeHyphen)<Integer.parseInt(gbeforeHyphen)){
            return 1;
        }else if(Integer.parseInt(StringUtils.substringBefore(tYear,"-"))==Integer.parseInt(StringUtils.substringBefore(gYear,"-"))){
            //比较学期
            if(this.getTerm()<g.getTerm()){
                return 1;
            }else if(this.getTerm()==g.getTerm()){
                return 0;
            }
        }
        return -1;
    }

    @Override
    public boolean equals(Object obj){
        if (obj != null && obj.getClass() == this.getClass()) {
            Grade grade= (Grade) obj;
            if (grade.getCourseNo() == null || courseNo == null||grade.getStuNo() == null || stuNo == null) {
                return false;
            }else{
                return courseNo.equals(grade.getCourseNo())&&stuNo.equals(grade.getStuNo());
            }
        }
        return false;
    }
}