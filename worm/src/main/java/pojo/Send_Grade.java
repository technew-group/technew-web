package pojo;

public class Send_Grade {
    private Integer id;

    private String stuNo;

    private String courseNo;

    private String score;

    private String courseType;

    private String courseName;

    private String count;

    private Integer send;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStuNo() {
        return stuNo;
    }

    public void setStuNo(String stuNo) {
        this.stuNo = stuNo == null ? null : stuNo.trim();
    }

    public String getCourseNo() {
        return courseNo;
    }

    public void setCourseNo(String courseNo) {
        this.courseNo = courseNo == null ? null : courseNo.trim();
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score == null ? null : score.trim();
    }

    public String getCourseType() {
        return courseType;
    }

    public void setCourseType(String courseType) {
        this.courseType = courseType == null ? null : courseType.trim();
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName == null ? null : courseName.trim();
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count == null ? null : count.trim();
    }

    public Integer getSend() {
        return send;
    }

    public void setSend(Integer send) {
        this.send = send;
    }

    @Override
    public String toString() {
        return "Send_Grade{" +
                "id=" + id +
                ", stuNo='" + stuNo + '\'' +
                ", courseNo='" + courseNo + '\'' +
                ", score='" + score + '\'' +
                ", courseType='" + courseType + '\'' +
                ", courseName='" + courseName + '\'' +
                ", count='" + count + '\'' +
                ", send=" + send +
                '}';
    }
}