package pojo;

import com.alibaba.fastjson.annotation.JSONField;

public class Schedule {
    private Integer id;
    /**
     * 教师姓名
     */
    @JSONField(name = "jsxm")
    private String teacherName;
    /**
     * 课程名称
     */
    @JSONField(name = "kcmc")
    private String courseName;
    /**
     * 授课地点
     */
    @JSONField(name = "skdd")
    private String place;
    /**
     * 授课节次
     */
    @JSONField(name = "skjc")
    private Integer timePoint;
    /**
     * 授课周次
     */
    @JSONField(name = "skzc")
    private Integer succesiveWeek;
    /**
     * 星期
     */
    @JSONField(name = "xqj")
    private Integer week;
    /**
     * 下课时间
     */
    @JSONField(name = "jssj")
    private String endTime;
    /**
     * 上课时间
     */
    @JSONField(name = "kssj")
    private String startTime;
    /**
     * 学号
     */
    private String stuNo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName == null ? null : teacherName.trim();
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName == null ? null : courseName.trim();
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place == null ? null : place.trim();
    }

    public Integer getTimePoint() {
        return timePoint;
    }

    public void setTimePoint(Integer timePoint) {
        this.timePoint = timePoint;
    }

    public Integer getSuccesiveWeek() {
        return succesiveWeek;
    }

    public void setSuccesiveWeek(Integer succesiveWeek) {
        this.succesiveWeek = succesiveWeek;
    }

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime == null ? null : endTime.trim();
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime == null ? null : startTime.trim();
    }

    public String getStuNo() {
        return stuNo;
    }

    public void setStuNo(String stuNo) {
        this.stuNo = stuNo == null ? null : stuNo.trim();
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "id=" + id +
                ", teacherName='" + teacherName + '\'' +
                ", courseName='" + courseName + '\'' +
                ", place='" + place + '\'' +
                ", timePoint=" + timePoint +
                ", succesiveWeek=" + succesiveWeek +
                ", week=" + week +
                ", endTime='" + endTime + '\'' +
                ", startTime='" + startTime + '\'' +
                ", stuNo='" + stuNo + '\'' +
                '}';
    }
}