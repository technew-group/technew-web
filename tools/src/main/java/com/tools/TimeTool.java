package com.tools;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;

/**
 * @author 张龙龙
 *
 *
 * */
public class TimeTool {

    private static final Logger LOGGER = LoggerFactory.getLogger(TimeTool.class);
    public TimeTool() {
    }

    /**
     * 获取当前周次
     * @return
     */
    public static int getSuccessiveWeek() {
        Date date = getCurrentDate();
        Date standardDate = new Date(119, 2, 4);
        Calendar cal = Calendar.getInstance();
        cal.setTime(standardDate);
        long time1 = cal.getTimeInMillis();
        cal.setTime(date);
        long time2 = cal.getTimeInMillis();
        long between_days = (time2 - time1) / 86400000L;
        int week=(Integer.valueOf((int) between_days))/7+1;
        return week;
    }


    /**
     * 获取当前时间
     * @return
     */
    public  static Date getCurrentDate() {
        Calendar now = Calendar.getInstance();
        Date date = new Date(now.get(1) - 1900, now.get(2), now.get(5));
        return date;
    }

    /**
     * 获取当前时间对应的上课节次
     * @return
     */
    @Test
    public static int getCurrentTimePoint(){
        Date date=new Date();
        Integer hours=date.getHours();
        Integer minutes = date.getMinutes();
        if((hours==8&&minutes<4)|| (hours==7&& minutes>=25)){
               return 1;
        }
        else if((hours==10&&minutes<25)|| (hours==9&& minutes>=30)){
            LOGGER.info("第二节课开始---------");
               return 3;
        }
        else if((hours==13&&minutes<29)|| (hours==12&& minutes>=30)){
              return 5;
        }
        else if(((hours==15&&minutes<10)||hours==14&&minutes>=30)){
            return 7;
        }
        else if((hours==18&&minutes<1)|| (hours==17&& minutes>=30)){
                return 9;
        }
        return 0;
    }

    public static int getWeek(){
        //计算星期
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int week = cal.get(Calendar.DAY_OF_WEEK) - 1;
        return week;
    }
    @Test
    public void test(){
        System.out.println(getCurrentTimePoint());

    }
}