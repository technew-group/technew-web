package com.tools;


import java.io.File;
import java.io.FileInputStream;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
/**
 * @author 张龙龙
 * @date 2019/3/3 15:33
 */
public class ExcelTool {
        private static final int NUMERIC = 0;
        private static final int STRING = 1;
        private static final int FORMULA = 2;
        private static final int BLANK = 3;
        private static final int BOOLEAN = 4;
        private static final int ERROR = 5;
        private String tableName = "night_room";

        public ExcelTool() {
        }

        public String getExcel(String filePath) throws Exception {
            boolean isExcel2003 = filePath.toLowerCase().endsWith("xls");
            Workbook workbook = null;
            if (isExcel2003) {
                workbook = new HSSFWorkbook(new FileInputStream(new File(filePath)));
            } else {
                workbook = new XSSFWorkbook(new FileInputStream(new File(filePath)));
            }

            Sheet sheet = ((Workbook)workbook).getSheet("Sheet1");
            int rows = sheet.getPhysicalNumberOfRows();
            StringBuffer value = new StringBuffer("insert into " + this.tableName + " values");

            for(int i = 0; i <= rows; ++i) {
                value.append("(default,\"");
                Row row = (Row) sheet.getRow(i);
                if (row != null) {
                    int cells = row.getPhysicalNumberOfCells();

                    for(int j = 0; j <= cells; ++j) {
                        Cell cell = row.getCell(j);
                        if (cell != null) {
                            switch(cell.getCellType()) {
                                case 0:
                                    value.append(cell.getNumericCellValue());
                                    break;
                                case 1:
                                    value.append(String.valueOf(cell.getStringCellValue().replace(" ", "")));
                                    value.toString().replace("\t", "");
                                    if (j == cells - 1) {
                                        value.append("\"");
                                    } else {
                                        value.append("\",\"");
                                    }
                                    break;
                                case 2:
                                    try {
                                        value.append(String.valueOf(cell.getStringCellValue()));
                                    } catch (IllegalStateException var13) {
                                        System.out.println("ExcelTool参数异常,请定位到getExcel()方法");
                                    }
                                case 3:
                                default:
                                    break;
                                case 4:
                                    value.append(cell.getBooleanCellValue());
                                    break;
                                case 5:
                                    value.append(cell.getErrorCellValue());
                            }
                        }
                    }
                }

                if (i == rows - 1) {
                    value.append(")\n");
                    break;
                }

                value.append("),\n");
            }

            return value.toString();
        }
}


